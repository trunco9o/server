startDate = ISODate("2018-05-18T00:00:00Z");
endDate = ISODate("2018-05-21T00:00:00Z");
db.reviews.aggregate([
    {$match : { "created" : { $gte: startDate, $lt: endDate} } },
    {$addFields:{
        created:{
            $dateFromParts:{
                year:{
                    $year:"$created"
                }, 
                month:{
                    $month:"$created"
                }, 
                day:{
                    $dayOfMonth : "$created" 
                }
            }
        }, 
        dateRange:{
            $map:{
                input:{
                    $range:[0, {$subtract:[endDate,startDate]}, 1000*60*60*24]
                },
                in:{
                    $add:[startDate, "$$this"]
                }
            }
        } 
    }},
    {$unwind:"$dateRange"},
    {$group:{
        _id:"$dateRange", 
        reviews:{$push:{$cond:[
                    {$eq:["$dateRange","$created"]},
                    {count: 1},
                    {count:0}
        ]}}
    }},
    {$sort:{_id:1}},
    {$project:{
        _id:0,
        date:"$_id",
        totalReviews:{$sum:"$reviews.count"}
    }}
])