import io
import csv

import random

from datetime import datetime
from bson.json_util import dumps, loads

from flask import request, make_response

from app import app, session

from models.Review import ReviewModel

class review_controller:
    """
    review_controller handles reviews
    """

    @app.route('/reviews/download')
    def downloadReviews():
        userId = session.get('user', None)

        si = io.StringIO()
        cw = csv.writer(si)

        selected = request.args.get('selected')

        if selected:
            reviews = ReviewModel.getByUserId(userId, loads(selected))
        else:
            reviews = ReviewModel.getByUserId(userId)

        reviewArr = [['status', 'completed', 'created']]

        for review in reviews:
            if not 'positive' in review and not 'isInvalid' in review:
                status = 'N/A'
            elif review['positive']:
                status = 'Positive'
            elif 'isInvalid' in review and review['isInvalid']:
                status = 'Invalid'
            else:
                status = 'Negative'

            completed = 'Yes' if 'completed' in review and review['completed'] else 'No'
            created = review['created']

            reviewArr.append([status, completed, created])
        
        cw.writerows(reviewArr)

        output = make_response(si.getvalue())

        output.headers["Content-Disposition"] = "attachment; filename=export.csv"
        output.headers["Content-type"] = "text/csv"

        return output

    @app.route('/reviews')
    def getUserReviews():
        """
        """

        #Get user from session
        userId = session.get('user', None)

        phoneNumber = request.args.get('phone')

        questions = request.args.get('questions')

        complete = request.args.get('complete')

        fromDate = request.args.get('start')

        toDate = request.args.get('end')

        _type = request.args.get('type')

        if phoneNumber:
            reviews = ReviewModel.getByPhoneNumber(userId, phoneNumber, complete=complete, questions=questions, fromDate=fromDate, toDate=toDate)

            return dumps(reviews)
        
        code = request.args.get('code')

        if code:
            review = ReviewModel.getByCode(userId, code, complete=complete, questions=questions, fromDate=fromDate, toDate=toDate)

            if not len(review):
                review = {}
            else:
                review = review[0]
                
            return dumps(review)

        reviews = ReviewModel.getByUserId(userId, fromDate=fromDate, toDate=toDate, complete=complete, questions=questions)

        if _type == 'random':
            reviews = list(reviews)
            count = len(reviews)

            if count == 1:
                return dumps(reviews[0])
            elif not count:
                return dumps({})
            else:     
                randomIndex = random.randint(0, count - 1)

                return dumps(reviews[randomIndex])

        return dumps(reviews)

    @app.route('/reviews/<string:reviewId>')
    def getUserReview(reviewId):
        """
        View given review for a user (GET /review/user/<string:reviewId>)
        Gets review, renders page
        """

        #Get user from session
        user = session.get('user', None)

        #Get review from database
        review = ReviewModel.getById(reviewId)

        #Render template
        return dumps(review)
        
    @app.route('/reviews/aggregation')
    def reviewAggregation():
        today = datetime.today()

        first = today.replace(day=1, hour=0, minute=0, second=0, microsecond=0)
        tomorrow = today.replace(day=(today.day + 3), hour=0, minute=0, second=0, microsecond=0)

        reviews = ReviewModel.getAggregation(first, tomorrow)

        return dumps(reviews)