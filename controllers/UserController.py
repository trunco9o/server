from flask import request
from bson.json_util import dumps

from app import app, session

from models.User import UserModel

class user_controller:
    """
    user_controller handles routes for getting and editing a user
    """

    @app.route('/user')
    def getUser():
        userId = session.get('user', False)

        user = UserModel.getById(userId, ignoreFields={'password': 0, 'salt': 0})

        return dumps(user)

    @app.route('/user', methods=['POST'])
    def createUser():
        userJson = request.json

        valid = UserModel.validateUserJson(userJson)

        if valid is True:
            return dumps(UserModel.save(userJson))
        else:
            return dumps(valid), 400

    @app.route('/user', methods=['PUT'])
    def updateUser():
        userId = session.get('user', False)

        userJson = request.json

        valid = UserModel.validateUserJson(userJson)

        if valid is True:
            user = UserModel.update(userId, userJson)
            return dumps(user)
        else:
            return dumps(valid), 400