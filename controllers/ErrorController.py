from app import app
from flask import render_template

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.errorhandler(403)
def page_not_found1(e):
    return render_template('404.html'), 403

@app.errorhandler(410)
def page_not_found2(e):
    return render_template('404.html'), 410

@app.errorhandler(500)
def page_not_found3(e):
    return render_template('404.html'), 500