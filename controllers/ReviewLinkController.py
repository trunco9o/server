from bson.json_util import dumps
from flask import redirect, render_template, request
from app import app, session

from models.ReviewLinks import ReviewLinksModel

class review_link_controller:
    @app.route('/reviewLinks')
    def getReviewLinks():
        userId = session.get('user')

        reviewLinks = ReviewLinksModel.getByUserId(userId)

        return dumps(reviewLinks)

    @app.route('/reviewLinks', methods=['POST'])
    def makeReviewLink():
        userId = session.get('user')

        json = request.json

        reviewLink = ReviewLinksModel.create()

        reviewLink['userId'] = userId
        reviewLink['site'] = json['site']
        reviewLink['url'] = json['url']

        reviewLink = ReviewLinksModel.save(reviewLink)

        return dumps(reviewLink)

    @app.route('/reviewLinks/<string:id>', methods=['PUT'])
    def updateReviewLink(id):
        userId = session.get('user')

        json = request.json

        reviewLink = ReviewLinksModel.getById(id)

        update = {
            'site': json['site'],
            'url': json['url']
        }

        reviewLink = ReviewLinksModel.update(userId, reviewLink['_id'], update)

        return dumps(reviewLink)

    @app.route('/reviewLinks/<string:id>', methods=['DELETE'])
    def deleteReviewLink(id):
        userId = session.get('user')

        reviewLink = ReviewLinksModel.getById(id)
        
        ReviewLinksModel.delete(reviewLink)
        
        return '{}'