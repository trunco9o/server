from flask import redirect, render_template
from app import app

from models.ReviewLinks import ReviewLinksModel

class review_link_redirect_controller:
    @app.route('/r/<string:hash>')
    def reviewLinkRedirect(hash):
        reviewLink = ReviewLinksModel.getReviewLinkByHash(hash)

        if reviewLink is None:
            return render_template('not-found.html')
        else:
            #Save data
            return redirect(reviewLink['url'])