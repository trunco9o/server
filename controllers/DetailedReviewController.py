from bson.json_util import dumps

from app import app, session
from flask import render_template, request, abort

from models.Review import ReviewModel
from models.User import UserModel
from models.Survey import SurveyModel

class detailed_review_controller:
    @app.route('/review/<string:reviewId>')
    def reviewPage(reviewId=False):

        review = ReviewModel.getById(reviewId)

        if not review or 'additionalComments' in review:
            return abort(404)

        user = UserModel.getById(review['userId'])
        survey = SurveyModel.getByUserId(user['_id'])

        reviewString = dumps(review)

        print(user)

        business = dumps({
            'name': user['business'],
            'color': survey['reviewColor']
        })

        return render_template('detailedReview.html', review=reviewString, business=business)

    @app.route('/review/<string:reviewId>', methods=['POST'])
    def saveDetailedReview(reviewId=False):
        review = ReviewModel.getById(reviewId)

        if not review or 'additionalComments' in review:
            return abort(404)

        json = request.json

        ReviewModel.update(reviewId, {'additionalComments': json['comments']})

        return dumps({'success': True})