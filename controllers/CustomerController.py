import io
import csv

from bson.json_util import dumps, loads

from flask import redirect, make_response, request

from app import app, session

from models.User import UserModel
from models.Review import ReviewModel
from models.Customer import CustomerModel

class customer_controller:
    """
    
    """

    @app.route('/customers', methods=['POST'])
    def uploadCustomer():
        userId = session.get('user', None)

        json = request.json

        customer = CustomerModel.create()

        customer['userId'] = userId
        customer['name'] = json['name']
        customer['phoneNumber'] = json['phone']
        customer['email'] = json['email']

        customer = CustomerModel.save(customer)

        return dumps(customer)

    @app.route('/customers/upload', methods=['POST'])
    def uploadCustomers():
        userId = session.get('user', None)

        requestFile = request.files['file']

        stream = io.StringIO(requestFile.stream.read().decode("UTF8"), newline=None)
        csv_input = csv.reader(stream)

        for row in csv_input:
            if row[0] != 'name':
                customer = CustomerModel.create(userId)

                customer['name'] = row[0]
                customer['phoneNumber'] = row[1]
                customer['email'] = row[2]
                
                CustomerModel.save(customer)

        return dumps(CustomerModel.getByUserId(userId))

    @app.route('/customers/download/all')
    def downloadAllCustomers():
        userId = session.get('user', None)

        si = io.StringIO()
        cw = csv.writer(si)

        selected = request.args.get('selected')

        if selected:
            reviewers = ReviewModel.getUserContacts(userId, loads(selected))
        else:
            reviewers = ReviewModel.getUserContacts(userId)

        customerArr = [['name', 'phone number', 'email address', 'created']]

        for customer in reviewers:
            name = customer['name'] if 'name' in customer and customer['name'] else 'N/A'
            phone = customer['phoneNumber'] if 'phoneNumber' in customer and customer['phoneNumber'] else 'N/A'
            email = customer['email'] if 'email' in customer and customer['email'] else 'N/A'

            customerArr.append([name, phone, email, customer['created']])

        if selected:
            customers = CustomerModel.getByUserId(userId, loads(selected))
        else:
            customers = CustomerModel.getByUserId(userId)

        for customer in customers:
            name = customer['name'] if 'name' in customer and customer['name'] else 'N/A'
            phone = customer['phoneNumber'] if 'phoneNumber' in customer and customer['phoneNumber'] else 'N/A'
            email = customer['email'] if 'email' in customer and customer['email'] else 'N/A'

            customerArr.append([name, phone, email, customer['created']])
        
        cw.writerows(customerArr)

        output = make_response(si.getvalue())

        output.headers["Content-Disposition"] = "attachment; filename=export.csv"
        output.headers["Content-type"] = "text/csv"

        return output

    @app.route('/customers/all')
    def allCustomers():
        userId = session.get('user', None)

        customerArr = []

        reviewers = ReviewModel.getUserContacts(userId)

        for customer in reviewers:
            customerArr.append(customer)

        customers = CustomerModel.getByUserId(userId)

        for customer in customers:
            customerArr.append(customer)

        return dumps(customerArr)

    @app.route('/customers/download/uploaded')
    def downloadUploadedCustomers():
        userId = session.get('user', None)

        si = io.StringIO()
        cw = csv.writer(si)

        customerArr = [['name', 'phone number', 'email address', 'created']]

        selected = request.args.get('selected')

        if selected:
            customers = CustomerModel.getByUserId(userId, loads(selected))
        else:
            customers = CustomerModel.getByUserId(userId)

        for customer in customers:
            name = customer['name'] if 'name' in customer and customer['name'] else 'N/A'
            phone = customer['phoneNumber'] if 'phoneNumber' in customer and customer['phoneNumber'] else 'N/A'
            email = customer['email'] if 'email' in customer and customer['email'] else 'N/A'

            customerArr.append([name, phone, email, customer['created']])
        
        cw.writerows(customerArr)

        output = make_response(si.getvalue())

        output.headers["Content-Disposition"] = "attachment; filename=export.csv"
        output.headers["Content-type"] = "text/csv"

        return output

    @app.route('/customers/uploaded')
    def uploadedCustomers():
        customers = CustomerModel.getByUserId(session.get('user', None))

        return dumps(customers)

    @app.route('/customers/download/reviewers')
    def downloadReviewersCustomers():
        userId = session.get('user', None)

        si = io.StringIO()
        cw = csv.writer(si)

        customerArr = [['name', 'phone number', 'email address', 'created']]

        selected = request.args.get('selected')

        if selected:
            reviewers = ReviewModel.getUserContacts(userId, loads(selected))
        else:
            reviewers = ReviewModel.getUserContacts(userId)

        for customer in reviewers:
            name = customer['name'] if 'name' in customer and customer['name'] else 'N/A'
            phone = customer['phoneNumber'] if 'phoneNumber' in customer and customer['phoneNumber'] else 'N/A'
            email = customer['email'] if 'email' in customer and customer['email'] else 'N/A'

            customerArr.append([name, phone, email, customer['created']])
        
        cw.writerows(customerArr)

        output = make_response(si.getvalue())

        output.headers["Content-Disposition"] = "attachment; filename=export.csv"
        output.headers["Content-type"] = "text/csv"

        return output

    @app.route('/customers/reviewers')
    def reviewers():
        reviewers = ReviewModel.getUserContacts(session.get('user', None))

        return dumps(reviewers)