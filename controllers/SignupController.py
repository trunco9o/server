import json

from flask import redirect, render_template, request

from app import app, session

from models.User import UserModel


class signup_controller:
    @app.route('/signup')
    def signupPage():
        return render_template('signup.html')

    @app.route('/signup', methods=['POST'])
    def saveSignup():
        userJson = request.json

        hasErrors = UserModel.validateUserJson(userJson)

        if hasErrors:
            return json.dumps(hasErrors)

        user = UserModel.create(userJson)

        #Send email

        return json.dumps({'error': False})

    @app.route('/signup/verify')
    def verifySignup():
        token = request.args.get('verificationToken')

        user = UserModel.verify(token)

        error = None

        if user:
            session['user'] = user['_id']

            try:
                UserModel.createSmsNumber(user)
            except Exception as ex:
                print(ex)
            
            UserModel.createCustomer(user)
        else:
            error = 'Sorry, we couldn\'t find your account.'

        return render_template('verified.html', error=error)
