from flask import render_template, redirect

from app import app, session

from models.User import UserModel
from models.Review import ReviewModel

class dashboard_controller:
    """
    dashboard_controller handles routes for the client dashboard
    """
    @app.route('/dashboard')
    def dashboard():
        """
        Dashboard index page (GET /dashboard). Fetches user from session and renders dashboard.html
        """

        #Get the userId from the session / Currently logged in user
        userId = session.get('user', None)

        #If there isn't a user, display an error. Should be replaced with middleware later
        if userId is None:
            return redirect('/login')

        #Lookup user in the database
        dbUser = UserModel.getById(userId, reviewLinks=True, survey=True)

        reviews = ReviewModel.getNewest(userId, 5)

        #Render dashboard.html, passing user
        return render_template('dashboard.html', user=dbUser, reviews=reviews)
