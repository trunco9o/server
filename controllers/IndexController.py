from app import app, session
from flask import render_template, redirect

class index_controller:
    """
    Index controller is for index page 
    """

    @app.route('/')
    def index():
        userId = session.get('user', None)

        if userId:
            return redirect('/dashboard')

        return render_template('index.html')

    @app.route('/privacy-policy')
    def privacy():
        return render_template('privacypolicy.htm')

    @app.route('/features')
    def features():
        return render_template('features.html')

    @app.route('/pricing')
    def pricing():
        return render_template('pricing.html')

    @app.route('/demo')
    def demo():
        return render_template('demo.html')