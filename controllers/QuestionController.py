import datetime
from bson.json_util import dumps

from flask import request, redirect

from app import app, session

from models.User import UserModel
from models.Survey import SurveyModel
from models.Question import QuestionModel
from models.RetiredQuestion import RetiredQuestionModel

class question_controller:
    """
    question_controller handles routes for creating, updating and deleting questions
    """

    @app.route('/question')
    def getQuestions():
        """
        Get all questions
        """

        userId = session.get('user', None)

        survey = SurveyModel.getByUserId(userId)

        questions = QuestionModel.getBySurveyId(survey['_id'])

        return dumps(questions)

    @app.route('/question', methods=['POST'])
    def saveQuestion():
        """
        Saves question (POST /question). Receives form from request and adds a question to the logged in user's survey. 
        Redirects to /dashboard
        """

        #Get form from request
        json = request.json

        #Get user from session
        userId = session.get('user', None)

        #Get the given survey
        survey = SurveyModel.getByUserId(userId)

        #Get the surveyId from user survey
        surveyId = survey['_id']

        valid = QuestionModel.validate(json, surveyId)

        if valid is not True:
            return dumps(valid), 500
        
        #Create a question
        question = QuestionModel.create()

        #Set question data
        question['surveyId'] = surveyId
        question['type'] = json['type']
        question['text'] = json['text']

        #Get the question type from the form
        questionType = json['type']

        #Check the question type
        isRange = (questionType == 'range')
        
        #If it is a range, set the scale property on Question
        if isRange:
            question['scale'] = int(json['scale'])

        if json['indicator']['isIndicator']:
            question['indicator']['isIndicator'] = True
            
            indicator = json['indicator']
            if json['type'] == 'open-ended':
                json['indicator']['isIndicator'] = False
            else:
                question['indicator']['isIndicator'] = True
                question['indicator']['positiveCutoff'] = indicator['positiveCutoff']
                question['indicator']['higherIsGood'] = indicator['higherIsGood']
                question['indicator']['booleanIndicator'] = indicator['booleanIndicator']

        newQuestion = QuestionModel.save(question)

        UserModel.setHasSurvey(userId, True)

        #Respond w question
        return dumps(newQuestion)

    @app.route('/question/<string:id>')
    def getQuestion(id):
        """
        Edit question page (GET /question/<string:id>).  
        Gets question from database
        """

        #Get user from session
        userId = session.get('user', None)

        #Get question
        question = QuestionModel.getById(id)

        #Get survey
        survey = SurveyModel.getByUserId(userId)

        #Verify survey belongs to user and question belongs to survey
        if survey['userId'] != userId or survey['_id'] != question['surveyId']:
            return 'Error'

        #Render question.html
        return dumps(question)

    @app.route('/question/<string:id>', methods=['PUT'])
    def updateQuestion(id):
        """
        Saves updated question (PUT question/<string:id>). 
        Gets given question from the database and updates the entry. 
        Redirects to /dashboard
        """
        
        #Get form from request
        json = request.json

        #Get user id from session
        userId = session.get('user', None)

        #Get question from database by ID in path
        question = QuestionModel.getById(id)

        #Get survey by user id
        survey = SurveyModel.getByUserId(userId)

        valid = QuestionModel.validate(json, survey['_id'], update=True)

        if valid is not True:
            return dumps(valid), 500

        update = {
            'text': json['text'],
            'type': json['type'],
            'scale': json['scale'],
            'indicator': json['indicator']
        }

        updatedQuestion = QuestionModel.update(id, update)

        return dumps(updatedQuestion)

    @app.route('/question/<string:id>', methods=['DELETE'])
    def deleteQuestion(id):
        """
        Deletes a question (DELETE /question/<string:id>).
        Gets given question from the database and deletes it. 
        Redirects to /dashboard
        """
        
        #Get user from session
        userId = session.get('user', None)

        #Get question by ID in path
        question = QuestionModel.getById(id)

        #Get survey for user
        survey = SurveyModel.getByUserId(userId)

        #Ensure survey belongs to user and question belongs to survey
        if survey['userId'] != userId or survey['_id'] != question['surveyId']:
            return 'Error'

        #Create retired question
        retiredQuestion = RetiredQuestionModel.create()

        #Save question ID
        questionId = question['_id']

        #Save other question vars
        questionType = question['type']
        
        questionUpdated = datetime.datetime.utcnow()

        if 'updated' in question:
            questionUpdated = question['updated']

        #Set retired question vars
        retiredQuestion['originalId'] = questionId
        retiredQuestion['surveyId'] = question['surveyId']
        retiredQuestion['type'] = questionType
        retiredQuestion['text'] = question['text']
        retiredQuestion['originalCreated'] = question['created']

        if questionType == 'range':
            retiredQuestion['scale'] = question['scale']

        retiredQuestion['originalUpdated'] = questionUpdated

        RetiredQuestionModel.save(retiredQuestion)
        #Delete question
        QuestionModel.delete(question)


        if QuestionModel.getTotalQuestions(survey['_id']) == 0:
            update = {'hasSurvey': False}
            UserModel.update(userId, update)

        return "{}"