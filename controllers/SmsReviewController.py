from services.ReviewService import review_service
from models.Survey import SurveyModel
from models.User import UserModel
from models.Question import QuestionModel
from models.ReviewLinks import ReviewLinksModel

@app.route('/review/sms', methods=['POST'])
def handleSms():
    """
    Handles incoming SMS from twilio
    """

    form = request.form

    fromNumber = form['From']
    toNumber = form['To']
    body = form['Body']

    reviewService = review_service(ReviewModel, UserModel, SurveyModel, QuestionModel, ReviewLinksModel)

    return reviewService.doReview(fromNumber, toNumber, body)


@app.route('/review/sms/error', methods=['POST'])
def handleSmsError():
    """
    Handles incoming SMS from twilio
    """

    print(request.form)

    return ''