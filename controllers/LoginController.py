from flask import request, redirect 
from app import app, session
from flask import render_template

from models.User import UserModel

class login_controller:
    """
    Index controller is for index page 
    """

    @app.route('/login')
    def loginPage(message=''):
        return render_template('login.html', message=message)

    @app.route('/login', methods=['POST'])
    def login():
        form = request.form

        user = UserModel.getByEmail(form['email'])

        if UserModel.checkLogin(user, form['password']):

            if(user['verified']):
                session['user'] = user['_id']

                return redirect('/dashboard')

            else:
                return render_template('verify.html')

        else:
            return login_controller.loginPage('Invalid login.')

    @app.route('/logout')
    def logout():
        if session['user']:
            del session['user']
        
        return redirect('/')