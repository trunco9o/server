from flask import render_template, request
from services.EmailService import email_service
from models.User import UserModel

    @app.route('/review/<string:id>')
    def detailedReview(id):
        """
        Add detailed review page (GET /review/<string:id>).
        Gets review and associated user, renders page
        """

        #Get review from URL
        review = ReviewModel.getById(id)

        #Get user from review
        user = UserModel.getById(review['userId'])

        #Render review.html
        return render_template('review.html', business=user['business'])

    @app.route('/review/<string:id>', methods=['POST'])
    def saveDetailedReview(id):
        """
        Save detailed review (POST /review/<string:id>).
        Saves detailed review
        """

        #Get form from request
        form = request.form

        #Check that review and contact fields are provided
        if not 'review' in form or not 'contact' in form:
            #Change error to review.html w/ error message
            return 'Error'
        
        #Get values from form
        reviewText = form['review']
        contact = form['contact']

        #Check that fields have value
        if not reviewText or not contact:
            #Change error to review.html w/ error message
            return 'Error'

        #Get review from URL
        review = ReviewModel.getById(id)

        #Get user from review
        user = UserModel.getById(review['userId'])

        contact = (contact == '1')

        review['additionalComments'] = reviewText
        review['contact'] = contact
        review['completed'] = datetime.utcnow()

        #Validate reivew
        review.validate()

        #Save review
        review.save()

        #Create a new email
        email = email_service.createMessage("New Review")

        #Set recipients
        email.recipients = [user['email']]

        #Set HTML
        email.html = email_service.makeEmailBody(reviewText, contact, review['phoneNumber'])

        #Send email
        email_service.sendEmail(email)

        #Render saved-review.html
        return render_template('saved-review.html')