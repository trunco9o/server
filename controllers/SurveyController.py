from flask import request
from bson.json_util import dumps

from app import app, session

from models.Survey import SurveyModel
from models.Question import QuestionModel
from models.ReviewLinks import ReviewLinksModel

class survey_controller:
    """
    user_controller handles routes for editing a user
    """

    @app.route('/survey')
    def getSurvey():
        userId = session.get('user', None)

        survey = SurveyModel.getByUserId(userId)

        survey['questions'] = QuestionModel.getBySurveyId(survey['_id'])

        survey['reviewLinks'] = ReviewLinksModel.getByUserId(userId)

        return dumps(survey)

    @app.route('/survey', methods=['PUT'])
    def updateSurvey():
        userId = session.get('user', None)

        json = request.json

        survey = SurveyModel.getByUserId(userId)

        update = {
            'trigger': json['trigger'],
            'invalidReviewMessage': json['invalidReviewMessage'],
            'negativeReviewMessage': json['negativeReviewMessage'],
            'positiveReviewMessage': json['positiveReviewMessage'],
            'positiveCutoff': json['positiveCutoff'],
            'reviewColor': json['reviewColor']
        }

        valid = SurveyModel.validateUpdate(update)

        if valid is True:
            survey = SurveyModel.update(survey['_id'], update)

            survey['questions'] = QuestionModel.getBySurveyId(survey['_id'])

            survey['reviewLinks'] = ReviewLinksModel.getByUserId(userId)

            return dumps(survey)
        else:
            return dumps(valid), 500


