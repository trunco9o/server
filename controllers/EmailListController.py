import json

from app import app, session
from flask import render_template, request

from models.EmailList import EmailListModel

class email_list_controller:

    @app.route('/email')
    def emailSignupPage():
        return render_template('email-signup.html')

    @app.route('/email', methods=['POST'])
    def saveEmail():
        message = ''

        try: 
            email = request.form['email']

            EmailListModel.create(email)
            
            message = 'Thanks! We\'ll be sure to let you know when RepGuru is ready.' 

            return render_template('email-signup.html', error=False, message=message)

        except Exception as ex:
            print(ex) 
            message = 'There was an error. Please check your email address and try again.'

            return render_template('email-signup.html', error=True, message=message)

    @app.route('/email/json', methods=['POST'])
    def saveJsonEmail():
        try:
            requestJson = request.json

            print(requestJson)

            email = requestJson['email']

            EmailListModel.create(email)
                
            message = 'Thanks! We\'ll be sure to let you know when RepGuru is ready.' 

            return json.dumps({'success': message})

        except Exception as ex:
            print(ex)
            return json.dumps({'error': 'There was an error. Please check your email address and try again.'})
