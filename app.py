#Import main Flask + session from flask
from flask import Flask, session
#Import Session object from Flask-Session package
from flask_session import Session
#Import Mail object from Flask-Mail
from flask_mail import Mail
#Import dotenv to load env variables
from dotenv import load_dotenv
#Import pathlib to load env path
from pathlib import Path

#Import User to use in first_request middleware
from models.User import userDb
#Set env path
env_path = str(Path('.') / '.env')

#Load .env file
load_dotenv(dotenv_path=env_path)

#Create a flask app
app = Flask(__name__)

#Set debug to True
app.debug = True

#Store session data in mongodb
SESSION_TYPE = 'mongodb'

#Don't actually send mail
MAIL_SUPPRESS_SEND = True

#Add SESSION_TYPE to config
app.config.from_object(__name__)

#Create session 
Session(app)

#Create mail
mail = Mail(app)

#Import controllers to run in app
from controllers.CustomerController import customer_controller
from controllers.DashboardController import dashboard_controller
from controllers.EmailListController import email_list_controller
from controllers.IndexController import index_controller
from controllers.LoginController import login_controller
from controllers.QuestionController import question_controller
from controllers.ReviewController import review_controller
from controllers.ReviewLinkController import review_link_controller
from controllers.UserController import user_controller
from controllers.LoginController import login_controller
from controllers.SignupController import signup_controller
from controllers.SurveyController import survey_controller
from controllers.DetailedReviewController import detailed_review_controller

import controllers.ErrorController

#Import template filters
from template_filters.date_filter import _filter_utc_date
from template_filters.get_question import _get_question_text

#Decorator to run method before first request handled
@app.before_first_request
def before_first_request():
    """
    Get the first user and create a session
    Remove for PROD!
    """

    #Get first user from database (populated by seeder)
    user = userDb.find({})[0]
    
    #Set a session variable, user, containing the first user's ID
    session['user'] = user['_id']
