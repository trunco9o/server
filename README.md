# Restaurant Review Server

Quick SMS feedback for restaurants and other businesses

## Dependencies
* Flask
* Flask-Session
* Flask-Mail
* livereload
* Twilio
* Pymongo
* Sass
* Python-dotenv
* Python-dateutil
* Stripe
* hashids

## env Variables
Create a file named `.env` in the top-level directory of the project

Within add the following variables:

```
TWILIO_SID=[Twilio SID]
TWILIO_AUTH_TOKEN=[Twilio auth token]
TWILIO_NUMBER=[Number from Twilio test account]
HOST=[IP/hostname if you don't want to run on localhost]
PORT=[port # if you don't want to run]
STRIPE_KEY=[Stripe API key]
STRIPE_SECRET=[Stripe API secret]
URL=[Site URL]
REVIEW_CODE_SALT=[random string]
```

## Installation
MongoDB and Python3 are prerequisites. MongoDB is expected on localhost, default port no password (for now).

Install python3

`brew install python3`

Install MongoDB (on OSX)

`brew install mongodb`

Run this command to install all python dependencies:
`$ [sudo] pip[3] install flask livereload twilio pymongo==3.6.1 Flask-Session Flask-Mail python-dotenv python-dateutil stripe hashids`

Install Node & NPM

https://nodejs.org/en/download/

Install Sass

https://sass-lang.com/install

Recommended because it supports `--watch` flag:
`gem install sass`

Alternatively

`brew install --devel sass/sass/sass`

Alternatively 

`npm i -g sass`

## Running the app
mongod should be running on localhost, eg

`$ mongod &`

## Getting Error about data/db?
https://stackoverflow.com/a/31616194/3321534

Then,

`$ export FLASK_APP=server.py`

`$ python[3] server.py`

## Seeding the DB 
A DB seeding script is provided to initalize data for development. 

mongod should be running on localhost, eg 
`$ mongod &`

After mongo is running, run:
`python[3] -m seeds.seeder`

To add more than one review when seeding the database, run 
`python[3] -m seeds.seeder.py reviews=[number of reviews]`

## Default user
The login for the default user account is email: `test@test.com`, password: `test`. 

## JS dev
1. Go to `./assets/js/` 
2. Run `$ npm install`
3. Run `$ npm compile[-watch]` use `compile-watch` if you 


## CSS dev
If ruby sass is installed, simply run `$ ./compile-sass.sh` in a new terminal window. It uses the `--watch` flag to compile on change.

node sass doesn't support `--watch`, thusly you'll have to run `sass assets/sass:static/css` each time you make a change. 


## Directory Structure

```
assets/ -- Sass and uncompiled JS
controllers/ -- Controllers handle requests
mocks/ -- Mocks provide data to avoid using database
models/ -- Models handle database interaction
seeds/ -- Seed database for dev
services/ -- Classes used across many controllers
static/ -- Static assets
    css/ -- Compiled CSS
    js/ -- Compiled JavaScripts
template_filters/ -- Jinja template filters - run a method from a template
templates/ -- Jinja templates
app.py -- Setup Flask app
constants.py -- Constant values for use in app
server.py -- Run server
```


## Getting documentation
pydoc is used for documentation. 

To see pydoc for a given class, run:
`pydoc[3] [module name].[file name]`

For more info, see:
https://docs.python.org/3.6/library/pydoc.html
