const path = require('path');

module.exports = {
    mode: 'development',
    entry:  {
        dashboard: ['babel-polyfill', path.resolve(__dirname, 'app/dashboard.js')],
        demo: ['babel-polyfill', path.resolve(__dirname, 'app/demo.js')],
        index: ['babel-polyfill', path.resolve(__dirname, 'app/index.js')],
        nav: [path.resolve(__dirname, 'app/nav.js')],
        pricing: ['babel-polyfill', path.resolve(__dirname, 'app/pricing.js')],
        signup: ['babel-polyfill', path.resolve(__dirname, 'app/signup.js')],
        detailedReview: ['babel-polyfill', path.resolve(__dirname, 'app/detailedReview.js')],
    },
    output: {
      path: path.resolve(__dirname, '../../static/js/'),
      filename: "[name].js"
    },
    module: {
      rules: [
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader'],
        },
        {
          test: /\.sass$|.scss$/,
          use: ['style-loader', 'css-loader', 'sass-loader']
        },
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'babel-loader',
          query: {
            presets: ['es2015','react'],
          }
        }
      ]
    }
  }