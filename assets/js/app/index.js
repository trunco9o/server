import React from 'react';
import ReactDOM  from 'react-dom';
import EmailSignup from './components/email-signup.component';

var element = document.getElementById('signup-container');

ReactDOM.render(<EmailSignup title="Sign up to get notified when we launch!"/>, element);