import React from 'react';
import ReactDOM  from 'react-dom';

import DetailedReview from './components/detailedReview.component';

const element = document.getElementById('container');

const businessJSON = document.getElementById('business').textContent;
const reviewJSON = document.getElementById('review').textContent;

const business = JSON.parse(businessJSON);
const review = JSON.parse(reviewJSON);

ReactDOM.render(<DetailedReview business={business} review={review}/>, element);