export default {
    index: 0,
    indicatorCount: 1,
    invalidReviewMessage: `Sorry that you had a negative experience! Please send us a detailed review.`,
    isInvalid: undefined,
    isPositive: undefined,
    negativeReviewMessage: `Sorry that you had a negative experience! Please send us a detailed review.`,
    positiveCutoff: 0.75,
    positiveReviewMessage: `Thanks for completing our survey!
    Review us online:`,
    questions: [
        {
            text: 'Did you enjoy your experience?',
            type: 'yes-no',
            scale: 5,
            indicator: {
                isIndicator: true,
                booleanIndicator: 'yes',
                higherIsGood: true,
                positiveCutoff: 3
            }
        }
    ],
    responseScores: [],
    reviewLinks: [
        {
            site: 'Yelp',
            url: 'https://yelp.com'
        },
        {
            site: 'Google',
            url: 'https://google.com'
        }
    ],
    trigger: 'Hey RepGuru'
};