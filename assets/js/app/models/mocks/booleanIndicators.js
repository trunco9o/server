export default {
    "yes": [
        "yes",
        "ya",
        "yeah",
        "yea",
        "yee",
        "yah",
        "yep",
        "hell ya",
        "hell yes",
        "hell yeah",
        "fuck yeah",
        "fuck ya",
        "definitely",
        "si"
    ],
    "no": [
        "no",
        "nope",
        "hell no",
        "no way",
        "na",
        "nah",
        "never",
        "fuck no",
        "hell fuckin no",
        "hell fucking no"
    ]
}