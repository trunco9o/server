import defaultSurvey from './defaultSurvey';
import booleanIndicators from './booleanIndicators';

import EventEmitter from 'events';
import MockMessageModel from './MockMessageModel';

import formatReviewLinks from '../../services/formatReviewLinks';

export default class MockSurveyModel extends EventEmitter {
    constructor(survey){
        super();

        this.messages = new MockMessageModel();

        if(!survey){
            this._survey = {...defaultSurvey};
        }
        else{
            this._survey = {...defaultSurvey, ...survey};
        }
    }

    addLink(link){
        this._survey.reviewLinks.push(link);

        this._broadcastSurvey();
    }

    addMessage(message){
        if(this._survey.index === 0 && message.toLowerCase() !== this._survey.trigger.toLowerCase()){

        }
        else{
            setTimeout(this._sendMessage.bind(this, message), 1000);
        }

        return this.messages.addMessage(message, true);
    }

    clearMessages(){
        this.messages.clear()

        this._broadcastSurvey();
    }

    getSurvey(){
        return {...this._survey};
    }

    incrementIndex(){
        this._survey.index++;
    }

    addQuestion(question){
        this._survey.questions.push(question);

        if(question.indicator.isIndicator)
            this._survey.indicatorCount++;

        this._broadcastSurvey();
    }
    
    removeLink(index){
        this._survey.reviewLinks.splice(index, 1);

        this._broadcastSurvey();
    }

    removeQuestion(index){
        const question = this._survey.questions[index];

        if(question._model.indicator.isIndicator)
            this._survey.indicatorCount--;

        this._survey.questions.splice(index, 1);

        this._broadcastSurvey();
    }

    resetSurvey(broadcast = false){
        this._survey = {...defaultSurvey};

        if(broadcast){
            this._broadcastSurvey();
        }
    }

    setTrigger(trigger){
        this._survey.trigger = trigger;

        this._broadcastSurvey();
    }

    updateInvalidReviewMessage(message){
        this._survey.invalidReviewMessage = message;

        this._broadcastSurvey();
    }

    updateLink(index, link){
        this._survey.reviewLinks[index] = link;

        this._broadcastSurvey();
    }

    updateNegativeReviewMessage(message){
        this._survey.negativeReviewMessage = message;

        this._broadcastSurvey();
    }

    updatePositiveCutoff(cutoff){
        this._survey.positiveCutoff = cutoff;

        this._broadcastSurvey();
    }

    updatePositiveReviewMessage(message){
        this._survey.positiveReviewMessage = message;

        this._broadcastSurvey();
    }

    updateQuestion(index, question){
        this._survey.questions[index] = question;

        this._broadcastSurvey();
    }

    _broadcastSurvey(reset = false){
        if(reset){
            this.resetSurvey();
        }
        else{
            this._clearSurveyState();
        }
        
        Promise.resolve(this.emit('surveyChange', this._survey))
            .then(() => this.messages.clear());
    }

    _checkBooleanIndicator(indicator, answer){
        const formattedAnswer = answer.toLowerCase();
        let value = 0;

        if(booleanIndicators.yes.indexOf(formattedAnswer) >= 0){
            if(indicator.booleanIndicator === 'yes')
                value = 1;
        }
        else if(booleanIndicators.no.indexOf(formattedAnswer) >= 0){
            if(indicator.booleanIndicator === 'no')
                value = 1;
        }
        else{
            this._survey.isInvalid = true;
        }

        return value;
    }

    _checkRangeIndicator(indicator, answer){
        const parsedAnswer = parseInt(answer);
        let value = 0;

        if(isNaN(parsedAnswer)){
            this._survey.isInvalid = true;
        }
        else if(indicator.higherIsGood){
            if(indicator.positiveCutoff <= answer)
                value = 1;
        }
        else{
            if(indicator.positiveCutoff >= answer)
                value = 1;
        }

        return value;
    }

    _clearSurveyState(){
        this._survey.isInvalid = undefined;
        this._survey.isPositive = undefined;
        this._survey.index = 0;
    }

    _gradeQuestion(question, answer){
        if(question._model.type === 'open-ended' || !question._model.indicator.isIndicator)
            return;
        
        let res;
        const indicator = question._model.indicator;

        if(question._model.type === 'yes-no'){
            res = this._checkBooleanIndicator(indicator, answer);
        }
        else if(question._model.type === 'range'){
            res = this._checkRangeIndicator(indicator, answer);
        }
        else{
            return;
        }
            
        this._survey.responseScores.push(res);
        
    }

    _gradeSurvey(){
        if(this._survey.isInvalid || !this._survey.responseScores);
            return;

        const total = this._survey.responseScores.reduce((accumulator, value) => accumulator += value);

        const score = (total / this._survey.indicatorCount);

        this._survey.isPositive = (score > this._survey.positiveCutoff);
    }

    _sendMessage(message){
        const surveyIndex = this._survey.index;
        const questionLength = this._survey.questions.length;

        let response;

        if(!questionLength){
            return;
        }

        let index = surveyIndex;

        if(surveyIndex > 1){
            index = surveyIndex - 1;    
        }

        if(surveyIndex > 0){
            this._gradeQuestion(this._survey.questions[index - 1], message);
        }

        if(surveyIndex === questionLength || surveyIndex === questionLength + 1){
            this._gradeSurvey();

            if(this._survey.isPositive){
                response = `${this._survey.positiveReviewMessage}\n${formatReviewLinks(this._survey.reviewLinks)}`;
            }
            else if(this._survey.isInvalid){
                response = this._survey.invalidReviewMessage;
            }
            else if(this._survey.isPositive === false){
                response = this._survey.negativeReviewMessage;
            }
            else{
                response = `${this._survey.positiveReviewMessage}\n${formatReviewLinks(this._survey.reviewLinks)}`;
            }
        }
        else if(surveyIndex > questionLength + 1){
            return;
        }
        else{
            response = `${this._survey.questions[surveyIndex]._model.text} (${surveyIndex + 1}/${questionLength})`;
        }

        this.messages.addMessage(response);
        this.incrementIndex();
    }
}