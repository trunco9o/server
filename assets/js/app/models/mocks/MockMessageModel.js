import EventEmitter from 'events';

export default class MockMessageModel extends EventEmitter {
    constructor(){
        super();

        this._messages = [];
    }

    addMessage(text, sent){
        const message = {
            text: text,
            sent: sent
        };

        this._messages.push(message);

        this._broadcastMessages();        
    }

    clear(){
        this._messages = [];

        this._broadcastMessages();
    }

    getMessages(){
        return [...this._messages];
    }

    _broadcastMessages(){
        this.emit('messageChange', this.getMessages());
    }
}