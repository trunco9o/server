import Model from './Model';

export default class ReviewModel extends Model {
    constructor(fetchReviews){
        super();

        this._actions = {
            all: '/reviews',
            aggregations: '/reviews/aggregation',
            review: id => `/reviews/${id}`,
            search: (options) => {
                let qs = '';

                for(let [key, val] of Object.entries(options)){
                    qs += `${key}=${val}&`;
                }

                return `/reviews?${qs}`;
            }
        };

        this._model = {};

        this._backup;
        this.filtered = false;

        if(fetchReviews){
            this._fetchReviews();
        }
    }

    _broadcastChange(){
        this.emit('change', {
            reviews: this.getReviews()
        });
    }

    async _fetchReviews(){
        const fetchConfig = this._getFetchConfig(this._methods.get);

        const res = await fetch(this._actions.all, fetchConfig);

        const json = await res.json();

        json.forEach(review => {
            this._model[review._id.$oid] = review
        });

        this._broadcastChange();
    }

    async fetchAggregations(){
        const fetchConfig = this._getFetchConfig(this._methods.get);

        const res = await fetch(this._actions.aggregations, fetchConfig);

        const json = await res.json();

        return json;
    }

    getReviews(){
        return {...this._model};
    }

    async query(options){
        const fetchConfig = this._getFetchConfig(this._methods.get);

        const res = await fetch(this._actions.search(options), fetchConfig);

        const json = await res.json();

        return json;
    }

    async searchReviews(options){

        const fetchConfig = this._getFetchConfig(this._methods.get);

        const res = await fetch(this._actions.search(options), fetchConfig);

        const json = await res.json();

        this._model = {};

        json.forEach(review => {
            this._model[review._id.$oid] = review
        });

        this._broadcastChange();
    }
}