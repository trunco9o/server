import EventEmitter from 'events';

export default class Model extends EventEmitter {
    constructor(){
        super();
        
        this._methods = {
            get: 'GET',
            post: 'POST',
            put: 'PUT',
            patch: 'PATCH',
            delete: 'DELETE'
        };
    }
    _getFetchConfig(method){
        const headers = new Headers();

        headers.set('Content-Type', 'application/json');

        const fetchConfig = {
            credentials: 'same-origin',
            headers: headers,
            mode: 'cors',
            method: method
        };

        return fetchConfig;
    }
}