import Model from './Model';

import QuestionModel from './QuestionModel';
import ReviewLinksModel from './ReviewLinksModel';

import EventEmitter from 'events';

import formatReviewLinks from '../services/formatReviewLinks';

export default class SurveyModel extends Model {
    _model;
    _fetching;
    _fetchPromise;

    constructor(){
        super();

        this.actions = {
            survey: '/survey'
        };

        this._fetchPromise = this._fetchSurvey();
    }

    // Survey Methods
    _broadcastSurvey(){
        this.emit('surveyChange', {...this._model});
    }

    async _fetchSurvey(){
        this._fetching = true;

        const fetchConfig = this._getFetchConfig(this._methods.GET);

        const res = await fetch(this.actions.survey, fetchConfig);

        const json = await res.json();

        let model = {...json};

        model.questions = json.questions.map(question => new QuestionModel(question));

        model.reviewLinks = json.reviewLinks.map(link => new ReviewLinksModel(link));

        this._model = model;

        this._fetching = false;
        this._fetchPromise = null;

        return json;
    }

    async _update(){
        const fetchConfig = this._getFetchConfig(this._methods.put);

        fetchConfig.body = JSON.stringify(this._model);

        const res = await fetch(this.actions.survey, fetchConfig);

        const json = await res.json();

        this._model = {...json};

        this._model.questions = json.questions.map(question => new QuestionModel(question));

        this._model.reviewLinks = json.reviewLinks.map(link => new ReviewLinksModel(link));
    }

    async getSurvey(){
        if(!this._model && !this._fetching){
            await this._fetchSurvey();
        }
        else{
            await this._fetchPromise;
        }
        
        return {...this._model};
    }

    async save(){
        await this._update();

        this._broadcastSurvey();
    }

    async updateSurvey(data){
        let changed = false;

        if(data.trigger){
            this.setTrigger(data.trigger);

            changed = true;
        }
        else if(data.positiveCutoff){
            this.updatePositiveCutoff(data.positiveCutoff);

            changed = true;
        }
        else if(data.messages){
            data.messages.map(message => {
                if(message.type === 'invalidReviewMessage'){
                    this.updateInvalidReviewMessage(message.message);
                }
                else if(message.type === 'positiveReviewMessage'){
                    this.updatePositiveReviewMessage(message.message);
                }
                else if(message.type === 'negativeReviewMessage'){
                    this.updateNegativeReviewMessage(message.message);
                }
            });

            changed = true;
        }

        if(changed){
            return await this.save();
        }
    }

    setTrigger(trigger){
        this._model.trigger = trigger;

        this._broadcastSurvey();
    }

    updateInvalidReviewMessage(message){
        this._model.invalidReviewMessage = message;

        this._broadcastSurvey();
    }

    updateNegativeReviewMessage(message){
        this._model.negativeReviewMessage = message;

        this._broadcastSurvey();
    }

    updatePositiveCutoff(cutoff){
        this._model.positiveCutoff = cutoff;

        this._broadcastSurvey();
    }

    updatePositiveReviewMessage(message){
        this._model.positiveReviewMessage = message;

        this._broadcastSurvey();
    }

    updateReviewColor(color){
        this._model.reviewColor = color;

        this.save();
    }

    // Question methods
    async addQuestion(question){
        const newQuestion = await question.save();

        this._model.questions.push(newQuestion);

        this._broadcastSurvey();
    }

    createQuestion(question){
        return new QuestionModel(question);
    }

    async removeQuestion(index, question){
        await question.delete();

        this._model.questions.splice(index, 1);

        this._broadcastSurvey();
    }

    async updateQuestion(index, question){
        let updateIndex = index;
        const questions = this._model.questions;

        if(updateIndex !== 0 && !updateIndex){
            for(let i = 0; i < questions.length; i++){
                if(questions[i].getQuestion().id === question.getQuestion().id){
                    updateIndex = i;
                    break;
                }
            }
        }

        const updatedQuestion = await question.save();

        this._model.questions[updateIndex] = updatedQuestion;

        this._broadcastSurvey();
    }    

    // Link methods
    async addLink(link){
        await link.save()
        
        this._model.reviewLinks.push(link);
        
        this._broadcastSurvey();
    }

    createLink(){
        return new ReviewLinksModel();
    }

    async removeLink(index, link){
        await link.delete();

        this._model.reviewLinks.splice(index, 1);

        this._broadcastSurvey();
    }

    async updateLink(index, link){
        let updateIndex = index;

        if(!updateIndex){
            const reviewLinks = this._model.reviewLinks;

            for(let i = 0; i < reviewLinks.length; i++){
                if(reviewLinks[i].getLink().id === link.getLink().id){
                    updateIndex = i;
                    break;
                }
            }
        }

        const updatedLink = await link.save();

        this._model.reviewLinks[updateIndex] = updatedLink;

        this._broadcastSurvey();
    }
}