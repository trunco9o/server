// @flow

import Model from './Model';

export default class ContactModel extends Model {
    _actions: Object;

    constructor(){
        super();

        this._actions = {
            all: '/customers/all',
            reviewers: '/customers/reviewers',
            uploaded: '/customers/uploaded',
            create: '/customers'
        }
    }

    async getCustomers(which: string): Promise<Array<Object>>{
        const fetchConfig = this._getFetchConfig(this._methods.get);

        const res = await fetch(this._actions[which], fetchConfig);

        const json = await res.json();

        return json;
    }

    async saveCustomer(customer: Object): Promise<Object> {
        const fetchConfig = this._getFetchConfig(this._methods.post);

        fetchConfig.body = JSON.stringify(customer);

        const res = await fetch(this._actions.create, fetchConfig);

        const json = await res.json();

        return json;
    }
}