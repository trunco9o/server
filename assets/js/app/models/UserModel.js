import Model from './Model';

export default class UserModel extends Model {
    constructor(fetchUser){
        super();

        this._action = '/user';

        this._errors = {};

        this._model = {
            name: '',
            email: '',
            business: '',
            phone: '',
            password: '',
            confirmPassword: '',
        };

        if(fetchUser){
            this._fetchUser();
        }
    }

    _broadcastChange(){
        this.emit('change', {
            user: this.getUser(),
            errors: this.getErrors()
        });
    }

    async _fetchUser(){
        const fetchConfig = this._getFetchConfig(this._methods.GET);

        const res = await fetch(this._action, fetchConfig);

        const json = await res.json();

        this._model = json;

        this._broadcastChange()
    }

    _validate(model = this._model, validatePassword){
        let valid = true;

        this._validateName(model, false);
        this._validateEmail(model, false);
        this._validateBusinessName(model, false);
        this._validatePhoneNumber(model, false);
        
        if(validatePassword){
            this._validatePassword(model, false);
            this._validatePasswordConfirmation(model, false);
        }

        for(let [key, value] of Object.entries(this._errors)){
            if(value)
                valid = false;
        }

        this._broadcastChange();

        return valid;
    }

    _validateName(model, broadcast = true){
        if(!model.name){
            this._errors.name = 'First and Last Name are required';
        }
        else{
            this._errors.name = false;
        }

        if(broadcast)
            this._broadcastChange();
    }

    _validateEmail(model, broadcast = true){
        if(!model.email){
            this._errors.email = 'Email is required';
        }
        else{
            this._errors.email = false;
        }

        if(broadcast)
            this._broadcastChange();
    }

    _validateBusinessName(model, broadcast = true){
        if(!model.business){
            this._errors.business = 'Business Name is required';
        }
        else{
            this._errors.business = false;
        }

        if(broadcast)
            this._broadcastChange();
    }

    _validatePassword(model, broadcast = true){
        if(!model.password){
            this._errors.password = 'Please enter a password';
        }
        else{
            this._errors.password = false;
        }

        if(broadcast)
            this._broadcastChange();
    }

    _validatePasswordConfirmation(model, broadcast = true){
        const passwordValid = (this._model.password === this._model.confirmPassword);

        if(!model.confirmPassword){
            this._errors.passwordConfirmation = 'Please enter a password confirmation';
        }
        else if(!passwordValid){
            this._errors.passwordConfirmation = 'Password confirmation doesn\'t match';
        }
        else{
            this._errors.passwordConfirmation = false;
        }

        if(broadcast)
            this._broadcastChange();
    }

    _validatePhoneNumber(model, broadcast = true){
        if(!model.phone){
            this._errors.phone = 'Business Phone Number is required';
        }
        else{
            this._errors.phone = false;
        }

        if(broadcast)
            this._broadcastChange();
    }

    getErrors(){
        return {... this._errors};
    }

    getUser(){
        return {... this._model};
    }

    async send(model, validatePassword = false, method = this._methods.put){
        const valid = this._validate(model, validatePassword);
        
        if(!valid){
            return 
        }
        
        const fetchConfig = this._getFetchConfig(method);

        fetchConfig.body = JSON.stringify(model);

        const res = await fetch(this._action, fetchConfig);

        const json = await res.json();

        if(json.error){
            this._errors = json;
            
            this._broadcastChange();

            throw new Error();
        }

        this._model = json;
        this._errors = {};

        this._broadcastChange();
    }

    setBusinessName(name){
        this._model.businessName = name;

        this._validateBusinessName(false);
    }

    setEmail(email){
        this._model.email = email;

        this._validateEmail(false);
    }

    setName(name){
        this._model.name = name;

        this._validateName(false);
    }

    setPassword(password){
        this._model.password = password;

        this._validatePassword();
    }

    setPasswordConfirmation(password){
        this._model.confirmPassword = password;

        this._validatePasswordConfirmation();
    }

    setPhoneNumber(phoneNumber){
        this._model.phoneNumber = phoneNumber;

        this._validatePhoneNumber(false);
    }
}