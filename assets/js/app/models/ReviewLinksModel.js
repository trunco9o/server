import Model from './Model';

export default class ReviewLinksModel extends Model {
    constructor(initLink){
        super();

        let link;

        if(!initLink){
            link = this._makeLink();
        }
        else{
            link = this._formatLink(initLink);
        }

        this._actions = {
            all: '/reviewLinks',
            create: '/reviewLinks',
            delete: id => `/reviewLinks/${id}`,
            update: id => `/reviewLinks/${id}`
        }

        this._model = {...link};
    }

    _broadcastChange(){
        this.emit('change', this.getLink());
    }

    _formatLink(link){
        if(link._id){
            link.id = link._id['$oid'];
        }

        return link;
    }

    _makeLink(){
        return {
            id: null,
            site: '',
            url: ''
        };
    }

    getLink(){
        return {...this._model};
    }

    async delete(){
        const fetchConfig = this._getFetchConfig(this._methods.delete);

        const res = await fetch(this._actions.delete(this._model.id), fetchConfig);

        const json = await res.json();

        return json;
    }

    async save(){
        const id = this._model.id;
        const action = id ? this._actions.update(id) : this._actions.create;
        const method = id ? this._methods.put : this._methods.post;

        const fetchConfig = this._getFetchConfig(method);

        fetchConfig.body = JSON.stringify(this._model);

        const res = await fetch(action, fetchConfig);

        const json = await res.json();

        this._model = this._formatLink(json);

        return this;
    }

    setSite(site){
        this._model.site = site;

        this._broadcastChange();
    }

    setUrl(url){
        this._model.url = url;

        this._broadcastChange();
    }
}