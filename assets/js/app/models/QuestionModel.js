import Model from './Model';

export default class QuestionModel extends Model {
    _model;
    _action;
    _method;

    constructor(initQuestion){
        super();

        let question;

        if(!initQuestion){
            question = this._makeQuestion();
        }
        else{
            question = this._formatQuestion(initQuestion);
        }

        this._actions = {
            all: '/question',
            create: '/question',
            delete: id => `/question/${id}`,
            update: id => `/question/${id}`
        }

        this._model = {...question};
    }

    _broadcastChange(){
        this.emit('change', this);
    }

    _formatQuestion(question){
        if(question._id){
            question.id = question._id.$oid;
        }

        return question;
    }

    _makeQuestion(){
        return {
            id: null,
            text: '',
            type: 'open-ended',
            scale: 5,
            indicator: {
                isIndicator: false,
                positiveCutoff: 3,
                higherIsGood: true,
                booleanIndicator: 'yes'
            }
         };
    }

    getQuestion(){
        return {...this._model};
    }

    async delete(){
        const fetchConfig = this._getFetchConfig(this._methods.delete);

        const res = await fetch(this._actions.delete(this._model.id), fetchConfig);

        const json = await res.json();

        return json;
    }

    async save(){
        const id = this._model.id;
        const action = id ? this._actions.update(id) : this._actions.create;
        const method = id ? this._methods.put : this._methods.post;

        const fetchConfig = this._getFetchConfig(method);

        fetchConfig.body = JSON.stringify(this._model);

        const res = await fetch(action, fetchConfig);

        const json = await res.json();

        this._model = this._formatQuestion(json);

        return this;
    }

    setBooleanIndicator(indicator){
        this._model.indicator.booleanIndicator = indicator;

        this._broadcastChange();
    }

    setIsIndicator(isIndicator){
        this._model.indicator.isIndicator = isIndicator;

        this._broadcastChange();
    }

    setPositiveCutoff(positiveCutoff){
        this._model.indicator.positiveCutoff = positiveCutoff;

        this._broadcastChange();
    }

    setRangeIndicator(indicator){
        this._model.indicator.higherIsGood = indicator;

        this._broadcastChange();
    }

    setScale(scale){
        let newScale = scale;

        if(typeof(newScale) === 'string')
            newScale = parseInt(newScale);

        this._model.scale = newScale;

        this._broadcastChange();
    }

    setText(text){
        this._model.text = text;

        this._broadcastChange();
    }

    setType(type){
        this._model.type = type;

        this._broadcastChange();
    }

    validate(){
        const question = this._model;

        const errors = {
            indicators: {}
        };

        const isIndicator = question.indicator.isIndicator;

        if(!question.text){
            errors['text'] = 'Please add a question.';
        }

        if(!question.type){
            errors['type'] = 'Please add a type.';
        }

        if(question.type === 'range' && !question.scale){
            errors['range'] = 'Please add a range.';
        }

        const isRangeIsIndicator = isIndicator && question.type === 'range';

        if(isRangeIsIndicator && !question.indicator.positiveCutoff){
            errors.indicators['positiveCutoff'] = 'Please add a cutoff for a negative review';
        }
        
        if(isRangeIsIndicator && typeof(question.indicator.higherIsGood) !== 'boolean'){
            errors.indicators['higherIsGood'] = 'Please select how to determine if this is a positive review.';
        }

        if(isIndicator && question.type === 'open-ended'){
            question.indicator.isIndicator = false;
        }

        if(isIndicator && question.type === 'yes-no' && 
            (question.indicator.booleanIndicator !== 'yes' && question.indicator.booleanIndicator !== 'no') )
        {
            question.indicator.booleanIndicator = null;
            errors.indicators['booleanIndicator'] = 'Please select how to determine if this is a positive review.';
        }

        if(Object.keys(errors).length > 1 || Object.keys(errors.indicators).length){
            return errors;
        }

        return true;
    }
}