import React from 'react';
import ReactDOM  from 'react-dom';

import Dashboard from './components/dashboard.component';

import SurveyModel from './models/SurveyModel';
import UserModel from './models/UserModel';
import ReviewModel from './models/ReviewModel';

const element = document.getElementById('container');

const App = (
        <Dashboard surveyModel={new SurveyModel()}
                   userModel={new UserModel(true)}
                   reviewModel={new ReviewModel(true)}
        />
);

ReactDOM.render(App, element);