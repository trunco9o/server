import React from 'react';
import ReactDOM  from 'react-dom';
import Demo from './components/demo.component';

import injectTapEventPlugin from "react-tap-event-plugin";

injectTapEventPlugin();

var element = document.getElementById('container');

ReactDOM.render(<Demo/>, element);