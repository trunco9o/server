import moment from 'moment';

export default function getLocalDateString(date, format='M/D/YYYY'){
    return moment.utc(date).local().format(format);
}