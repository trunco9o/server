const key = 'lastState';

export default {
    getLastState(){
        return window.localStorage.getItem(key);
    },
    setLastState(state){
        window.localStorage.setItem(key, state);
    }
}