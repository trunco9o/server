export default function formatReviewLinks(reviewLinks){
    if(!reviewLinks)
        return;

    return reviewLinks.map(reviewLink => `${reviewLink._model.site}: ${reviewLink._model.url}`).join('\n');
}