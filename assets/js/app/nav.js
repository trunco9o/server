(function(){
    var mobileNav = document.getElementById('mobile-nav');
    var navButton = document.getElementById('nav-button');

    navButton.onclick = navToggle;

    function navToggle(){
        if(!mobileNav.classList.contains('active')){
            navButton.classList.add('change');
            mobileNav.classList.add('active');
        }
        else{
            mobileNav.classList.remove('active');
            navButton.classList.remove('change');
        }
    }

    window.onresize = function(){
        var greaterThan = window.innerWidth > 854;

        if(greaterThan && mobileNav.classList.contains('active')){
            mobileNav.classList.remove('active');
        }

        if(greaterThan && navButton.classList.contains('change')){
            navButton.classList.remove('change');
        }
    }
})()