import React, { Component } from 'react';
import EventEmitter from 'Events';

import stateService from '../services/dashboardState';

import { withStyles } from '@material-ui/core/styles';

import {fadeIn, fadeOut} from 'cssinjs-animations';

import PropTypes from 'prop-types';

import classnames from 'classnames';

import NavBar from '../components/shared/navbar.component';
import Menu from './dashboard/menu.component';
import QuickView from './dashboard/quickview.component';
import UserInfo from './dashboard/userInfo.component';
import Usage from './dashboard/usage.component';
import Survey from './dashboard/survey/survey.component';
import Reviews from './dashboard/reviews.component';
import CustomerList from './dashboard/customerList/customerList.component';
import Settings from './dashboard/settings/settings.component';
import ReviewVerification from './dashboard/reviewVerification/reviewVerification.component';
import ReviewVisibilityHandlerComponent from './shared/reviews/reviewVisibilityHandler.component';
import SMSMarketing from './dashboard/smsMarketing.component';

const styles = theme => ({
    root: {
      flexGrow: 1,
      height: '100vh',
      zIndex: 1,
      overflow: 'hidden',
      position: 'relative',
      display: 'flex',
      '&$selected': { 
        backgroundColor: theme.palette.action.selected, 
      }, 
    },
    content: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.default,
      padding: theme.spacing.unit * 3,
      minWidth: 0, // So the Typography noWrap works
      overflowX: 'scroll'
    },
    toolbar: theme.mixins.toolbar,
    '@keyframes fadeOut': fadeOut,
    fadeOut: {
        animationName: 'fadeOut',
        animationDuration: '0.3s'
    },
    '@keyframes fadeIn': fadeIn,
    fadeIn: {
        animationName: 'fadeIn',
        animationDuration: '0.3s'
    },
    '@media screen and (max-width: 600px)': {
        content: {
            padding: `${theme.spacing.unit * 3}px 0px`
        }
    }
});

class Dashboard extends Component{
    toggleMenuEventEmitter;
    updateMenuCallback;
    toggleMenuCallback;

    constructor(props){
        super(props);

        this.updateMenuCallback = () => this.updateMenu();
        this.toggleMenuCallback = () => this.toggleMenu();

        let page = stateService.getLastState();

        this.toggleMenuEventEmitter = new EventEmitter();
        this.toggleMenuEventEmitter.on('toggle', this.toggleMenuCallback);

        if(!page)
            page = 'quickview';

        const mobile = (window.innerWidth <= 960);

        this.state = {
            open: false,
            page: page,
            reviews: this.props.reviewModel.searchReviews({questions: true}),
            user: this.props.userModel.getUser(),
            showButton: mobile,
            menuOpen: !mobile
        };

        this.props.userModel.on('change', change => {
            this.setState({
                ...this.state,
                user: change.user
            });
        });

        this.props.reviewModel.on('change', change => {
            this.setState({
                ...this.state,
                reviews: change.reviews
            });
        });
    }

    componentDidMount(){
        window.addEventListener('resize', this.updateMenuCallback);
    }

    componentWillUnmount(){
        window.removeEventListener('resize', this.updateMenuCallback);
        this.toggleMenuEventEmitter.removeEventListener('toggle', this.toggleMenuCallback)
    }

    updateMenu(){
        const width = window.innerWidth;
        
        const state = {...this.state};
        
        const menuOpen = (width >= 960);

        state.menuOpen = menuOpen;
        state.showButton = !menuOpen;

        this.setState(state);
    }

    toggleMenu(){
        const {menuOpen} = this.state;

        this.setState({
            ...this.state,
            menuOpen: !menuOpen
        });
    }

    render(){
        const {classes} = this.props;
        const {transitionClass, page, showButton, menuOpen} = this.state;

        const title = (
            <div>
                <img style={{height: '50px'}} src="/static/images/buddah.svg"/>
                <span>RepGuru</span>
            </div>
        );

        const mainClass = transitionClass ? classnames([transitionClass, classes.content]) : classes.content;

        return ( 
            <div className={classes.root}>
                <NavBar title="RepGuru" toggleMenuEventEmitter={this.toggleMenuEventEmitter} showButton={showButton} isLoggedIn={true}/>
                <Menu menuClick={(page) => this.changePageStart(page)} selected={page} toggleMenuEventEmitter={this.toggleMenuEventEmitter} open={menuOpen}/>
                <main className={mainClass}>
                    <div className={classes.toolbar} />
                    {this.getComponent()}
                </main>
            </div>
        );
    }

    changePageStart(page){
        if(page === this.state.page)
            return;

        const {classes} = this.props;

        this.setState({
            ...this.state,
            transitionClass: classes.fadeOut
        });

        setTimeout(() => this.changePage(page), 299);
    }

    changePage(page){
        const {classes} = this.props;

        stateService.setLastState(page);

        this.setState({
            page: page,
            open: false,
            transitionClass: classes.fadeIn
        });
    }

    getComponent(){
        if(this.state.page === 'quickview'){
            return <QuickView user={this.state.user} 
                              reviews={Object.entries(this.state.reviews).slice(0, 5).map(entry => entry[1])}
                    />
        }
        else if(this.state.page === 'user-info'){
            return <UserInfo user={this.state.user} errors={this.props.userModel.getErrors()} save={user => this.props.userModel.send(user)}/>
        }
        else if(this.state.page === 'usage'){
            return <Usage getAggregations={() => this.props.reviewModel.fetchAggregations()}/>
        }
        else if(this.state.page === 'survey'){
            return <Survey 
                surveyModel={this.props.surveyModel}
                questionModel={this.props.questionModel}
                businessName={this.state.user.business}
            />
        }
        else if(this.state.page === 'reviews'){
            return (
                <ReviewVisibilityHandlerComponent>
                    <Reviews reviews={this.state.reviews} search={(start, end) => this.searchReviews({start: start, end: end})}/>
                </ReviewVisibilityHandlerComponent>
            );
        }
        else if(this.state.page === 'contact-list'){
            return <CustomerList/>
        }
        else if(this.state.page === 'review-verification'){
            return <ReviewVerification search={val => this.props.reviewModel.query(val)}/>
        }
        else if(this.state.page === 'settings'){
            return <Settings settings={this.state.user.settings} save={settings => this.saveSettings(settings)}/>
        }
        else if(this.state.page === 'sms-marketing'){
            return <SMSMarketing/>
        }
        else{
            return <div></div>
        }
    }

    saveSettings(settings){
        console.log(settings);
        const {user} = this.state;

        user.settings = settings;

        this.props.userModel.send(user);
    }

    searchReviews(start, end){
        this.props.reviewModel.searchReviews({start: start, end: end});
    }
}

export default withStyles(styles)(Dashboard);

Dashboard.propTypes = {
    userModel: PropTypes.object.isRequired,
    surveyModel: PropTypes.object.isRequired,
    reviewModel: PropTypes.object.isRequired
}