// @flow
import React, { Component } from 'react';

import NavBar from '../components/shared/navbar.component';

import Message from '../components/shared/message.component';
import { errorStyle } from '../components/shared/message.styles';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core';

type Props = {
    classes: Object;
    business: Object;
};

type State = {
    comments: string;
    success: boolean|void;
}

const styles = theme => ({
    root: {
        flexGrow: 1,
        height: '100vh',
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    paper: {
        padding: '20px',
        width: '80vw',
        height: '75vh'
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
        minWidth: 0, // So the Typography noWrap works
        overflowX: 'scroll'
    },
    button: {
        marginTop: '20px'
    }
});

class DetailedReview extends Component<Props, State> {
    constructor(props: Props){
        super(props);

        this.state = {
            comments: '',
            success: undefined
        };
    }

    render(){
        const {classes, business} = this.props;
        const {comments, success} = this.state;

        return (
            <div className={classes.root}>
                <NavBar title={business.name} isLoggedIn={false} color={business.color}/>
                <main className={classes.main}>
                    <Paper elevation={8} className={classes.paper}>
                        <Typography variant="title">
                            Tell us more about your experience
                        </Typography>
                        {this.getMessage(success)}
                        {this.getForm(comments, classes, business, success)}
                    </Paper>
                </main>
            </div>
        );
    }

    getForm(comments: string, classes: Object, business: Object, success: boolean|void){
        if(success === true)
            return;
        
        return (
            <div>
                <TextField
                    multiline
                    name="comments"
                    id="comments"
                    margin="normal"
                    label="Comments"
                    fullWidth={true}
                    rows={15}
                    value={comments}
                    onChange={e => this.setState({comments: e.target.value})}
                />
                <Button variant="raised" 
                        style={{backgroundColor: business.color, color: '#fff'}} 
                        className={classes.button}
                        onClick={() => this.send()}>
                    Send
                </Button>
            </div>
        );
    }

    getMessage(success: boolean|void){
        if(success === true){
            return <Message message="Thank you for your review!" margin="15px"/>
        }
        else if (success === false){
            return <Message message="There was an error, please try again" stylePack={errorStyle} margin="15px"/>
        }
        else {
            return '';
        }
    }

    async send(){
        const {comments} = this.state;

        const headers = new Headers();

        headers.set('Content-Type', 'application/json');

        const fetchConfig = {
            credentials: 'same-origin',
            headers: headers,
            mode: 'cors',
            method: 'POST',
            body: JSON.stringify({comments: comments})
        };

        const res = await fetch('', fetchConfig);

        const json = await res.json();

        this.setState({
            ...this.state,
            success: json.success
        });
    }
}

export default withStyles(styles)(DetailedReview);