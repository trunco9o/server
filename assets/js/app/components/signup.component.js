import React, { Component } from 'react';

import UserModel from '../models/UserModel';

import '../../../sass/partials/forms.sass';
import '../../../sass/partials/buttons.sass';

export default class Signup extends Component {
    constructor(props){
        super(props);

        this.userModel = new UserModel();

        this.userModel.on('change', this.updateState.bind(this));

        this.state = {
            user: this.userModel.getUser(),
            errors: this.userModel.getErrors()
        };
    }

    render(){
        return <div>
            <div className={this.state.success ? '' : 'hidden'}>
                <h2>Success! Please check your email for a verification link.</h2>
            </div>
            <div className={this.state.success ? 'hidden' : ''}>
                <div className="form-group">
                    <label htmlFor="name">First and Last Name</label>
                    <input type="text" 
                        name="name" 
                        value={this.state.user.name} 
                        onChange={e => this.userModel.setName(e.target.value)}
                    />
                    <p className={this.state.errors.name ? '' : 'hidden'}>
                        {this.state.errors.name}
                    </p>
                </div>

                <div className="form-group">
                    <label htmlFor="email">Email</label>
                    <input type="email" 
                        name="email" 
                        value={this.state.user.email}
                        onChange={e => this.userModel.setEmail(e.target.value)}
                    />
                    <p className={this.state.errors.email ? '' : 'hidden'}>
                        {this.state.errors.email}
                    </p>
                </div>

                <div className="form-group">
                    <label htmlFor="business">Business Name</label>
                    <input type="email" 
                        name="email" 
                        value={this.state.user.businessName} 
                        onChange={e => this.userModel.setBusinessName(e.target.value)}
                    />
                    <p className={this.state.errors.businessName ? '' : 'hidden'}>
                        {this.state.errors.businessName}
                    </p>
                </div>

                <div className="form-group">
                    <label htmlFor="phone">Business Phone Number</label>
                    <input type="phone" 
                        name="phone" 
                        value={this.state.user.phoneNumber}
                        onChange={e => this.userModel.setPhoneNumber(e.target.value)}
                    />
                    <p className={this.state.errors.phoneNumber ? '' : 'hidden'}>
                        {this.state.errors.phoneNumber}
                    </p>
                </div>
                
                <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input type="password" 
                        name="password" 
                        value={this.state.user.password} 
                        onChange={e => this.userModel.setPassword(e.target.value)}
                    />
                    <p className={this.state.errors.password ? '' : 'hidden'}>
                        {this.state.errors.password}
                    </p>
                </div>

                <div className="form-group">
                    <label htmlFor="confirm-password">Confirm Password</label>
                    <input type="password" 
                        name="confirm-password" 
                        value={this.state.user.confirmPassword} 
                        onChange={e => this.userModel.setPasswordConfirmation(e.target.value)}
                    />
                    <p className={this.state.errors.passwordConfirmation ? '' : 'hidden'}>
                        {this.state.errors.passwordConfirmation}
                    </p>
                </div>

                <button className="button" onClick={() => this.submit()}>Sign Up!</button>
            </div>
        </div>
    }

    updateState(values){
        this.setState(values);
    }

    submit(){
        this.userModel.send()
            .then(json => {
                this.setState({
                    ...this.state,
                    success: true
                });
            })
            .catch(error => {});
    }
}