import React, { Component } from 'react';

import Messenger from '../components/shared/messenger/messenger.component';
import Customizer from './dashboard/survey/components/customizer/customizer.component';

import MockSurveyModel from '../models/mocks/MockSurveyModel';

import '../../../sass/partials/demo/demo.sass';

export default class Demo extends Component {

    constructor(props){
        super(props);

        this._mockSurvey = new MockSurveyModel();

        this._mockSurvey.on('surveyChange', this.resetState.bind(this));
        this._mockSurvey.messages.on('messageChange', this.updateMessages.bind(this));

        this.state = {
            customizerCollapsed: true,
            messages: this._mockSurvey.messages.getMessages(),
            survey: this._mockSurvey.getSurvey()
        }
    }

    componentDidMount() {
        window.addEventListener('resize', this.collapseCustomizer.bind(this));
    }

    componentWillUnmount(){
        window.removeEventListener('resize', this.collapseCustomizer)
    }
    
    render(){
        return <div id="demo">
            <h1 className="text-center">Try RepGuru!</h1>
            <div className="right">

                <h2 onClick={this.toggleCollapse.bind(this)}>
                    <span>Customize Demo &nbsp;</span>
                    <i className="arrow-down"></i>
                </h2>

                <div className={this.state.customizerCollapsed ? 'mobile-hidden' : ''}>
                    <Customizer survey={this.state.survey}
                                addLink={link => this._mockSurvey.addLink(link)}
                                addQuestion={question => this._mockSurvey.addQuestion(question)}
                                onTriggerChange={trigger => this._mockSurvey.setTrigger(trigger)}
                                removeLink={index => this._mockSurvey.removeLink(index)}
                                removeQuestion={index => this._mockSurvey.removeQuestion(index)}
                                resetSurvey={() => this._mockSurvey.resetSurvey(true)}
                                updateInvalidMessage={message => this._mockSurvey.updateInvalidReviewMessage(message)}
                                updateLink={(index, link) => this._mockSurvey.updateLink(index, link)}
                                updateNegativeMessage={message => this._mockSurvey.updateNegativeReviewMessage(message)}
                                updatePositiveCutoff={cutoff => this._mockSurvey.updatePositiveCutoff(cutoff)}
                                updatePositiveMessage={message => this._mockSurvey.updatePositiveReviewMessage(message)}
                                updateQuestion={(index, question) => this._mockSurvey.updateQuestion(index, question)}
                    />
                </div>
            </div>
            <div className="left">
                <h2>Demo</h2>
                <ol>
                    <li>Type in "{this.state.survey.trigger}" and hit 'Enter' or the send button</li>
                    <li>Complete the survey</li>
                </ol>
                <button className="button button-red" onClick={() => this._mockSurvey.clearMessages()}>Reset</button>
                <Messenger messages={this.state.messages}
                           addMessage={message => this._mockSurvey.addMessage(message)}
                />
            </div>
            <div className="clearfix"></div>
        </div>
    }

    collapseCustomizer(){
        if(window.innerWidth > 1199 && !this.state.customizerCollapsed){
            this.toggleCollapse();
        }
    }

    resetState(survey){        
        return this.setState({
            ...this.state,
            survey: survey
        });
    }

    toggleCollapse(){
        const collapse = this.state.customizerCollapsed;

        return this.setState({
            ...this.state,
            customizerCollapsed: !collapse
        });
    }

    updateMessages(messages){
        return this.setState({
            ...this.state,
            messages: messages
        });
    }
}
