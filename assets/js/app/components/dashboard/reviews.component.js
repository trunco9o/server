import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import EnhancedTable from './table/table.component';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import moment from 'moment';

import classNames from 'classnames';

const styles = theme => ({
    title: {
        paddingTop: '20px'
    },
    header: {
        marginBottom: '15px'
    },
    firstInput: {
        marginRight: '15px'
    },
    paper: {
        marginBottom: '50px'
    },
    paddedPaper: {
        padding: '20px'
    }
});

const headerData = [
    {id: 'created', label: 'Created'},
    {id: 'status', label: 'Status'},
    {id: 'completed', label: 'Completed'}
];

class Reviews extends Component{
    constructor(props){
        super(props);

        this.state = {
            startDate: null,
            endDate: null
        }
    }

    render(){
        const {status, tableData} = this.getTableData();

        const {minDate, maxDate} = this;
        const {startDate, endDate} = this.state;

        const inputStartDate = startDate ? startDate : minDate;
        const inputEndDate = endDate ? endDate : maxDate;

        const {classes} = this.props;

        return (
            <div>
                <Paper elevation={8} className={classes.paper}>
                    <EnhancedTable title={this.getTitle(inputStartDate, inputEndDate)} 
                                   headerData={headerData} tableData={tableData} 
                                   exportClick={(e, selected, all) => this.exportClick(e, selected, all)}
                                   viewClick={reviewId => this.viewClick(reviewId)}   
                                />
                </Paper>
            </div>
        );
    }

    viewClick(id){
        const {reviews, showReview} = this.props;

        const review = reviews[id];

        return this.props.showReview(review);
    }

    getTableData(){
        const {reviews} = this.props;

        const tableData = [];

        let status;

        for(let [id, review] of Object.entries(reviews)){
            if(review.positive){
                status = 'Positive';
            }
            else if(review.positive === false){
                status = 'Negative';
            }
            else if(review.invalid){
                status = 'Invalid';
            }
            else{
                status = 'N/A';
            }

            const created = moment.utc(review.created.$date).local();

            if(!this.minDate || created < moment(this.minDate))
                this.minDate = created.format('YYYY-MM-DD');

            if(!this.maxDate || created > moment(this.maxDate))
                this.maxDate = created.format('YYYY-MM-DD');

            tableData.push({
                created: created.format('M/D/Y'),
                status: status,
                completed: review.completed ? 'Yes' : 'No',
                id: review._id.$oid
            });
        }

        if(!tableData.length){
            const today = moment().format('YYYY-MM-DD');

            this.minDate = today;
            this.maxDate = today;
        }

        return {
            tableData: tableData,
            status: status
        };
    }

    getTitle(minDate, maxDate){
        const {classes} = this.props;

        return (
            <div className={classes.title}>
                <Typography className={classes.header} variant="title">Reviews</Typography>

                <TextField
                    label="From"
                    type="date"
                    value={minDate}
                    className={classes.firstInput}
                    onChange={e => this.updateFromDate(e.target.value)}
                />
                <TextField
                    label="To"
                    type="date"
                    value={maxDate}
                    onChange={e => this.updateToDate(e.target.value)}
                />

            </div>
        );
    }

    exportClick(e, selected, all){
        let url = window.location.origin;

        url += '/reviews/download';
        
        if(!all) {
            const selectedString = JSON.stringify(selected);
            const encoded = encodeURIComponent(selectedString);

            const search = `?selected=${encoded}`;

            url += search;
        }
        
        window.location.replace(url);
    }

    updateFromDate(date){
        let {endDate} = this.state;

        if(!endDate && !this.maxDate){
            endDate = moment().format('YYYY-MM-DD');
        } else if(this.maxDate){
            endDate = this.maxDate
        }

        this.setState({
            endDate: endDate,
            startDate: date
        });

        this.props.search(date, endDate);
    }

    updateToDate(date){
        let {startDate} = this.state;

        if(!startDate && !this.minDate){
            startDate = moment().format('YYYY-MM-DD');
        } else if(this.minDate){
            startDate = this.minDate
        }

        this.setState({
            startDate: startDate,
            endDate: date
        });

        this.props.search(startDate, date);
    }

}

Reviews.propTypes = {
    reviews: PropTypes.object
};

export default withStyles(styles)(Reviews);