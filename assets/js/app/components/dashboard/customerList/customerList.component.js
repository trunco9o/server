// @flow

import React, { Component } from 'react';

import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import CustomerTable from './components/customerTable.component';
import CustomerTabs from './components/customerTabs.component';

import ContactModel from '../../../models/ContactModel';

const styles = theme => ({
    paper: {
        marginBottom: '25px',
        padding: '20px'
    },
    input: {
        marginBottom: '15px'
    },
    uploadContainer: {
        margin: '15px 0px'
    }
});

type Props = {
    classes: Object
}

type State = {
    customers: Array<Object>,
    which: string,
    name: string,
    phone: string,
    email: string
}

class CustomerList extends Component<Props, State> {
    _contactModel: ContactModel;


    constructor(props){
        super(props);

        this._contactModel = new ContactModel();

        this.state = {
            customers: [],
            which: 'all',
            name: '',
            phone: '',
            email: ''
        };
    }

    componentDidMount(){
        this._loadCustomers();
    }

    render(){
        const {classes} = this.props;
        const tabs = <CustomerTabs tabClick={which => this.tabClick(which)} value={this.state.which}/>;

        return (
            <div>
                <Paper elevation={8} classes={{root: classes.paper}} style={{paddingBottom: '50px'}}>
                    <CustomerTable title={tabs} customers={this.state.customers} exportClick={(event, selected, all) => this.exportClick(event, selected, all)}/>
                </Paper>

                <Paper elevation={8} classes={{root: classes.paper}}>
                    <Typography variant="title">
                       Upload Customer List
                    </Typography>

                    <div className={classes.uploadContainer}>
                        <div>
                            <label htmlFor="file">
                                Upload a CSV or Excel file (<a href="/static/template.csv">Template</a>)
                            </label>
                        </div>
                        <div>
                            <input type="file" name="file" accept=".csv, .xslx, .xls"/>
                        </div>
                    </div>

                    <div>
                        <Button variant="raised" color="primary">Upload</Button>
                    </div>
                </Paper>

                <Paper elevation={8} classes={{root: classes.paper}}>
                    <Typography variant="title">
                        Create Customer
                    </Typography>

                    <div className={classes.input}>
                        <TextField label="Name" value={this.state.name} onChange={e => this.setState({...this.state, name: e.target.value})}/>
                    </div>

                    <div className={classes.input}>
                        <TextField label="Phone Number" value={this.state.phone} onChange={e => this.setState({...this.state, phone: e.target.value})}/>
                    </div>

                    <div className={classes.input}>
                        <TextField label="Email Address" value={this.state.email} onChange={e => this.setState({...this.state, email: e.target.value})}/>
                    </div>

                    <Button variant="raised" color="primary" onClick={() => this.save()}>Save</Button>
                </Paper>
            </div>
        );
    }

    _loadCustomers(){
        this.tabClick('all');
    }

    exportClick(event, selected, all){
        let url = window.location.origin;

        url += `/customers/download/${this.state.which}`;
        
        if(!all) {
            const selectedString = JSON.stringify(selected);
            const encoded = encodeURIComponent(selectedString);

            const search = `?selected=${encoded}`;

            url += search;
        }
        
        window.location.replace(url);
    }

    tabClick(which: string){

        this.setState({
            customers: [],
            which: which
        });

        return this._contactModel.getCustomers(which)
            .then((customers) => {
                this.setState({
                    ...this.state,
                    customers: customers
                });
            });
    }

    async save() {
        const {name, phone, email} = this.state;

        await this._contactModel.saveCustomer({
            name: name,
            phone: phone,
            email: email
        });

        this.setState({
            ...this.state,
            name: '',
            phone: '',
            email: ''
        });
    }
}

export default withStyles(styles)(CustomerList);