import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

const styles = {
    root: {
      flexGrow: 1,
    },
};

const tabMap = ['all', 'reviewers', 'uploaded']

class CustomerTabs extends Component {

    constructor(props){
        super(props);
    }

    render(){
        const {classes} = this.props;

        return (
            <Paper className={classes.root}>
                <Tabs value={tabMap.indexOf(this.props.value)}
                      onChange={(event, value) => this.props.tabClick(tabMap[value])}
                      indicatorColor="primary"
                      textColor="primary"
                      centered
                >
                    <Tab label="All Customers" />
                    <Tab label="RepGuru Customers" />
                    <Tab label="Uploaded Customers" />
                </Tabs>
            </Paper>
        );
    }
}

export default withStyles(styles)(CustomerTabs);