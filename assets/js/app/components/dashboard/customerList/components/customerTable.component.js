import React, { Component } from 'react';

import EnhancedTable from '../../table/table.component';

const headerData = [
    {id: 'name', label: 'Name'},
    {id: 'phone', label: 'Phone'},
    {id: 'email', label: 'Email'}
];

export default class CustomerTable extends Component {

    constructor(props){
        super(props);
    }

    render(){
        const tableData = this.getTableData();

        return (
            <EnhancedTable title={this.props.title} headerData={headerData} tableData={tableData} exportClick={this.props.exportClick}/>
        );
    }

    getTableData(){
        return this.props.customers.map(customer => {
            return {
                name: customer.name ? customer.name : 'N/A',
                phone: customer.phoneNumber ? customer.phoneNumber : 'N/A',
                email: customer.email ? customer.email : 'N/A',
                id: customer._id.$oid
            };
        });
    }
}
