import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Messenger from '../../../shared/messenger/messenger.component';

import MockSurveyModel from '../../../../models/mocks/MockSurveyModel';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import {withStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
    paper: {
        padding: '30px',
        marginBottom: '20px'
    }
});

class TryIt extends Component {
    _surveyChangeListener;
    _messageChangeListener;

    constructor(props){
        super(props);

        this._mockSurvey = new MockSurveyModel(props.survey);

        this._surveyChangeListener = this.resetState.bind(this);
        this._messageChangeListener = this.updateMessages.bind(this);
        
        this._mockSurvey.on('surveyChange', this._surveyChangeListener);
        this._mockSurvey.messages.on('messageChange', this._messageChangeListener);

        this.state = {
            messages: this._mockSurvey.messages.getMessages(),
            survey: this._mockSurvey.getSurvey()
        }
    }

    componentWillReceiveProps(props){
        this._mockSurvey = new MockSurveyModel(props.survey);

        this._mockSurvey.on('surveyChange', this._surveyChangeListener);
        this._mockSurvey.messages.on('messageChange', this._messageChangeListener);

        this.setState({
            messages: this._mockSurvey.messages.getMessages(),
            survey: this._mockSurvey.getSurvey()
        });
    }

    render(){
        const {classes} = this.props;

        return (
            <Paper elevation={8} className={classes.paper}>
                <Grid container spacing={16}>
                    <Grid item xs={12} md={6}>
                        <Typography variant="title" gutterBottom>Try It</Typography>
                        <Messenger messages={this.state.messages}
                           addMessage={message => this._mockSurvey.addMessage(message)}/>
                    </Grid>
                    <Grid item xs={12} md={6} style={{marginTop: '50px'}}>
                        <div>
                            <Typography variant="subheading">
                                <ol style={{paddingLeft: '15px'}}>
                                    <li style={{marginBottom: '25px'}}>Start with your trigger</li>
                                    <li style={{marginBottom: '25px'}}>Complete your questions</li>
                                    <li>Hit the reset button to start over at any point</li>
                                </ol>
                            </Typography>
                        </div>
                        <div>
                            <Button variant="raised" color="secondary">Reset</Button>
                        </div>
                    </Grid>
                </Grid>
            </Paper>
        );
    }

    resetState(survey){        
        return this.setState({
            ...this.state,
            survey: survey
        });
    }

    updateMessages(messages){
        return this.setState({
            ...this.state,
            messages: messages
        });
    }
}

export default withStyles(styles)(TryIt);