import React, { Component } from 'react';
import PropTypes from 'prop-types';

import MockSurveyModel from '../../../../models/mocks/MockSurveyModel';

import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import {withStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';

import {CirclePicker, SliderPicker} from 'react-color';

const styles = theme => ({
    paper: {
        padding: '30px',
        marginBottom: '20px'
    },
    innerPaper: {
        padding: '20px'
    },
    pickerContainer: {
        marginTop: '20px',
        marginBottom: '20px',
    },
    flex: {
        flex: 1,
    },
    buttonGroup: {
        marginTop: '20px',
        '& button:first-child': {
            marginRight: '10px'
        }
    }
});

class ReviewPage extends Component {
    constructor(props){
        super(props);

        this.state = {
            color: props.reviewColor || '#3f51b5'
        };

    }

    componentWillReceiveProps(props){
        this.setState({
            color: props.reviewColor
        });
    }

    render(){
        const {classes} = this.props;
        return (
            <Paper elevation={8} className={classes.paper}>
                <Typography variant="title" gutterBottom>Customize Detailed Review Page</Typography>

                <Grid container spacing={24} className={classes.pickerContainer}>
                    <Grid item sm={12} md={3}>
                        <CirclePicker 
                            color={this.state.color}
                            onChangeComplete={ color => this.handleChangeComplete(color) }
                        />
                    </Grid>
                    <Grid item sm={12} md={6}>
                        <SliderPicker 
                            color={this.state.color}
                            onChangeComplete={ color => this.handleChangeComplete(color) }
                        />
                    </Grid>
                </Grid>

                <div>
                    <div>
                        <AppBar position="static" style={{backgroundColor: this.state.color}}>
                            <Toolbar>
                                <Typography variant="title" color="inherit" className={classes.flex}>
                                    {this.props.businessName}
                                </Typography>
                            </Toolbar>
                        </AppBar>
                        <Paper elevation={8} className={classes.innerPaper}>
                            <Typography variant="title">
                                Tell us more about your experience
                            </Typography>
                        </Paper>
                    </div>
                </div>
                <div className={classes.buttonGroup}>
                    <Button variant="raised" color="primary" onClick={() => this.props.save(this.state.color)}>
                        Save
                    </Button>
                    <Button variant="raised" color="secondary" onClick={() => this.props.save('#3f51b5')}>
                        Reset
                    </Button>
                </div>
            </Paper>
        );
    }

    handleChangeComplete(color){
        this.setState({
            color: color.hex
        });
    }
}

export default withStyles(styles)(ReviewPage);