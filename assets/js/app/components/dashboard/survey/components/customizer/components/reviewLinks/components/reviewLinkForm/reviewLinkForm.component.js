import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ReviewLinkInputs from './components/reviewLinkInputs.component';

import { withStyles } from '@material-ui/core';
import Button from '@material-ui/core/Button';

const styles = theme => ({
    buttonGroup: {
        'margin-top': '15px',
        '& button:first-child': {
            marginRight: '10px'
        }
    }
});

class ReviewLinkForm extends Component {

    constructor(props){
        super(props);

        const {link} = props;

        this.linkChangeCallback = link => this.setState({link: link});

        link.on('change', this.linkChangeCallback);

        const linkObject = link.getLink();

        this.state = {
            link: linkObject
        };
    }

    componentWillReceiveProps(nextProps){
        const {link} = this.props;

        link.removeListener('change', this.linkChangeCallback);

        const newLink = nextProps.link;

        newLink.on('change', this.linkChangeCallback);

        const nextPropsLink = newLink.getLink();

        if(nextPropsLink !== this.state.link) {
            this.setState({link: nextPropsLink});
        }
    }

    componentWillUnmount(){
        const {link} = this.props;
        
        link.removeListener('change', this.linkChangeCallback);
    }

    render(){
        const {link} = this.state;
        const {classes} = this.props;

        const {site, url} = link;

        return (
            <div id="review-link-form">
                <ReviewLinkInputs 
                    change={(type, value) => this.updateLink(type, value)}
                    site={site}
                    url={url}
                />
                <div className={classes.buttonGroup}>
                    <Button variant="raised" color="primary" onClick={() => this.props.save()}>
                        Save
                    </Button>
                    <Button variant="raised" color="secondary" onClick={() => this.props.clear()}>
                        Clear
                    </Button>
                </div>
            </div>
        );
    }

    updateLink(type, value){
        const {link} = this.props;
        
        if(type === 'site'){
            link.setSite(value);
        }
        else if(type === 'url'){
            link.setUrl(value);
        }
    }
}

ReviewLinkForm.propTypes = {
    link: PropTypes.object.isRequired,
    save: PropTypes.func.isRequired,
    clear: PropTypes.func.isRequired
};

export default withStyles(styles)(ReviewLinkForm);