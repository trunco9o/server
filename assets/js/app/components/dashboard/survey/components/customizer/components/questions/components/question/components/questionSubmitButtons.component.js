import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {withStyles} from '@material-ui/core';
import Button from '@material-ui/core/Button';

const styles = theme => ({
    buttonGroup: {
        'margin-top': '15px',
        '& button:first-child': {
            marginRight: '10px'
        }
    }
});

class QuestionSubmitButtons extends Component {
    constructor(props){
        super(props);
    }
    
    render(){
        const {classes} = this.props;

        return (
            <div className={classes.buttonGroup}>
                <Button variant="raised" color="primary" onClick={this.props.onSave}>
                    Save
                </Button>
                <Button variant="raised" color="secondary" onClick={this.props.onCancel}>
                    Clear
                </Button>
            </div>
        );

    }
}

export default withStyles(styles)(QuestionSubmitButtons);

QuestionSubmitButtons.propTypes = {
    onSave: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired
};