import React, { Component } from 'react';

import InputLabel from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';

export default class QuestionRangeInput extends Component {
    constructor(props){
        super(props);

        this.state = {
            value: props.value
        };
    }
    
    render(){
        return (
            <div id="range-div">
                <TextField
                    id="number-range"
                    label="Range"
                    value={this.state.value}
                    onChange={e => this.updateScale(e.target.value)}
                    type="number"
                    InputLabelProps={{
                        shrink: true,
                    }}
                    margin="normal"
                />
            </div>
        );

    }

    updateScale(value){
        let val = value;

        if(typeof(val) === 'string')
            val = parseInt(val);

        this.setState({
            value: value
        });

        this.props.change(value);
    }
}