import React, { Component } from 'react';

import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';

export default class RangeReviewIndicator extends Component {
    constructor(props){
        super(props);

        this.state = {
            use: props.use
        };
    }

    componentWillReceiveProps(props){
        this.setState({
            use: props.use
        });
    }
    
    render(){
        return (
            <div className="form-group">
                <InputLabel htmlFor="use-this-review">Use this question to determine if this is a good or bad review?</InputLabel>
                <br/>
                <Select
                    value={this.state.use ? 1 : 0}
                    onChange={(e) => this.props.change('isIndicator', !!e.target.value)}
                    inputProps={{
                        name: 'use-this-review',
                        id: 'use-this-review',
                    }}
                >
                    <MenuItem value={1}>Yes</MenuItem>
                    <MenuItem value={0}>No</MenuItem>
                </Select>
            </div>
        );
    }
}