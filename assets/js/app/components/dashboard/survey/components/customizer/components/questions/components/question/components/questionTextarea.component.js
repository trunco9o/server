import React, { Component } from 'react';
import PropTypes from 'prop-types';

import TextField from '@material-ui/core/TextField';

export default class QuestionTextarea extends Component {
    constructor(props){
        super(props);

        this.state = {
            text: this.props.text
        };
    }

    static getDerivedStateFromProps(props, state){
        return {
            text: props.text
        };
    }
    
    render(){
        const {change} = this.props;

        return (
            <div className="form-group">
                <TextField 
                    id="question"
                    label="question"
                    margin="normal"
                    value={this.state.text}
                    onChange={e => this.change(e)}
                    multiline
                />
            </div>
        );
    }

    change(e){
        const value = e.target.value;

        this.setState({
            text: value
        });

        this.props.change(value);
    }
}

QuestionTextarea.propTypes = {
    text: PropTypes.string.isRequired,
    change: PropTypes.func.isRequired
};