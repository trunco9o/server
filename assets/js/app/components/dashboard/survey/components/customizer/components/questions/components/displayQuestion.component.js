import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import Question from './question/question.component';

const styles = theme => ({
    buttonGroup: {
        'margin-top': '15px',
        '& button:first-child': {
            marginRight: '10px'
        }
    }
});

class DisplayQuestion extends Component {

    constructor(props){
        super(props);

        const questionModel = props.question;

        const question = questionModel.getQuestion();

        this.state = {
            question: question
        };
     
    }

    componentWillReceiveProps(props){
        const question = props.question.getQuestion();

        this.setState({
            question: question
        });
    }
    
    render(){
        const {classes} = this.props;

        const {question} = this.state;

        return (
            <div>
                <div>
                    <Typography variant="subheading" gutterBottom>{question.text}</Typography>
                    <Typography variant="body2" gutterBottom>Type: {question.type}</Typography>

                    <div className={classes.buttonGroup}>
                        <Button variant="raised" color="primary" onClick={() => this.props.edit()}>
                            Edit
                        </Button>
                        <Button variant="raised" color="secondary" onClick={() => this.props.remove()}>
                            Remove
                        </Button>
                    </div>

                </div>
            </div>
        );
    }
}

export default withStyles(styles)(DisplayQuestion);

DisplayQuestion.propTypes = {
    question: PropTypes.object.isRequired,
    edit: PropTypes.func.isRequired,
    remove: PropTypes.func.isRequired
};