import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import ReviewLink from './components/reviewLink.component';
import ReviewLinkForm from './components/reviewLinkForm/reviewLinkForm.component';

const styles = theme => ({
    root: {
        flexGrow: 1
    },
    areaTitle: {
        marginTop: 0
    }
});


class ReviewLinks extends Component {

    constructor(props){
        super(props);

        this.state = {
            editLink: props.createLink(),
            links: props.links
        }
    }

    componentWillReceiveProps(props){
        this.setState({
            ...this.state,
            links: props.links
        });
    }
    
    render(){
        const {classes} = this.props;
        const {editLink} = this.state;
        const link = editLink.getLink();

        return (
            <div className={classes.root}>
                <Grid container spacing={16}>
                    <Grid item xs={12} sm={6}>
                        <Typography variant="title" gutterBottom>Review Links</Typography>
                        {this.getLinks()}
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Typography variant="title" gutterBottom>Add Review Link</Typography>
                        <ReviewLinkForm 
                            link={editLink}
                            save={() => link.id ? this.updateLink(null, editLink) : this.saveLink(editLink)}
                            clear={() => this.resetLink()}
                        />
                    </Grid>
                </Grid>
            </div>
        );
    }

    getLinks(){
        if(!this.props.links)
            return;

        return this.props.links.map((reviewLink, index) => (
            <ReviewLink 
                key={index}
                link={reviewLink} 
                edit={() => this.editLink(reviewLink)}
                remove={() => this.props.removeLink(index, reviewLink)}
            />
        ));
    }

    editLink(link){
        this.setState({
            ...this.state,
            editLink: link
        });
    }

    updateLink(index, link){
        this.props.updateLink(index, link);

        this.resetLink();
    }

    saveLink(link){
        this.props.addLink(link);

        this.resetLink();
    }

    resetLink(){
        this.setState({
            ...this.state,
            editLink: this.props.createLink()
        });
    }
}

ReviewLinks.propTypes = {
    createLink: PropTypes.func.isRequired,
    removeLink: PropTypes.func.isRequired,
    addLink: PropTypes.func.isRequired,
    updateLink: PropTypes.func.isRequired,
    links: PropTypes.array.isRequired
};

export default withStyles(styles)(ReviewLinks);