import React, { Component } from 'react';

import Slider from 'react-rangeslider';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';

import 'react-rangeslider/lib/index.css';

export default class RangeReviewIndicator extends Component {
    constructor(props){
        super(props);

        this.state = {
            showCutoff: true,
            higherIsGood: props.higherIsGood,
            scale: props.scale,
            positiveCutoff: props.positiveCutoff
        };
    }

    componentWillReceiveProps(props){
        this.setState({
            ...this.state,
            higherIsGood: props.higherIsGood,
            scale: props.scale,
            positiveCutoff: props.positiveCutoff
        });
    }
    
    render(){
        return ( 
            <div className="form-group">
                <div>
                    <InputLabel htmlFor="range-review-prompt">Tell us what number indicates a good review</InputLabel>
                    <br/>
                    <Select
                        value={this.state.higherIsGood ? 1 : 0}
                        onChange={e => this.props.change('higherIsGood', !!e.target.value)}
                        inputProps={{
                            name: 'range-review-prompt',
                            id: 'range-review-prompt',
                        }}
                    >
                        <MenuItem value={0}>1 is good</MenuItem>
                        <MenuItem value={1}>{this.state.scale || 5} is good</MenuItem>
                    </Select>
                </div>

                <div>    
                    <InputLabel>At what point should we consider this a bad review?</InputLabel>
                    <Slider
                        min={1}
                        max={parseInt(this.state.scale) || 5}
                        value={this.state.positiveCutoff}
                        onChangeStart={this.changeStart.bind(this)}
                        onChange={value => this.props.change('positiveCutoff', value)}
                        onChangeComplete={this.changeEnd.bind(this)}
                    />

                    {this.getCutoff()}
                </div>
            </div>
        );

    }

    changeStart(){
        this.setState({
            showCutoff: false
        });
    }

    changeEnd(){
        this.setState({
            showCutoff: true
        });
    }

    getCutoff(){
        if(this.state.showCutoff)
            return (
                <p>
                    {this.props.positiveCutoff}
                </p>
            );
    }
}