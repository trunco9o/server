import React, { Component } from 'react';
import PropTypes from 'prop-types';

import TextField from '@material-ui/core/TextField';


export default class ReviewLinkInputs extends Component {
    constructor(props){
        super(props);

        this.state={
            site: props.site,
            url: props.url
        }
    }

    componentWillReceiveProps(props){
        this.setState({
            site: props.site,
            url: props.url
        });
    }

    render(){
        const {change} = this.props;

        return (
            <div>
                <div>
                    <TextField id="siteName"
                        label="Site Name"
                        value={this.state.site}
                        onChange={e => change('site', e.target.value)}
                        margin="normal"
                    />
                </div>
                <div>
                    <TextField id="url"
                        label="Url"
                        value={this.state.url}
                        onChange={e => change('url', e.target.value)}
                        margin="normal"
                    />
                </div>
            </div>
        );
    }
}