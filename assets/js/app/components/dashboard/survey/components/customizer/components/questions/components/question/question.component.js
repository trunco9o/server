import React, { Component } from 'react';
import PropTypes from 'prop-types';

import QuestionTextarea from './components/questionTextarea.component';
import QuestionTypeSelect from './components/questionTypeSelect.component';
import QuestionRangeInput from './components/questionRangeInput.component';
import QuestionSubmitButtons from './components/questionSubmitButtons.component';
import QuestionReviewIndicators from './components/question.reviewIndicators.component';

export default class Question extends Component {
    _questionModel;
    _initialErrors;

    constructor(props){
        super(props);

        const {question} = this.props;

        this.questionChangeCallback = question => this.setState({question: question});

        question.on('change', this.questionChangeCallback);

        const questionObject = question.getQuestion();

        this.state = {
            question: questionObject
        };
    }

    componentWillReceiveProps(nextProps) {
        const {question} = this.props;

        question.removeListener('change', this.questionChangeCallback);

        const newQuestion = nextProps.question;

        newQuestion.on('change', this.questionChangeCallback);

        const nextPropsQuestion = newQuestion.getQuestion();

        if (nextPropsQuestion !== this.state.question) {
            this.setState({ question: nextPropsQuestion });
        }
    }

    componentWillUnmount(){
        const {question} = this.props;

        question.removeListener('change', this.questionChangeCallback);
    }
    
    render(){
        const questionModel = this.props.question;

        const {question} = this.state;

        return (
            <div>
                <QuestionTextarea text={question.text} change={text => questionModel.setText(text)}/>

                <QuestionTypeSelect type={question.type} change={type => questionModel.setType(type)}/>

                {this.getInputs(questionModel, question)}

                <QuestionSubmitButtons onSave={() => this.props.save(questionModel)} 
                                        onCancel={() => this.props.clear()}
                />
            </div>
        );
    }

    getInputs(questionModel, question){
        const inputs = [];

        const type = question.type;

        if(type === 'open-ended' || !type){
            return;
        }
        else if(type === 'range'){
            inputs.push(
                <QuestionRangeInput 
                    key='range-input'
                    show={type === 'range'} 
                    value={question.scale}
                    change={value => questionModel.setScale(value)}
                />
            );
        }
        
        inputs.push(
            <QuestionReviewIndicators 
                key='review-indicators'
                type={type} 
                scale={question.scale}
                indicator={question.indicator} 
                change={(type, value) => this.updateIndicator(type, value)}
            />
        );
        
        return inputs;
    }

    updateIndicator(type, value){
        const {question} = this.props;

        if(type === 'positiveCutoff'){
            question.setPositiveCutoff(value);
        }
        else if(type === 'isIndicator'){
            question.setIsIndicator(value);
        }
        else if(type === 'booleanIndicator'){
            question.setBooleanIndicator(value);
        }
        else if(type === 'higherIsGood'){
            question.setRangeIndicator(value);
        }
    }
}

Question.propTypes = {
    question: PropTypes.object.isRequired,
    save: PropTypes.func.isRequired,
    clear: PropTypes.func.isRequired
};