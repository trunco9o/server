import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {withStyles} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Edit from '@material-ui/icons/Edit';
import DeleteForever from '@material-ui/icons/DeleteForever';

import ReviewLinkForm from './reviewLinkForm/reviewLinkForm.component';

const styles = theme => ({
    editRemove: {
        marginLeft: '5px'
    },
    glyphButton: {
        cursor: 'pointer',
        display: 'inline-block'
    },
    inline: {
        display: 'inline-block'
    }
});

class ReviewLink extends Component {

    constructor(props){
        super(props);

        const linkModel = props.link;

        const link = linkModel.getLink();

        this.state = {
            link: link
        };
    }

    componentWillReceiveProps(props){
        const link = props.link.getLink();

        this.setState({
            link: link
        });
    }

    render(){
        const {classes, key} = this.props;
        
        const {link} = this.state;

        return (
            <div key={key}>
                <Typography className={classes.inline} variant="subheading" gutterBottom>
                    <span>
                        {link.site}: <a href={link.url}>{link.url}</a>
                    </span>
                    {this.getEditRemove(classes)}
                </Typography>
            </div>
        );
    }

    getEditRemove(classes){
        return [
            <Typography variant="body2" className={classes.glyphButton} onClick={() => this.props.edit()} key='edit'>
                <Edit style={{ fontSize: 20, verticalAlign: 'text-bottom' }} />
            </Typography>,
            <Typography variant="body2" className={classes.glyphButton} onClick={() => this.props.remove()} key='remove'>
                <DeleteForever style={{ fontSize: 20, verticalAlign: 'text-bottom' }} />
            </Typography>
        ];
    }
}

ReviewLink.propTypes = {
    link: PropTypes.object.isRequired,
    edit: PropTypes.func.isRequired,
    remove: PropTypes.func.isRequired,
    key: PropTypes.number
};

export default withStyles(styles)(ReviewLink);