import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';

export default class QuestionTypeSelect extends Component {
    constructor(props){
        super(props);

        this.state = {
            type: this.props.type || 'open-ended'
        };
    }
    
    static getDerivedStateFromProps(props, state){
        return {
            type: props.type
        };
    }

    render(){
        const {type} = this.state;

        return (
            <div className="form-group">
                <InputLabel htmlFor="type">Type</InputLabel>
                <br/>
                <Select
                    value={type}
                    onChange={e => this.change(e)}
                    inputProps={{
                        name: 'type',
                        id: 'type',
                    }}
                >
                    <MenuItem value={'open-ended'}>Open Ended</MenuItem>
                    <MenuItem value={'yes-no'}>Yes/No</MenuItem>
                    <MenuItem value={'range'}>Range</MenuItem>
                </Select>
            </div>
        );
    }

    change(e){
        const value = e.target.value;

        this.setState({
            type: value
        });

        this.props.change(value)
    }
}

QuestionTypeSelect.propTypes = {
    type: PropTypes.string.isRequired,
    change: PropTypes.func.isRequired
};