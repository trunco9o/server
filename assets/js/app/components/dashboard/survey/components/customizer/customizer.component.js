import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Slider from 'react-rangeslider';

import {withStyles} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';

import Questions from './components/questions/questions.component';
import ReviewLinks from './components/reviewLinks/reviewLinks.component';

import formatReviewLinks from '../../../../../services/formatReviewLinks';

const styles = theme => ({
    paper: {
        padding: '30px',
        'margin-bottom': '20px'
    },
    textArea: {
        'min-width': '35%'
    }
});

class Customizer extends Component {

    constructor(props){
        super(props);

        this.state = {
            trigger: '',
            positiveCutoff: false,
            invalidMessage: '',
            negativeMessage: '',
            positiveMessage: '',
        };
    }
    
    render(){
        const {classes, survey} = this.props;

        const positiveCutoff = this.state.positiveCutoff !== false ? this.state.positiveCutoff : survey.positiveCutoff;

        if(!survey){
            return <span></span>
        }

        return (
            <div id="customizer">
                <Paper elevation={8} className={classes.paper}>
                    <Typography variant="title" gutterBottom>Survey Trigger</Typography>
                    
                    <TextField 
                        id="trigger"
                        label="Trigger"
                        defaultValue={survey.trigger}
                        onChange={e => this.updateTrigger(e.target.value)}
                        margin="normal"
                    />
                    <Typography variant="body2" gutterBottom>The trigger word controls what phrase triggers a review</Typography>
                    <Button variant="raised" color="primary" onClick={() => this.props.updateSurvey({trigger: this.state.trigger})}>
                        Save
                    </Button>
                </Paper>

                <Paper elevation={8} className={classes.paper}>
                    <Questions 
                        questions={survey.questions}
                        addQuestion={question => this.props.addQuestion(question)}
                        createQuestion={question => this.props.createQuestion(question)}
                        removeQuestion={(index, question) => this.props.removeQuestion(index, question)}
                        updateQuestion={(index, question) => this.props.updateQuestion(index, question)}
                    />
                </Paper>

                <Paper elevation={8} className={classes.paper}>
                    <ReviewLinks 
                        links={survey.reviewLinks}
                        addLink={link => this.props.addLink(link)}
                        createLink={link => this.props.createLink(link)}
                        removeLink={(index, link) => this.props.removeLink(index, link)}
                        updateLink={(index, link) => this.props.updateLink(index, link)}
                    />
                </Paper>

                <Paper elevation={8} className={classes.paper}>
                    <Typography variant="title" gutterBottom>Responses</Typography>
                    <div className="form-group">
                        <TextField 
                            id="positive-review-message"
                            className={classes.textArea}
                            label="Positive Review Message"
                            defaultValue={survey.positiveReviewMessage}
                            onChange={e => this.updateMessage('positiveMessage', e.target.value)}
                            margin="normal"
                            multiline
                        />
                    </div>

                    <div className="form-group">
                        <TextField 
                            id="negative-review-message"
                            className={classes.textArea}
                            label="Negative Review Message"
                            defaultValue={survey.negativeReviewMessage}
                            onChange={e => this.updateMessage('negativeMessage', e.target.value)}
                            margin="normal"
                            multiline
                        />
                    </div>

                    <div className="form-group">
                        <TextField 
                            id="invalid-review-message"
                            className={classes.textArea}
                            label="Invalid Review Message"
                            defaultValue={survey.invalidReviewMessage}
                            onChange={e => this.updateMessage('invalidMessage', e.target.value)}
                            margin="normal"
                            multiline
                        />
                    </div>

                    <Button variant="raised" color="primary" onClick={() => this.saveMessages()}>
                        Save
                    </Button>
                </Paper>

                <Paper elevation={8} className={classes.paper}>
                    <Typography variant="title" gutterBottom>Negative Review Cutoff</Typography>
                    <Typography variant="body2" gutterBottom>At least {Math.round(positiveCutoff * 100)}% of answers need to be positive for a user to get your social media links.</Typography>
                    <Slider
                        min={0}
                        max={1}
                        step={0.01}
                        value={positiveCutoff}
                        onChange={value => this.updatePositiveCutoff(value)}
                    />

                    <Button variant="raised" color="primary" onClick={() => this.props.updateSurvey({'positiveCutoff': this.state.positiveCutoff})}>
                        Save
                    </Button>
                </Paper>

                {this.getResetSurvey()}
            </div>
        );
    }

    getResetSurvey(){
        if(this.props.reset){
            return (
                <Button variant="raised" color="primary" onClick={this.props.resetSurvey}>
                    Reset Survey
                </Button>
            );
        }
    }

    updateMessage(type, value){
        let state = {...this.state};

        state[type] = value;

        this.setState(state);
    }

    updateTrigger(value){
        this.setState({
            ...this.state,
            trigger: value
        });
    }

    saveMessages(){
        this.props.updateSurvey({
            messages: [
                {type: 'invalidReviewMessage', message: this.state.invalidMessage || this.props.survey.invalidReviewMessage},
                {type: 'positiveReviewMessage', message: this.state.positiveMessage || this.props.survey.positiveReviewMessage},
                {type: 'negativeReviewMessage', message: this.state.negativeMessage || this.props.survey.negativeReviewMessage}
            ]
        });
    }

    updatePositiveCutoff(value){
        const positiveCutoff = Math.round(value * 100) / 100
        this.setState({
            ...this.state,
            positiveCutoff: positiveCutoff
        });
    }
}

Customizer.propTypes = {
    // Survey props
    reset: PropTypes.bool,
    resetSurvey: PropTypes.func,
    updateSurvey: PropTypes.func.isRequired,
    // Question props
    addQuestion: PropTypes.func.isRequired,
    createQuestion: PropTypes.func.isRequired,
    removeQuestion: PropTypes.func.isRequired,
    // Link props
    addLink: PropTypes.func.isRequired,
    createLink: PropTypes.func.isRequired,
    removeLink: PropTypes.func.isRequired
};

export default withStyles(styles)(Customizer)