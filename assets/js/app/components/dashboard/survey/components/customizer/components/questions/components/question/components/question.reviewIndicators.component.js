import React, { Component } from 'react';
import PropTypes from 'prop-types';

import RangeReviewIndicator from './reviewIndicators/rangeReviewIndicator.component';
import BooleanReviewIndicator from './reviewIndicators/booleanReviewIndicator.component';
import IndicatorPrompt from './reviewIndicators/indicatorPrompt.component';

export default class QuestionReviewIndicators extends Component {
    constructor(props){
        super(props);

        this.state = {
            type: props.type,
            indicator: props.indicator
        };
    }

    componentWillReceiveProps(props){
        this.setState({
            type: props.type,
            indicator: props.indicator
        });
    }
    
    render(){
        const {type, indicator} = this.state;

        return (
            <div className="form-group">
                <div>
                    {this.getIndicatorPrompt(indicator, type)}
                </div>

                <div>
                    {this.getIndicatorForm(indicator, type)}
                </div>
            </div>
        );
    }

    getIndicatorForm(indicator, type){
        if(!indicator.isIndicator || type === 'open-ended'){
            return;
        }
        else if(type === 'range'){
            return (
                <RangeReviewIndicator
                    key='rangeReviewIndicator'
                    scale={this.props.scale}
                    higherIsGood={indicator.higherIsGood}
                    positiveCutoff={indicator.positiveCutoff}
                    change={(type, value) => this.props.change(type, value)}
                />
            );
        }
        else if(type === 'yes-no'){
            return (
                <BooleanReviewIndicator 
                    key='booleanReviewIndicator'
                    value={indicator.booleanIndicator}
                    change={(type, value) => this.props.change(type, value)}
                />
            );
        }
    }

    getIndicatorPrompt(indicator, type){
        if(type === 'open-ended')
            return;

        return (
            <IndicatorPrompt 
                use={indicator.isIndicator} 
                change={(type, value) => this.props.change(type, value)}
            />
        );
    }
}

QuestionReviewIndicators.propTypes = {
    change: PropTypes.func.isRequired,
    type: PropTypes.string.isRequired,
    scale: PropTypes.number.isRequired,
    indicator: PropTypes.object.isRequired
};