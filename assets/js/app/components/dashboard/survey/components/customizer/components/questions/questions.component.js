import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';

import DisplayQuestion from './components/displayQuestion.component';
import Question from './components/question/question.component';

import { withStyles } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
    root: {
        flexGrow: 1
    },
    areaTitle: {
        marginTop: 0
    }
});

class Questions extends Component {

    constructor(props){
        super(props);

        this.state = {
            editQuestion: props.createQuestion(),
            questions: props.questions
        }
    }

    componentWillReceiveProps(props){
        this.setState({
            ...this.state,
            questions: props.questions
        });
    }
    
    render(){
        const {classes} = this.props;

        return (
            <div className={classes.root}>
                <Grid container spacing={16}>
                    <Grid item xs={12} sm={6}>
                        <Typography variant="title" gutterBottom>Questions</Typography>
                        {this.getQuestionList()}
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Typography variant="title" gutterBottom>Add Question</Typography>
                        {this.getQuestionControls()}
                    </Grid>
                </Grid>
            </div>
        );
    }

    getQuestionList(){
        if(!this.state.questions)
            return;
        
        return this.state.questions.map((question, index) => (
            <DisplayQuestion 
                key={index}
                question={question}
                edit={() => this.editQuestion(question)}
                remove={() => this.props.removeQuestion(index, question)}
            />
        ));
    }

    getQuestionControls(){
        const {editQuestion} = this.state;

        return (
            <Question
                question={editQuestion}
                save={question => question.id ? this.saveQuestion(question) : this.updateQuestion(null, question)}
                clear={() => this.resetQuestion()}
            />
        );
    }

    resetQuestion(){
        this.setState({
            editQuestion: this.props.createQuestion()
        });
    }

    editQuestion(question){
        let questionCopy = question.getQuestion();

        questionCopy = this.props.createQuestion(questionCopy);

        this.setState({
            editQuestion: questionCopy
        });
    }

    saveQuestion(question){
        this.props.addQuestion(question);

        this.resetQuestion();
    }

    updateQuestion(index, question){
        this.props.updateQuestion(index, question);

        this.resetQuestion();
    }
}

export default withStyles(styles)(Questions);

Questions.propTypes = {
    removeQuestion: PropTypes.func.isRequired,
    addQuestion: PropTypes.func.isRequired,
    updateQuestion: PropTypes.func.isRequired,
    questions: PropTypes.array.isRequired
};