import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';

export default class RangeReviewIndicator extends Component {
    constructor(props){
        super(props);

        this.state = {
            value: props.value
        };
    }

    componentWillReceiveProps(props){
        this.setState({
            value: props.value
        });
    }
    
    render(){
        return (
            <div className="form-group">
                <InputLabel htmlFor="yes-no-review-prompt">Tell us which option indicates a good review</InputLabel>
                <br/>
                <Select
                    value={this.state.value}
                    onChange={e => this.props.change('booleanIndicator', e.target.value)}
                    inputProps={{
                        name: 'yes-no-review-prompt',
                        id: 'yes-no-review-prompt',
                    }}
                >
                    <MenuItem value={'yes'}>Yes</MenuItem>
                    <MenuItem value={'no'}>No</MenuItem>
                </Select>
            </div>
        );

    }
}

RangeReviewIndicator.propTypes = {
    value: PropTypes.string.isRequired,
    change: PropTypes.func.isRequired
};