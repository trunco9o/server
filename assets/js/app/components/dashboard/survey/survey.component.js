import React, { Component } from 'react';
import PropTypes from 'prop-types';

import TryIt from './components/tryit.component';
import Customizer from './components/customizer/customizer.component';
import ReviewPage from './components/reviewPage.component';

export default class Survey extends Component{
    _surveyModel;

    constructor(props){
        super(props);

        this._surveyModel = this.props.surveyModel;

        this.state = {
            survey: false
        };

        this.updateSurvey = survey => this.setState({survey: survey});

        this._surveyModel.on('surveyChange', this.updateSurvey);
    }

    componentDidMount(){
        this._surveyModel.getSurvey()
        .then(survey => {
            this.setState({
                survey: survey
            });
        });
    }

    componentWillUnmount(){
        this._surveyModel.removeListener('surveyChange', this.updateSurvey)
    }

    render(){
        const {survey} = this.state;
        const surveyModel = this._surveyModel;
        return (
            <div>
                <Customizer 
                    survey={survey}
                    updateSurvey={data => surveyModel.updateSurvey(data)}

                    addQuestion={question => surveyModel.addQuestion(question)}
                    createQuestion={question => surveyModel.createQuestion(question)}
                    removeQuestion={(index, question) => surveyModel.removeQuestion(index, question)}
                    updateQuestion={(index, question) => surveyModel.updateQuestion(index, question)}
                    
                    addLink={link => surveyModel.addLink(link)}
                    createLink={link => surveyModel.createLink(link)}
                    removeLink={(index, link) => surveyModel.removeLink(index, link)}
                    updateLink={(index, link) => surveyModel.updateLink(index, link)}
                />

                <ReviewPage 
                    businessName={this.props.businessName} 
                    reviewColor={survey.reviewColor} 
                    save={color => this._surveyModel.updateReviewColor(color)}
                />

                <TryIt survey={survey}/>
                
            </div>
        );
    }
}

Survey.propTypes = {
    surveyModel: PropTypes.object.isRequired,
    businessName: PropTypes.string.isRequired
};