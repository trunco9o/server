// @flow

import React, { Component } from 'react';

import moment from 'moment';

import {withStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';

import {timeFormatDefaultLocale} from 'd3-time-format';

// $FlowFixMe
import "react-vis/dist/styles/legends.scss";
// $FlowFixMe
import "react-vis/dist/styles/plot.scss";

import {
    XYPlot,
    XAxis,
    YAxis,
    VerticalGridLines,
    HorizontalGridLines,
    LineSeries
  } from 'react-vis';

const ONE_DAY = 86400000;

type Props = {
    classes: Object,
    getAggregations: Function
}

type State = {
    series: Array<Object>,
    total: number,
    totalPositive: number,
    totalNegative: number,
    totalInvalid: number
}

class Usage extends Component<Props, State>{
    constructor(props){
        super(props);

        this.state = {
            series: [],
            total: 0,
            totalPositive: 0,
            totalNegative: 0,
            totalInvalid: 0
        }

        this.getReviews();
    }

    render(){
        return (
            <Paper elevation={8} style={{padding: '20px', marginBottom: '50px', minHeight: '600px'}}>
                <Typography variant="title">
                    Usage
                </Typography>

                <Grid container spacing={16} style={{marginBottom: '25px', marginTop: '25px'}}>
                    <Grid item sm={6} md={3}>
                        <Card>
                            <CardContent>
                                {this.state.total} total reviews
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item sm={6} md={3}>
                        <Card>
                            <CardContent>
                                {this.state.totalPositive} positive reviews
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item sm={6} md={3}>
                        <Card>
                            <CardContent>
                                {this.state.totalNegative} negative reviews
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item sm={6} md={3}>
                        <Card>
                            <CardContent>
                                {this.state.totalInvalid} invalid reviews
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>

                {this.getChart()}
            </Paper>
        );
    }

    getChart(){
        const { series } = this.state;

        if(!series.length)
            return;

        return (
            <XYPlot
                xType="time-utc"
                width={800}
                height={550}
                margin={{bottom: 50}}>
                <VerticalGridLines />
                <HorizontalGridLines />
                <XAxis tickFormat={date => moment.utc(date).format('M/D/Y')} tickLabelAngle={-45} tickTotal={series.length}/>
                <YAxis/>
                <LineSeries data={series} color="#3f51b5"/>
            </XYPlot>
        );
    }

    async getReviews(){
        const data = await this.props.getAggregations();

        const series = [];
        let totalReviews = 0;
        let totalNegative = 0;
        let totalPositive = 0;
        let totalInvalid = 0;

        data.map(point => {
            const total = point.totalReviews;
            const date = point.date.$date;

            series.push({
                x: date,
                y: total
            });

            console.log(point);
            totalReviews += total;
            totalPositive += point.positiveReviews;
            totalNegative += point.negativeReviews;
            totalInvalid += point.invalidReviews;
        });
        
        this.setState({
            ...this.state,
            series: series,
            total: totalReviews,
            totalPositive: totalPositive,
            totalNegative: totalNegative,
            totalInvalid: totalInvalid
        });
    }
}

export default withStyles()(Usage)