import React, { Component } from 'react';

import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

export default class SMSMarketing extends Component {
    render(){
        return (
            <div>
                <Paper elevation={8} style={{padding: '20px'}}>
                    <Typography variant='title'>
                        SMS Marketing
                    </Typography>

                    <div style={{textAlign: 'center'}}>
                        <img style={{maxWidth: '90%', height: 'auto'}} src="/static/images/guru.jpg"/>
                        <Typography variant="subheading">
                            Sit tight, SMS marketing is coming soon.
                        </Typography>
                    </div>
                </Paper>
            </div>
        )
    }
}