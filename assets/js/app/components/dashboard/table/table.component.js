import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';

import EnhancedTableHead from './components/tableHead.component';
import EnhancedTableToolbar from './components/tableToolbar.component';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  table: {
    minWidth: 1020,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
});

/**
 * Props:
 * title
 * tableData
 * headerData
 */

class EnhancedTable extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      order: 'asc',
      orderBy: '',
      selected: [],
      page: 0,
      rowsPerPage: 10,
      allSelected: false
    };
  }

  handleRequestSort(event, property){
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    const data =
      order === 'desc'
        ? this.props.tableData.sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
        : this.props.tableData.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));

    this.setState({ data, order, orderBy });
  };

  handleSelectAllClick(event, checked){
    return this.setState({ ...this.state, selected: [], allSelected: checked });
  };

  handleClick(event, id){
    const { selected } = this.state;
    const { tableData } = this.props;
  
    const selectedIndex = selected.indexOf(id);

    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    const allSelected = (newSelected.length === tableData.length);

    this.setState({ ...this.state, selected: newSelected, allSelected: allSelected });
  };

  handleChangePage(event, page){
    this.setState({ ...this.state, page });
  };

  handleChangeRowsPerPage(event){
    this.setState({ ...this.state, rowsPerPage: event.target.value });
  };

  isSelected(id){
    return this.state.selected.indexOf(id) !== -1;
  }

  render() {
    const { classes, headerData, tableData } = this.props;
    const { order, orderBy, selected, rowsPerPage, page, allSelected } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, this.props.tableData.length - page * rowsPerPage);

    const numSelected = allSelected ? tableData.length : selected.length;
    return (
      <div>
        <EnhancedTableToolbar 
          title={this.props.title} 
          numSelected={numSelected} 
          exportClick={e => this.props.exportClick(e, selected, allSelected)}
          viewClick={e => this.props.viewClick(selected[0])}
        />
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <EnhancedTableHead
              order={order}
              orderBy={orderBy}
              data={headerData}
              numSelected={numSelected}
              rowCount={tableData.length}
              onRequestSort={(event, property) => this.handleRequestSort(event, property)}
              onSelectAllClick={(event, checked) => this.handleSelectAllClick(event, checked)}
            />
            <TableBody>
              {tableData.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(n => {
                const isSelected = allSelected || this.isSelected(n.id);
                return (
                  <TableRow
                    hover
                    key={n.id}
                    tabIndex={-1}
                    role="checkbox"
                    selected={isSelected}
                    aria-checked={isSelected}
                    onClick={event => this.handleClick(event, n.id)}
                  >
                    <TableCell padding="checkbox">
                      <Checkbox checked={isSelected} color="primary" />
                    </TableCell>
                    {this.getTableCells(n)}
                  </TableRow>
                );
              })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          component="div"
          count={tableData.length}
          rowsPerPage={rowsPerPage}
          rowsPerPageOptions={[5,10,15,20,25]}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
          onChangePage={(event, page) => this.handleChangePage(event, page)}
          onChangeRowsPerPage={event => this.handleChangeRowsPerPage(event)}
        />
      </div>
    );
  }

  getTableCells(dataObject){
    const tableData = [];

    for(let [key, value] of Object.entries(dataObject)){
      if(key !== 'id'){
        tableData.push(
          <TableCell key={`${key}${value}`}>{value}</TableCell>
        );
      }
    }

    return tableData;
  }
}

EnhancedTable.propTypes = {
  exportClick: PropTypes.func,
  tableData: PropTypes.arrayOf(PropTypes.object),
  headerData: PropTypes.arrayOf(PropTypes.object)
}

export default withStyles(styles)(EnhancedTable);