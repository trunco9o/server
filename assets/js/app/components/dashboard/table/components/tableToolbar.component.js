import React, {Component} from 'react';

import { withStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import CloudDownload from '@material-ui/icons/CloudDownload';
import FilterListIcon from '@material-ui/icons/FilterList';
import PageView from '@material-ui/icons/PageView';
import { lighten } from '@material-ui/core/styles/colorManipulator';
import classNames from 'classnames';

const styles = theme => ({
    root: {
      paddingRight: theme.spacing.unit,
    },
    highlight:
      theme.palette.type === 'light'
        ? {
            color: theme.palette.grey['700'],
            backgroundColor: lighten(theme.palette.primary.light, 0.85),
          }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark,
          },
    spacer: {
      flex: '1 1 80%',
    },
    actions: {
      color: theme.palette.text.secondary,
    },
    title: {
      flex: '0 0 auto',
    },
    titleFullWidth: {
        width: '100%'
    },
    tooltip: {
        display: 'inline-block'
    }
});

class EnhancedTableToolbar extends Component {
    constructor(props){
        super(props);
    }

    render(){
        const { numSelected, classes, exportClick, title, viewClick } = this.props;

        return (
            <Toolbar
                className={classNames(classes.root, {
                [classes.highlight]: numSelected > 0,
                })}
            >
                {this.getTitle(classes, title, numSelected)}
                <div className={classes.spacer} />
                <div className={classes.actions}>
                {numSelected > 0 ? (
                    <div style={{display: 'inline-block'}}>
                        <Tooltip title="Export" class={classes.tooltip}>
                            <IconButton aria-label="Export" onClick={exportClick}>
                                <CloudDownload />
                            </IconButton>
                        </Tooltip>
                        {viewClick && numSelected === 1 ? 
                            <Tooltip title="View" class={classes.tooltip}>
                                <IconButton aria-label="Export" onClick={viewClick}>
                                    <PageView />
                                </IconButton>
                            </Tooltip>
                        : ''}
                    </div>
                ) : ''}
                </div>
            </Toolbar>
        );
    }

    getTitle(classes, title, numSelected){
        const titleIsString = typeof(title) === 'string';

        const componentClasses = numSelected || titleIsString ? classes.title : classNames(classes.title, classes.titleFullWidth);

        return (
            <div className={componentClasses}>
                {numSelected ? this.getSelected(numSelected) : this.getTitleBlock(titleIsString, title)}
            </div>
        );
    }

    getSelected(numSelected){
        return (
            <Typography color="inherit" variant="subheading">
                {numSelected} selected
            </Typography>
        );
    }

    getTitleBlock(titleIsString, title){
        return titleIsString ? 
        (
            <Typography variant="title" id="tableTitle">
                {title}
            </Typography>
        )
        :
        title
    }
}
  
  export default withStyles(styles)(EnhancedTableToolbar);