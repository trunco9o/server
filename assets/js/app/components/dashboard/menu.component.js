import React, { Component } from 'react';

import Drawer from '@material-ui/core/Drawer';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';

import { withStyles } from '@material-ui/core/styles';

const drawerWidth = 240;

const styles = theme => ({
    drawerPaper: {
      position: 'relative',
      width: drawerWidth,
      zIndex: theme.zIndex.drawer
    },
    toolbar: theme.mixins.toolbar,
  });


class Menu extends Component{
    toggleCallback;

    constructor(props){
        super(props);
    }

    render(){
        const { classes, open } = this.props;

        const mobile = (window.innerWidth <= 960);
        const variant = mobile ? 'temporary' : 'permanent';
        const paperClasses = mobile ? {} : {paper: classes.drawerPaper};

        return (
            <Drawer variant={variant}
                    classes={paperClasses}
                    anchor='left'
                    open={open}
                    onClose={() => this.toggleDrawer(false)}
            >
                {mobile? '' : <div className={classes.toolbar} />}
                <MenuItem selected={this.props.selected === 'quickview'} onClick={() => this.props.menuClick('quickview')}>
                    <Typography variant="subheading">
                        Quick View
                    </Typography>
                </MenuItem>
                <MenuItem selected={this.props.selected === 'reviews'} onClick={() => this.props.menuClick('reviews')}>
                    <Typography variant="subheading">
                        Reviews
                    </Typography>
                </MenuItem>
                <MenuItem selected={this.props.selected === 'review-verification'} onClick={() => this.props.menuClick('review-verification')}>
                    <Typography variant="subheading">
                        Review Verification
                    </Typography>
                </MenuItem>
                <MenuItem selected={this.props.selected === 'survey'} onClick={() => this.props.menuClick('survey')}>
                    <Typography variant="subheading">
                        Survey
                    </Typography>
                </MenuItem>
                <MenuItem selected={this.props.selected === 'sms-marketing'} onClick={() => this.props.menuClick('sms-marketing')}>
                    <Typography variant="subheading">
                        SMS Marketing
                    </Typography>
                </MenuItem>
                <MenuItem selected={this.props.selected === 'contact-list'} onClick={() => this.props.menuClick('contact-list')}>
                    <Typography variant="subheading">
                        Customer Contact List
                    </Typography>
                </MenuItem>
                <MenuItem selected={this.props.selected === 'usage'} onClick={() => this.props.menuClick('usage')}>
                    <Typography variant="subheading">
                        Usage
                    </Typography>
                </MenuItem>
                <MenuItem selected={this.props.selected === 'user-info'} onClick={() => this.props.menuClick('user-info')}>
                    <Typography variant="subheading">
                        Update Business Info
                    </Typography>
                </MenuItem>
                <MenuItem selected={this.props.selected === 'settings'} onClick={() => this.props.menuClick('settings')}>
                    <Typography variant="subheading">
                        Settings
                    </Typography>
                </MenuItem>
            </Drawer>
        );
    }

    toggleDrawer(toggle){
        this.props.toggleMenuEventEmitter.emit('toggle');
    }
}

export default withStyles(styles)(Menu);