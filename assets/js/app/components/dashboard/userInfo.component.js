import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({
    textArea: {
        minWidth: '35%',
        marginBottom: '15px'
    },
    button: {
        marginTop: '25px'
    }
});

class UserInfo extends Component{
    constructor(props){
        super(props);

        this.state = {
            user: props.user
        };
    }

    componentWillReceiveProps(props){
        this.setState({
            user: props.user
        });
    }

    render(){
        const {classes} = this.props;
        const {user} = this.state;

        if(!user.name)
            return <span></span>

        return (
            <Paper elevation={8}>
                <div style={{padding: '20px', paddingBottom: '50px'}}>
                    <div>
                        <TextField defaultValue={user.business}
                            className={classes.textArea}
                            label="Business Name"
                            onChange={e => this.updateBusinessName(e.target.value)}
                        />
                    </div>

                    <div>
                        <TextField defaultValue={user.name}
                            className={classes.textArea}
                            label="Name"
                            onChange={e => this.updateName(e.target.value)}
                        />
                    </div>

                    <div>
                        <TextField defaultValue={user.email}
                            className={classes.textArea}
                            label="Email Address"
                            onChange={e => this.updateEmail(e.target.value)}
                        />
                    </div>

                    <div>
                        <TextField defaultValue={user.phone}
                            className={classes.textArea}
                            label="Phone Number"
                            onChange={e => this.updatePhone(e.target.value)}
                        />
                    </div>

                    <div>
                        <TextField value={user.textNumber}
                            className={classes.textArea}
                            label="Review Jawn Phone Number"
                            disabled={true}
                        />
                    </div>

                    <Button variant="raised" color="primary" className={classes.button} onClick={() => this.props.save(this.state.user)}>Save</Button>
                </div>
            </Paper>
        );
    }

    updatePhone(phone){
        const {user} = this.state;

        user.phoneNumber = phone;

        this.setState({user: user});
    }

    updateEmail(email){
        const {user} = this.state;

        user.email = email;

        this.setState({user: user});
    }

    updateName(name){
        const {user} = this.state;

        user.name = name;

        this.setState({user: user});
    }

    updateBusinessName(name){
        const {user} = this.state;

        user.business = name;

        this.setState({user: user});
    }
}

export default withStyles(styles)(UserInfo);