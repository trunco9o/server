import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import getLocalDateString from '../../services/date';

const styles = theme => ({
    paper: {
        marginBottom: '50px'
    }
});

class QuickView extends Component{
    constructor(props){
        super(props);
    }

    render(){
        const {classes, user} = this.props;

        return (
            <div>
                <Paper elevation={8} classes={{root: classes.paper}}>
                    <div style={{padding: '20px', paddingBottom: '50px'}}>
                        <div>
                            <h2>{user.business}</h2>
                            <div id="contact-info">
                                <h4>Name: {user.name}</h4>
                                <h4>Email address: {user.email}</h4>
                                <h4>Phone number: {user.phone}</h4>
                                <h4>ReviewJawn phone number: {user.textNumber}</h4>
                            </div>
                        </div>
                    </div>
                </Paper>
                <Paper elevation={8}>
                    <div style={{padding: '20px', paddingBottom: '50px'}}>
                        <h2>Recent Reviews</h2>
                        <div>
                            <Table className={classes.table}>
                                <TableHead>
                                <TableRow>
                                    <TableCell>Created</TableCell>
                                    <TableCell>Status</TableCell>
                                    <TableCell>Completed</TableCell>
                                </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.makeTableRows()}
                                </TableBody>
                            </Table>
                        </div>
                    </div>
                </Paper>
            </div>
        );
    }

    makeTableRows(){
        const {reviews} = this.props;

        return Object.keys(reviews).map(key => {
            const review = reviews[key];

            let status; 

            if(review.positive){
                status = 'Positive';
            }
            else if(review.positive === false){
                status = 'Negative';
            }
            else if(review.invalid){
                status = 'Invalid';
            }
            else{
                status = 'N/A';
            }

            return <TableRow key={key}>
                        <TableCell>{getLocalDateString(review.created.$date)}</TableCell>
                        <TableCell>{status}</TableCell>
                        <TableCell>{review.completed ? 'Yes' : 'No'}</TableCell>
                    </TableRow>
        });
    }
}

export default withStyles(styles)(QuickView);