// @flow

import React, { Component } from 'react';

import { withStyles } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';

import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
    paper: {
        padding: '20px',
        marginBottom: '20px'
    },
    formGroup: {
        display: 'flex',
        width: '450px',
        maxWidth: '50%',
        marginBottom: '15px'
    },
    label: {
        whiteSpace: 'nowrap'
    }
});

type Props = {
    classes: Object,
    detailedReview: string,
    save: Function,
    updateSettings: Function,
    detailedReviewMessage: string
};

const options = ['always', 'positive reviews', 'invalid reviews', 'negative reviews', 'negative and invalid reviews'];
const defaultOption = 'negative reviews';

class ReviewFrequency extends Component<Props> {
    constructor(props){
        super(props);
    }

    render(){
        const {classes, updateSettings, save, detailedReview, detailedReviewMessage} = this.props;

        return (
            <div>
                <Paper elevation={8} className={classes.paper}>
                    <Typography variant='title'>
                        Detailed Review Settings
                    </Typography>

                    <FormControl className={classes.formGroup}>
                        <InputLabel htmlFor="review-frequency" className={classes.label}>
                            When should users be prompted to give a detailed review
                        </InputLabel>
                        <Select value={detailedReview || defaultOption} 
                                onChange={(e) => updateSettings('sendDetailedReview', e.target.value)}
                                inputProps={{
                                    name: 'review-frequency',
                                    id: 'review-frequency',
                                }}
                        >
                            {this.getMenuOptions(detailedReview)}
                        </Select>
                    </FormControl>

                    <div>
                        <TextField label="Detailed Review Message" 
                                   defaultValue={detailedReviewMessage}
                                   onChange={e => updateSettings('detailedReviewMessage', e.target.value)}
                        />
                    </div>

                    <Button variant="raised" color="primary" onClick={save}>
                        Save
                    </Button>
                </Paper>
            </div>
        );
    }

    getMenuOptions(detailedReview: string): Array<MenuItem> {
        return options.map((option,i) => {
            return (
                <MenuItem key={i} value={option}>{this.formatOption(option)}</MenuItem>
            );
        });
    }

    formatOption(option: string): string{
        return `${option[0].toUpperCase()}${option.substr(1, option.length)}`;
    }
}

export default withStyles(styles)(ReviewFrequency);