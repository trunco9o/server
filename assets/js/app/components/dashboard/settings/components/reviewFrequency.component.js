// @flow

import React, { Component } from 'react';

import { withStyles } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';

import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
    paper: {
        padding: '20px',
        marginBottom: '20px'
    },
    formGroup: {
        display: 'flex',
        width: '450px',
        maxWidth: '50%',
        marginBottom: '15px'
    },
    label: {
        whiteSpace: 'nowrap'
    }
});

type Props = {
    classes: Object,
    reviewFrequency: string,
    save: Function,
    updateSettings: Function,
    reviewFrequencyMessage: string
};

const options = ['daily', 'weekly', 'monthly', '60 days', '90 days'];
const defaultOption = 'daily';

class ReviewFrequency extends Component<Props> {
    constructor(props){
        super(props);
    }

    render(){
        const {classes, updateSettings, save, reviewFrequency, reviewFrequencyMessage} = this.props;

        return (
            <div>
                <Paper elevation={8} className={classes.paper}>
                    <Typography variant='title'>
                        Review Frequency Settings
                    </Typography>

                    <FormControl className={classes.formGroup}>
                        <InputLabel htmlFor="review-frequency" className={classes.label}>
                            How frequently should customers be allowed to send reviews?
                        </InputLabel>
                        <Select value={reviewFrequency || defaultOption} 
                                onChange={e => updateSettings('reviewFrequency', e.target.value)}
                                inputProps={{
                                    name: 'review-frequency',
                                    id: 'review-frequency',
                                }}
                        >
                            {this.getMenuOptions(reviewFrequency)}
                        </Select>
                    </FormControl>

                    <div>
                        <TextField label="Frequent review warning" 
                                   defaultValue={reviewFrequencyMessage}
                                   onChange={e => updateSettings('reviewFrequencyMessage', e.target.value)}
                        />
                    </div>

                    <Button variant="raised" color="primary" onClick={save}>
                        Save
                    </Button>
                </Paper>
            </div>
        );
    }

    getMenuOptions(reviewFrequency: string): Array<MenuItem> {
        return options.map((option,i) => {
            return (
                <MenuItem key={i} value={option}>{this.formatOption(option)}</MenuItem>
            );
        });
    }

    formatOption(option: string): string{
        return `${option[0].toUpperCase()}${option.substr(1, option.length)}`;
    }
}

export default withStyles(styles)(ReviewFrequency);