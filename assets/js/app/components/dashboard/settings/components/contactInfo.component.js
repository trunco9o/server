// @flow

import React, { Component } from 'react';

import { withStyles } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
    paper: {
        padding: '20px',
        marginBottom: '20px'
    }
});

type Props = {
    classes: Object,
    contactInfo: boolean,
    save: Function,
    updateSettings: Function,
    emailRequestMessage: string,
    nameRequestMessage: string
};

class ContactInfo extends Component<Props> {
    constructor(props){
        super(props);
    }

    render(){
        const {classes, save, updateSettings, contactInfo, emailRequestMessage, nameRequestMessage} = this.props;

        return (
            <div>
                <Paper elevation={8} className={classes.paper}>
                    <Typography variant='title'>
                        Contact Info Settings
                    </Typography>

                    <div>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={contactInfo || false}
                                    value='randomCode'
                                    onChange={() => updateSettings('requestContactInfo')}
                                />
                            }
                            label="Request contact info (name, email)"
                        />
                    </div>

                    <div>
                        <TextField label="Name request message" 
                                   defaultValue={nameRequestMessage}
                                   onChange={e => updateSettings('nameRequestMessage', e.target.value)}
                        />
                    </div>

                    <div>
                        <TextField label="Email request message" 
                                   defaultValue={emailRequestMessage}
                                   onChange={e => updateSettings('emailRequestMessage', e.target.value)}
                        />
                    </div>

                    <Button variant="raised" color="primary" onClick={save}>
                        Save
                    </Button>
                </Paper>
            </div>
        );
    }
}

export default withStyles(styles)(ContactInfo);