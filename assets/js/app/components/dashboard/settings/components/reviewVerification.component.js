// @flow

import React, { Component } from 'react';

import { withStyles } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
    paper: {
        padding: '20px',
        marginBottom: '20px'
    }
});

type State = {
    settings: Object
}

type Props = {
    classes: Object,
    generateCode: boolean,
    save: Function,
    updateSettings: Function,
    reviewCodeMessage: string
};

class ReviewVerification extends Component<Props, State> {
    constructor(props){
        super(props);
    }

    render(){
        const {classes, generateCode, save, updateSettings, reviewCodeMessage} = this.props;

        return (
            <div>
                <Paper elevation={8} className={classes.paper}>
                    <Typography variant='title'>
                        Review Verification Settings
                    </Typography>

                    <div>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={generateCode || false}
                                    value='randomCode'
                                    onChange={() => updateSettings('generateReviewCode')}
                                />
                            }
                            label="Generate random code to validate reviews"
                        />
                    </div>

                    <div>
                        <TextField label="Review Code Message" 
                                   defaultValue={reviewCodeMessage}
                                   onChange={e => updateSettings('reviewCodeMessage', e.target.value)}
                        />
                    </div>

                    <Typography>Preview:</Typography>
                    <Typography>{reviewCodeMessage} randomCode</Typography>

                    <Button variant="raised" color="primary" onClick={save}>
                        Save
                    </Button>
                </Paper>
            </div>
        );
    }
}

export default withStyles(styles)(ReviewVerification);