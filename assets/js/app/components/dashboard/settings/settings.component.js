// @flow

import React, { Component } from 'react';

import { withStyles } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import ReviewVerification from './components/reviewVerification.component';
import ReviewFrequency from './components/reviewFrequency.component';
import DetailedReview from './components/detailedReview.component';
import ContactInfo from './components/contactInfo.component';

const styles = theme => ({
    paper: {
        padding: '20px',
        marginBottom: '20px'
    }
});

type State = {
    settings: Object
}

type Props = {
    classes: Object,
    settings: Object,
    save: Function
};

class Settings extends Component<Props, State> {
    constructor(props){
        super(props);
        
        this.state = {
            settings: props.settings
        };
    }

    componentWillReceiveProps(props){
        this.setState({
            ...this.state,
            settings: props.settings
        });
    }

    render(){
        const {classes} = this.props;
        const {settings} = this.state;

        if(!settings)
            return <div></div>;

        return (
            <div>
                <ReviewVerification generateCode={settings.generateReviewCode} 
                                    reviewCodeMessage={settings.reviewCodeMessage}
                                    updateSettings={(prop, val) => this.updateSettings(prop, val)} 
                                    save={() => this.props.save(settings)}
                />
                
                <ReviewFrequency reviewFrequency={settings.reviewFrequency} 
                                 updateSettings={(prop, val) => this.updateSettings(prop, val)}
                                 reviewFrequencyMessage={settings.reviewFrequencyMessage}
                                 save={() => this.props.save(settings)}
                />

                <DetailedReview detailedReview={settings.sendDetailedReview} 
                                updateSettings={(prop, val) => this.updateSettings(prop, val)}
                                detailedReviewMessage={settings.detailedReviewMessage}
                                save={() => this.props.save(settings)}
                />

                <ContactInfo contactInfo={settings.requestContactInfo}
                             emailRequestMessage={settings.emailRequestMessage}
                             nameRequestMessage={settings.nameRequestMessage}
                             updateSettings={(prop, val) => this.updateSettings(prop, val)}
                             save={() => this.props.save(settings)}
                />
            </div>
        );
    }

    updateSettings(prop: string, val: any){
        const {settings} = this.state;

        settings[prop] = (val ? val : !settings[prop]);

        this.setState({
            ...this.state,
            settings: settings
        });
    }
}

export default withStyles(styles)(Settings);