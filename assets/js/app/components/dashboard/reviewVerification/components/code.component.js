import React, { Component } from "react";

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import ReviewCard from '../../../shared/reviews/reviewCard.component';
import Message from '../../../shared/message.component';
import {errorStyle} from '../../../shared/message.styles';

export default class Code extends Component {
    constructor(props){
        super(props)
    }

    render(){
        const {classes, reviewer} = this.props;

        return (
            <Paper elevation={8} className={classes.paper}>
                <Grid container spacing={16}>
                    <Grid item xs={12} sm={3}>
                        <Typography variant="title">
                            Verify by code
                        </Typography>
                        <div className={classes.inputContainer}>
                            <TextField
                                label="Code"
                                className={classes.firstInput}
                                onChange={e => this.setState({code: e.target.value})}
                            />
                        </div>

                        <div>
                            <Button variant="raised" color="primary" onClick={() => this.props.search(this.state.code)}>
                                Search
                            </Button>
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        {this.getReviewer(reviewer)}
                    </Grid>
                </Grid>
            </Paper>
        );
    }

    getReviewer(reviewer){
        if(!reviewer){
            return;
        } 
        else if(!Object.keys(reviewer).length){
            const {code} = this.state;
            const message = `We couldn\'t find a review matching ${code}`;
            return <Message message={message} marginTop="10px" stylePack={errorStyle}/>;
        }

        return (
            <ReviewCard review={reviewer}/>
        );
    }
}