// @flow

import React, {Component} from 'react';

import moment from 'moment';
import classnames from 'classnames';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import ReviewCard from '../../../shared/reviews/reviewCard.component';
import Message from '../../../shared/message.component';
import {errorStyle} from '../../../shared/message.styles';

type Props = {
    classes: Object,
    reviewer: Object,
    search: Function,
    showReview: Function
};

type State = {
    start: string,
    end: string,
};

export default class Random extends Component<Props, State> {
    constructor(props: Props){
        super(props);

        const today = moment();

        this.state = {
            end: today.format('YYYY-MM-DD'),
            start: today.subtract(1, 'month').format('YYYY-MM-DD')
        }
    }

    render(){
        const {classes, reviewer} = this.props;
        const {start, end} = this.state;

        return (
            <Paper elevation={8} className={classes.paper}>
                <Grid container spacing={16}>
                    <Grid item xs={12} sm={4}>
                        <Typography variant="title">
                            Select a random reviewer
                        </Typography>
                        <div className={classes.inputContainer}>
                            <Typography variant="subheading">
                                Select a date range
                            </Typography>

                            <TextField
                                label="From"
                                type="date"
                                value={start}
                                className={classes.firstInput}
                                onChange={e => this.updateFromDate(e.target.value)}
                            />

                            <TextField
                                label="To"
                                type="date"
                                value={end}
                                onChange={e => this.updateToDate(e.target.value)}
                            />
                        </div>
                        <div>
                            <Button variant="raised" color="primary" onClick={() => this.props.search(this.state)}>
                                Search
                            </Button>
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        {this.getReviewer(reviewer)}
                    </Grid>
                </Grid>
            </Paper>
        );
    }

    getReviewer(reviewer: Object){
        if(!reviewer){
            return;
        } 
        else if(!Object.keys(reviewer).length){
            const message = 'No reviews were completed in this time period.';

            return <Message message={message} marginTop="10px" stylePack={errorStyle}/>;
        }

        return (
            <ReviewCard review={reviewer} showReview={this.props.showReview}/>
        );
    }

    updateFromDate(val: string){
        this.setState({
            ...this.state,
            start: val
        });
    }

    updateToDate(val: string){
        this.setState({
            ...this.state,
            end: val
        });
    }
}