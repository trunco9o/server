import React, {Component} from 'react';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import ReviewCard from '../../../shared/reviews/reviewCard.component';
import Message from '../../../shared/message.component';
import {errorStyle} from '../../../shared/message.styles';

const styles = {
    review: {
        marginTop: '20px'
    }
}

export default class PhoneNumber extends Component {
    constructor(props){
        super(props);
    }

    render(){
        const {classes, reviews} = this.props;

        return (
            <Paper elevation={8} className={classes.paper}>
                <Grid container spacing={16}>
                    <Grid item xs={12} sm={3}>
                        <Typography variant="title">
                            Verify by phone number
                        </Typography>
                        <div className={classes.inputContainer}>
                            <TextField
                                label="Phone Number"
                                className={classes.firstInput}
                                onChange={e => this.setState({phone: e.target.value})}
                            />
                        </div>

                        <div>
                            <Button variant="raised" color="primary" onClick={() => this.props.search(this.state.phone)}>
                                Search
                            </Button>
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        {this.getReviews(reviews)}
                    </Grid>
                </Grid>
            </Paper>
        );
    }

    getReviews(reviews){
        if(!reviews){
            return;
        } else if(!Array.isArray(reviews) || !reviews.length){
            const message = `Sorry, we couldn\'t find any reviews for your business from ${this.state.phone}`;

            return <Message message={message} marginTop="10px" stylePack={errorStyle}/>;
        }

        return reviews.map((review, i) => <ReviewCard key={i} review={review} showReview={this.props.showReview}/>);
    }
}
