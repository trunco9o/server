import React, { Component } from 'react';

import { withStyles } from '@material-ui/core';

import Code from './components/code.component';
import Random from './components/random.component';
import PhoneNumber from './components/phoneNumber.component';
import ReviewVisibilityHandler from '../../shared/reviews/reviewVisibilityHandler.component';

const styles = theme => ({
    paper: {
        padding: '20px',
        marginBottom: '20px'
    },
    firstInput: {
        marginRight: '15px'
    },
    inputContainer: {
        marginTop: '10px',
        marginBottom: '20px'
    }
});

class ReviewVerification extends Component {

    constructor(props){
        super(props);

        this.state = {
            phone: undefined,
            code: undefined,
            type: undefined
        };
    }

    render(){
        const {classes} = this.props;
        const {phone, code, type} = this.state;

        return (
            <ReviewVisibilityHandler>
                <PhoneNumber classes={classes} search={val => this.search('phone', val)} reviews={phone}/>

                <Code classes={classes} search={val => this.search('code', val)} reviewer={code}/>

                <Random classes={classes} search={val => this.search('type', 'random', val)} reviewer={type}/>
            </ReviewVisibilityHandler>
        );
    }

    async search(type, val, extras){
        const query = {...extras};

        query['questions'] = 1;
        query['complete'] = 1;

        query[type] = val;

        const res = await this.props.search(query)
        
        const state = {...this.state};

        state[type] = res;

        this.setState(state);
    }
}

export default withStyles(styles)(ReviewVerification);