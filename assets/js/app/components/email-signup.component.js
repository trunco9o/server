import React, { Component } from 'react';

import '../../../sass/partials/signup/signup.sass';

export default class EmailSignup extends Component {
    constructor(props){
        super(props);

        this._fetchConfig = this.getFetchConfig();

        this.state = {
            email: '',
            error: false,
            success: false,
            showForm: true
        };
    }

    render(){
        return <div>
            <div className={this.state.showForm ? '' : 'hidden'}>
                <h2>{this.props.title}</h2>
                <h3 className={this.props.subtitle ? '' : 'hidden'}>{this.props.subtitle}</h3>

                <div className="form-group">
                    <label htmlFor="email">Email address</label>
                    <input type="email" name="email" onChange={this.setEmail.bind(this)}/>
                    <p className={this.state.error ? 'error' : 'hidden'}>{this.state.error}</p>
                </div>

                <div>
                    <button onClick={this.submit.bind(this)} className="button" name="submit">Submit</button>
                </div>
            </div>
            <div className={this.state.showForm ? 'hidden' : ''}>
                <h2>{this.state.success}</h2>
            </div>
        </div>
    }

    setEmail(e){
        const email = e.target.value;

        this.setState({
            ...this.state,
            email: email
        });
    }

    getFetchConfig(){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json; charset=utf-8');

        return {
            method: 'POST',
            headers: headers,
            mode: 'cors'
        }
    }

    async submit(){

        if(!this.state.email.length){
            return this.setState({
                ...this.state,
                error: 'Please enter your email address'
            });
        }

        this._fetchConfig.body = JSON.stringify({
            email: this.state.email
        });

        const res = await fetch('/email/json', this._fetchConfig)
        
        const json = await res.json();

        if(json.success){
            this.setState({
                ...this.state,
                error: false,
                showForm: false,
                success: json.success
            });
        }
        else{
            this.setState({
                ...this.state,
                error: json.error
            });
        }
    }
}