// @flow

import React, { Component } from 'react';

import _ from 'lodash';

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core';

type Props = {
    message: string,
    color: string,
    textColor: string,
    classes: Object,
    marginTop: string,
    marginBottom: string,
    margin: string,
    messageStyle: Object,
    containerStyle: Object,
    stylePack: Object
};

const styles = theme => ({
    root: {
        ...theme.typography.button,
        backgroundColor: theme.palette.common.white,
        padding: theme.spacing.unit,
        display: 'inline-block',
    },
});

class Message extends Component<Props> {

    static defaultProps: Object;

    constructor(props: Props){
        super(props);
    }

    render(){
        const {classes, message} = this.props;

        const styles = this.getStyles();

        return (
            <div className={classes.root} style={styles.container}>
                <span style={styles.message}>{message}</span>
            </div>
        );
    }

    getStyles(): Object{
        const {messageStyle, containerStyle, stylePack} = this.props;

        const container = {
            ...containerStyle,
            ...stylePack.container
        }

        const message = {
            ...messageStyle,
            ...stylePack.message,
        };

        this.setOverrides(container, message);

        return {
            message: _(message).omitBy(_.isEmpty).value(),
            container: _(container).omitBy(_.isEmpty).value()
        };
    }

    setOverrides(container: Object, message: Object): void {
        const {color, textColor, marginTop, marginBottom, margin} = this.props;

        if(color)
            container.backgroundColor = color;

        if(margin)
            container.margin = margin;

        if(marginTop)
            container.marginTop = marginTop;

        if(marginBottom)
            container.marginBottom = marginBottom;

        if(textColor)
            message.color = textColor;
    }

}

Message.defaultProps = {
    stylePack: {}
}

export default withStyles(styles)(Message);
