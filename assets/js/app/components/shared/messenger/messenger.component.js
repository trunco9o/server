import React, { Component } from 'react';

import Header from './components/header.component';
import Messages from './components/messages.component';
import MessageInput from './components/messageInput.component';

import './sass/messenger.sass';

export default class Messenger extends Component {

    constructor(props){
        super(props);
    }
    
    render(){
        return <div id="messenger">
            <Header/>
            <Messages messages={this.props.messages}/>
            <MessageInput onSend={this.props.addMessage}/>
        </div>
    }
}
