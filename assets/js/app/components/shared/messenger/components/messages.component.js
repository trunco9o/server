import React, { Component } from 'react';

import '../sass/messages.sass'

export default class Messages extends Component {

    constructor(props){
        super(props);
    }

    render(){
        const messages = this.props.messages.map((message, index) => {
            return <li className={message.sent ? 'sent' : ''} key={index}>
                <div>{message.text}</div>
            </li>
        });

        return <div id="messages">
                <ul>
                    {messages}
                </ul>
            </div>
    }
}
