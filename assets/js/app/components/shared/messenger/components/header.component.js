import React, { Component } from 'react';

import '../sass/header.sass'

export default class Header extends Component {

    constructor(props){
        super(props);
    }
    
    render(){

        return <div id="head">
            <div id="back">
                    <span>&nbsp;</span>
                </div>
                <div id="img">
                    <div>
                        <img src="/static/images/buddah.svg"/>
                    </div>
                    <span>RepGuru</span>
                </div>
                <div id="info">
                    <span>&nbsp;</span>
                </div>
            </div>
    }
}
