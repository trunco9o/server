import React, { Component } from 'react';

import '../sass/messageInput.sass';

export default class MessageInput extends Component {

    constructor(props){
        super(props);

        this.state = {
            message: '',
            inputRows: 1
        };
    }
    
    render(){
        return <div id="input">
                    <textarea rows={this.state.inputRows} 
                              onKeyDown={this.setMessage.bind(this)} 
                              name="message-input" 
                              value={this.state.message}
                    ></textarea>

                    <button onClick={this.sendMessage.bind(this)}>
                        <img src="/static/images/up-arrow.svg"/>
                    </button>
                </div>
    }

    setMessage(e){
        const keyCode = e.keyCode;
        const key = e.key;

        let message = this.state.message;
        let inputRows;

        //Enter
        if(keyCode === 13){
            return this.sendMessage();
        }
        //Space
        else if(keyCode === 8){
            message = message.slice(0, message.length - 1);
        }
        //non letter/number key
        else if(key.length > 1){
            return;
        }
        else{
            message += e.key;
        }

        inputRows = (Math.ceil(message.length/36 + 0.01));

        return this.setState({
            message: message,
            inputRows: inputRows
        });
        
    }

    sendMessage(){
        const message = this.state.message;

        if(message.length)
            this.props.onSend(message);

        return this.setState({
            message: '',
            inputRows: 1
        });
    }
}
