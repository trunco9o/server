// @flow

import React, { Component } from 'react';

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core';

import getHeadline from './lib/headline';

type Props = {
    review: Object,
    classes: Object,
    maxWidth: string,
    showReview: Function
};

const styles = {
    card: {
        minWidth: 275
    },
    title: {
        marginBottom: 16,
        fontSize: 14,
    },
};

class ReviewCard extends Component<Props> {
    static defaultProps: Object;

    constructor(props: Props){
        super(props);
    }

    render(){
        const {review, classes, maxWidth, showReview} = this.props;

        return (
            <div>
                <Card className={classes.card} style={{maxWidth: maxWidth}}>
                    <CardContent>
                        <Typography variant="subheading">
                            Verified Reviewer
                        </Typography>

                        {this.getContactInfo(classes, review)}

                        {this.getReviewInfo(review)}
                    </CardContent>

                    <CardActions>
                        <Button size="small" onClick={() => showReview(review)}>See full review</Button>
                    </CardActions>
                </Card>
            </div>
        );
    }

    getContactInfo(classes: Object, review: Object): Typography {
        return (
            <Typography className={classes.title} color="textSecondary">
                {getHeadline(review)}
            </Typography>
        );
    }

    getReviewInfo(review: Object): Typography {
        const status = this.getStatus(review.isPositive, review.invalid, review.noIndicator);

        return (
            <div>
                <Typography>
                    {`Rating: ${review.score}/100`}
                </Typography>
                {status}
            </div>
        );
    }

    getStatus(positive: boolean, invalid: boolean, noIndicator: boolean): Typography {
        let str;
        let color;

        if(noIndicator){
            str = 'Add an indicator question to your survey to get quantative feedback.';
            color = 'yellow';
        }
        if(positive){
            str =  'Positive';
            color = 'green';
        }
        else if(invalid){
            str = 'Invalid';
            color = 'gray';
        }
        else if (positive === false){
            str = 'Negative';
            color = 'gray';
        }
        else{
            str = 'N/A';
            color = 'red';
        }

        return (
            <Typography style={{color: color}}>
                {str}
            </Typography>
        );
    }
}

ReviewCard.defaultProps = {
    maxWidth: '300px'
};

export default withStyles(styles)(ReviewCard);