// @flow

import * as React from 'react';
import {withStyles} from '@material-ui/core';
import {flipInY, flipOutY} from 'react-animations';

import FullReview from './fullReview.component';

type Props = {
    children: React.Node;
    classes: Object;
};

type State = {
    review: Object | null,
    showReview: boolean,
    childClass: Object | null
};

const styles = theme => ({
    '@keyframes flipInY': flipInY,
    flipInY: {
        animationName: 'flipInY',
        animationDuration: '1s'
    },
    '@keyframes flipOutY': flipOutY,
    flipOutY: {
        animationName: 'flipOutY',
        animationDuration: '1s'
    }
});

const Component = React.Component;

class ReviewVisiblityHandler extends Component<Props, State> {
    constructor(props: Props){
        super(props);

        this.state = {
            review: null,
            showReview: false,
            childClass: null
        };
    }

    render(){
        const {showReview, review, childClass} = this.state;
        const {children} = this.props;

        let content;

        if(showReview){
            content = <FullReview review={review} close={() => this.startAnimation()}/>
        }
        else {
            content = React.Children.map(children, child => {
                return React.cloneElement(child, {showReview: review => this.startAnimation(review)})
            });
        }
        return (
            <div className={childClass} id="div-review-visibility-container">
                {content}
            </div>
        );
    }

    startAnimation(review: Object | void){
        const state = {...this.state};
        const {classes} = this.props;

        if(review){
            state.review = review;
        }

        this.setState({
            ...state,
            childClass: classes.flipOutY
        });

        setTimeout(() => this.animateOutFinished(), 1000);
    }

    animateOutFinished(){
        const {showReview} = this.state;
        const {classes} = this.props;

        this.setState({
            ...this.state,
            showReview: !showReview,
            childClass: classes.flipInY
        });
    }

}

export default withStyles(styles)(ReviewVisiblityHandler);