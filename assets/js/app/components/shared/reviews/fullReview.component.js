// @flow

import React, { Component } from 'react';

import Paper from '@material-ui/core/Paper';
import {withStyles} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

import getTitle from './lib/title';
import getHeadline from './lib/headline';

type Props = {
    review: Object,
    close: Function,
    classes: Object
};

const styles = theme => ({
    paper: {
        padding: '20px'
    },
    table: {
        marginBottom: '25px'
    },
    title: {
        marginBottom: '15px'
    },
    inline: {
        display: 'inline-block'
    }
});

class FullReview extends Component<Props> {
    constructor(props: Props){
        super(props);
    }

    render(){
        const {classes, review, close} = this.props;

        if(!Object.keys(review).length)
            return;

        return (
            <Paper className={classes.paper}>
                <Typography variant="title" className={classes.title}>
                    {getTitle(review.step === 'done')}
                </Typography>

                <Typography variant="subheading" color="textSecondary">
                    {getHeadline(review)}
                </Typography>

                {this.getContactInfo(review)}

                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>
                                Question
                            </TableCell>
                            <TableCell>
                                Answer
                            </TableCell>
                            <TableCell>
                                Question Type
                            </TableCell>
                            <TableCell>
                                Is Indicator
                            </TableCell>
                        </TableRow>
                    </TableHead>

                    <TableBody>
                        {this.getAnswers(review.answers, review.questions)}
                    </TableBody>

                    {this.getAdditionalComments(review)}
                </Table>

                <Button variant="raised" onClick={close}>Back</Button>
            </Paper>
        );
    }

    getAdditionalComments(review: Object){
        if(!review.additionalComments)
            return;

        return (
            <div>
                <Typography>
                    Additional Comments:
                </Typography>
                <Typography>
                    {review.additionalComments}
                </Typography>
            </div>
        );
    }

    getAnswers(answers: Array<Object>, questions: Array<Object>): Array<TableRow>{
        if(!questions) {
            return [
                <TableRow>
                    <TableCell colSpan={4}>No Answers</TableCell>
                </TableRow>
            ];
        }
        return answers.map((answer, index) => {
            const question = questions[index];

            return (
                <TableRow key={index}>
                    <TableCell>
                        {question.text}
                    </TableCell>
                    <TableCell>
                        {answer.answer}
                    </TableCell>
                    <TableCell>
                        {question.type}
                    </TableCell>
                    <TableCell>
                        {question.indicator.isIndicator ? 'Yes' : 'No'}   
                    </TableCell>
                </TableRow>
            );
        });
    }

    getContactInfo(review: Object){
        if(!review.contact)
            return;

        const {classes} = this.props;

        return (
            <div>
                <div>
                    <Typography variant="subheading" className={classes.inline}>
                        Phone Number
                    </Typography>
                    &nbsp;
                    <Typography variant="subheading" className={classes.inline}>
                        {review.phoneNumber}
                    </Typography>
                </div>
                <div>
                    <Typography variant="subheading" className={classes.inline}>
                        Email Address
                    </Typography>
                    &nbsp;
                    <Typography variant="subheading" className={classes.inline}>
                        {review.email}
                    </Typography>
                </div>
            </div>
        );
    }

}

export default withStyles(styles)(FullReview);
