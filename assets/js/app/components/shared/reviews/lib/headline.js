// @flow
import getLocalDateString from '../../../../services/date';

export default getHeadline;

function getHeadline(review: Object): string{
    const firstValue = getFirstValue(review);

    const created = getDateString(review);

    return `${firstValue} ${created}`;
}

function getFirstValue(review: Object): string{
    let firstValue: string;

    if(!review.contact){
        firstValue = 'Anonymous';
    }
    else {
        firstValue = review.name || review.email || review.phoneNumber;
    }

    return firstValue;
}

function getDateString(review: Object): string{ 
    let date: string;
    let value: string;

    if(review.step === 'done' && review.completed){
        date = review.completed.$date;
        value = 'Completed';
    }
    else {
        date = review.created.$date;
        value = 'Created';
    }

    return `${value} ${getLocalDateString(date, 'MM/DD/YYYY')}`;
}