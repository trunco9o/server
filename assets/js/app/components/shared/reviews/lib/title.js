// @flow
export default getTitle;

function getTitle(done: boolean): string{
    return `${done ? 'Completed' : 'In Progress'} Review`;
}