import React, {Component} from 'react';

import { withStyles } from '@material-ui/core/styles';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';

import Menu from '@material-ui/icons/Menu';

const styles = theme => ({
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
    },
    flex: {
        flex: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    toolbar: theme.mixins.toolbar,
});

class NavBar extends Component{
    constructor(props){
        super(props);
    }

    render(){
        const { classes, title, isLoggedIn, color, showButton } = this.props;

        const style = {};

        if(color)
            style['backgroundColor'] = color;

        return ( 
            <AppBar position="absolute" className={classes.appBar} style={style}>
                <Toolbar>
                    {this.getButton(showButton, classes)}
                    <Typography variant="title" color="inherit" className={classes.flex}>
                        {title}
                    </Typography>
                    {this.getAction(isLoggedIn)}
                </Toolbar>
            </AppBar>
        );
    }

    getAction(isLoggedIn){
        if(isLoggedIn){
            return <Button color="inherit">Log Out</Button>
        }

        return '';
    }

    getButton(showButton, classes){
        if(showButton)
            return (
                <IconButton className={classes.menuButton} color="inherit" onClick={() => this.emitToggle()}>
                    <Menu/>
                </IconButton>
            );
    }

    emitToggle(){
        this.props.toggleMenuEventEmitter.emit('toggle');
    }

}

export default withStyles(styles)(NavBar)

NavBar.defaultPropTypes = {
    isLoggedIn: true,
    showButton: false
};