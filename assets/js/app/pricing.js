import React from 'react';
import ReactDOM  from 'react-dom';
import EmailSignup from './components/email-signup.component';

var element = document.getElementById('center');

ReactDOM.render(<EmailSignup title="Protect your rep!" subtitle="Sign up to get notified when we launch"/>, element);