import re

contact_info = "Can we share your contact info with %s?"

done = "Thank you!"

email_address = "If you would like to provide your email address to %s, please share it now. Otherwise reply \"no\"."

name = "If you would like to provide your first and/or last name to %s, please share it now. Otherwise reply \"no\"."

positive = [
    "yes",
    "ya",
    "yeah",
    "yea",
    "yee",
    "yah",
    "yep",
    "hell ya",
    "hell yes",
    "hell yeah",
    "fuck yeah",
    "fuck ya",
    "definitely",
    "si",
    "1",
    "yp",
    "yeaah",
    "ooooo ya",
    "oh ya",
    "def",
    "mos def",
    "most def",
    "most definitely",
    "ok",
    "okay",
    "k",
    "okey-dokey",
    "by all means",
    "affirmative",
    "aye aye",
    "roger",
    "10-4",
    "uh-huh",
    "righto",
    "very well",
    "yup",
    "yuhp",
    "yuppers",
    "yeppers",
    "right on",
    "ja",
    "surely",
    "amen",
    "fo' shizzle",
    "fo shizzle",
    "totally",
    "sure",
    "yessir",
    "yesssir",
    "yes maam",
    "yes ma am",
    "yes ma'am",
    "yaaaaaassss",
    "yeses",
    "yesssssssss",
    "yesyesyes",
    "kk",
    "kkk",
    "kkkk",
    "okie",
    "cool",
    "kewl"
]

negative = [
    "no",
    "nah",
    "no way",
    "never",
    "fuck off",
    "fuck no",
    "hell no"
]

positiveRegex = re.compile("y{1,}e*a*(s*|h*|a*)(u*p|u*m)*r*s*[a-z]*|y+u{1,}h?p+(er)*s*|o{1,}(h|u|e)k{1,}(ay|ey)*s*")

negativeRegex = re.compile("no")