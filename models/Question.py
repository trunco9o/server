import datetime

from pymongo import MongoClient
from bson.objectid import ObjectId
from bson.json_util import dumps

db = MongoClient().reviews

questionDb = db.question

class QuestionModel:
    """
    """
    
    @staticmethod
    def create():
        """
        Create a new Question
        """

        question = {
            'indicator': {
                'isIndicator': False,
                'positiveCutoff': 3,
                'higherIsGood': True,
                'booleanIndicator': 'yes'
            }
        }

        question['created'] = datetime.datetime.utcnow()

        #Create a question
        return question

    @staticmethod
    def getById(id):
        """
        Retrieve a question from the database
        """

        #Find question by ID
        question = questionDb.find_one({'_id': ObjectId(id)})
        
        return question

    @staticmethod
    def getBySurveyId(id):
        questions = questionDb.find({'surveyId': ObjectId(id)})

        return questions

    @staticmethod
    def getTotalQuestions(id):
        questionCount = questionDb.count({'surveyId': ObjectId(id)})

        return questionCount

    @staticmethod
    def getByIndex(surveyId, index):
        allQuestions = questionDb.find({})

        question = questionDb.find({'surveyId': ObjectId(surveyId)})[index]

        return question

    @staticmethod
    def save(question):
        res = questionDb.insert_one(question)

        return QuestionModel.getById(res.inserted_id)

    @staticmethod
    def delete(question):
        id = question['_id']
        
        query = {'_id': ObjectId(id)}

        questionDb.remove(query)

    @staticmethod
    def validate(json, surveyId, update=False):
        valid = True
        errors = {}

        if update is True:
            if QuestionModel.getTotalQuestions(surveyId) >= 5:
                errors['total'] = 'Your survey already has 5 questions. Please delete one to add this one.'
                valid = False

        if not 'type' in json or not json['type']:
            errors['type'] = 'Please add a question type'
            valid = False
        
        #Get the question type from the form
        questionType = json['type']

        #Check the question type
        isRange = (questionType == 'range')

        if isRange and (not 'scale' in json or not json['scale']):
            errors['scale'] = 'Please add a range'
            valid = False

        if not 'indicator' in json or not json['indicator']:
            errors['indicator'] = 'Indicator is required on question'
            valid = False

        else:
            indicator = json['indicator']
            
            if json['type'] == 'range' and (not 'positiveCutoff' in indicator or not indicator['positiveCutoff']):
                errors['positiveCutoff'] = 'Please add a cutoff for a negative review'
                valid = False

            if json['type'] == 'range' and (not 'higherIsGood' in indicator or not type(indicator['higherIsGood']) is bool): 
                errors['higherIsGood'] = 'Please select how to determine if this is a positive review'
                valid = False
            
            if json['type'] == 'yes-no' and (indicator['booleanIndicator'] != 'yes' and indicator['booleanIndicator'] != 'no'):
                errors['booleanIndicator'] = 'Please select how to determine if this is a positive review'
                valid = False

        if valid is True:
            return valid
        else:
            return errors

    
    @staticmethod
    def update(id, update):
        update['updated'] = datetime.datetime.utcnow()

        questionDb.update_one({'_id': ObjectId(id)}, {'$set': update}, upsert=False)

        return QuestionModel.getById(id)