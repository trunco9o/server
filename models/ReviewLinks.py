import datetime
import hashlib
import re

from pymongo import MongoClient
from bson.objectid import ObjectId

db = MongoClient().reviews

reviewLinkDb = db.reviewLink

class ReviewLinksModel:
    """
    """

    urlRegex = re.compile('^(http)s?:\/\/')

    @staticmethod
    def delete(userId, site):
        """
        """

        reviewLinkDb.remove({'userId': ObjectId(userId), 'site': site})

    @staticmethod
    def create():
        """
        """

        #Create a new ReviewLinks
        reviewLinks =  {}

        #Set the userId on reviewLinks
        reviewLinks['created'] = datetime.datetime.utcnow()

        return reviewLinks

    @staticmethod
    def getBySite(id, siteName):
        query = {'userId': id, 'site': siteName}

        reviewLinks = reviewLinkDb.find_one(query)

        return reviewLinks

    @staticmethod
    def update(userId, reviewId, reviewLink):
        hashed = hashlib.md5(reviewLink['url'].encode('utf-8')).hexdigest()

        url = reviewLink['url']

        urlMatch = ReviewLinksModel.urlRegex.match(url)
        fixedUrl = url

        if urlMatch is None:
            fixedUrl = 'http://%s' % url

        reviewLink['url'] = fixedUrl

        query = {'_id': ObjectId(reviewId)}
        update = {'$set': reviewLink}

        res = reviewLinkDb.update_one(query, update, upsert=False)

        return ReviewLinksModel.getById(reviewId)
        
    @staticmethod
    def save(reviewLink):
        url = reviewLink['url']

        urlMatch = ReviewLinksModel.urlRegex.match(url)
        fixedUrl = url

        if urlMatch is None:
            fixedUrl = 'http://%s' % url

        reviewLink['url'] = fixedUrl

        res = reviewLinkDb.insert_one(reviewLink)

        return  ReviewLinksModel.getById(res.inserted_id)

    @staticmethod
    def getByUserId(id, includeIds=False, timestamps=False):
        """
        """

        #Set up base query
        query = {'userId': ObjectId(id)}

        reviewLinks = reviewLinkDb.find(query)

        return reviewLinks

    @staticmethod
    def getById(id):
        query = {'_id': ObjectId(id)}

        reviewLink = reviewLinkDb.find_one(query)

        return reviewLink

    @staticmethod
    def getReviewLinkByHash(hash):
        reviewLink = reviewLinkDb.find_one({'hash': hash})

        return reviewLink

    @staticmethod
    def createHash(reviewLink):
        hashed = hashlib.md5(reviewLink['url'].encode('utf-8')).hexdigest()
        
        reviewLink['hash'] = hashed[0:10]

    @staticmethod
    def getReviewLinksString(userId):
        reviewLinks = ReviewLinksModel.getByUserId(userId)

        return ''

    @staticmethod 
    def delete(reviewLink):
        id = reviewLink['_id']
        
        query = {'_id': ObjectId(id)}

        reviewLinkDb.remove(query)