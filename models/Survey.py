import datetime

from pymongo import MongoClient
from bson.objectid import ObjectId

db = MongoClient().reviews

surveyDb = db.survey

class SurveyModel:
    """
    SurveyModel handles interaction with connection to find and create a Survey in the database.
    """

    @staticmethod
    def getByUserId(id):
        """
        Retrieve a survey for a given user
        """

        #Find survey by user ID
        survey = surveyDb.find_one({'userId': ObjectId(id)})

        return survey

    @staticmethod
    def getById(id):
        """
        Retreive a survey by ID
        """

        #Find survey by ID
        survey = surveyDb.find_one({'_id': ObjectId(id)})

        return survey

    @staticmethod
    def create():
        survey = {}

        survey['created'] = datetime.datetime.utcnow()

        return survey

    @staticmethod
    def setIndicator(id, indicator):
        update = {
            'hasIndicators': indicator
        }

        return SurveyModel.update(id, update)

    @staticmethod
    def save(survey):
        result = surveyDb.insert_one(survey)

        survey['_id'] = result.inserted_id

        return survey

    @staticmethod
    def update(id, update):
        update['updated'] = datetime.datetime.utcnow()

        surveyDb.update_one({'_id': ObjectId(id)}, {'$set': update}, upsert=False)

        return SurveyModel.getById(id)

    @staticmethod
    def validateUpdate(update):
        valid = True
        errors = []
        if not 'trigger' in update or not update['trigger']:
            valid = False
            errors.append({'trigger': 'A trigger word is required'})

        if not 'invalidReviewMessage' in update or not update['invalidReviewMessage']:
            valid = False
            errors.append({'negativeReviewMessage': 'An invalid review message is required'})

        if not 'negativeReviewMessage' in update or not update['negativeReviewMessage']:
            valid = False
            errors.append({'negativeReviewMessage': 'A negative review message is required'})

        if not 'positiveReviewMessage' in update or not update['positiveReviewMessage']:
            valid = False
            errors.append({'positiveReviewMessage': 'A positive review message is required'})

        if not 'positiveCutoff' in update or not update['positiveCutoff']:
            valid = False
            errors.append({'positiveCutoff': 'A cutoff point is required'})

        if valid:
            return valid
        else:
            return errors