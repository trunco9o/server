from os import getenv
import datetime, time
import json

from pymongo import MongoClient
from bson.objectid import ObjectId

from assets.strings.python import responses

db = MongoClient().reviews

reviewDb = db.reviews

class ReviewModel:
    """
    ReviewModel handles interaction with connection to find and create a Review in the database.
    TO BE REFACTORED
    """

    @staticmethod
    def create():
        """
        """

        #Create a new Review
        review =  {
            'answers': []
        }

        review['created'] = datetime.datetime.utcnow()

        return review

    @staticmethod
    def save(review):
        res = reviewDb.insert_one(review)

        id = res.inserted_id

        return ReviewModel.getById(id)

    @staticmethod
    def update(id, update):
        update['updated'] = datetime.datetime.utcnow()

        reviewDb.update_one({'_id': ObjectId(id)}, {'$set': update}, upsert=False)

        updatedReview = ReviewModel.getById(id)

        return ReviewModel.getById(id)

    # Getters
    @staticmethod
    def getByUserId(userId, selectIds=False, fromDate=False, toDate=False, complete=False, questions=False, query={}):
        """
        """
        query['userId']: ObjectId(userId)

        if selectIds:
            query['_id'] = {
                '$in': [ObjectId(id) for id in selectIds]
            }

        if fromDate and toDate:
            fromDate = time.strptime(fromDate, '%Y-%m-%d')
            toDate = time.strptime(toDate, '%Y-%m-%d')

            fromDate = time.mktime(fromDate)
            toDate = time.mktime(toDate)

            fromDate = datetime.datetime.fromtimestamp(fromDate)
            toDate = datetime.datetime.fromtimestamp(toDate)

            query['created'] = {
                '$lte': toDate,
                '$gte': fromDate
            }

        if complete:
            query['step'] = 'done'

        print(questions)
        if questions:
            reviews = reviewDb.aggregate([
                {'$match': query},
                {'$unwind': '$answers'},
                {
                    "$lookup": {
                        "from": "question",
                        "localField": "answers.questionId",
                        "foreignField": "_id",
                        "as": "questions"
                    },
                },
                {'$unwind': '$questions'},
                {
                    "$group": {
                        "_id": "$_id",
                        "answers": {"$push": "$answers"},
                        "questions": {"$push": "$questions"}, 
                        "userId": {"$first": "$userId"},
                        "created": {"$first": "$created"},
                        "phoneNumber": {"$first": "$phoneNumber"},
                        "completed": {"$first": "$completed"},
                        "email": {"$first": "$email"},
                        "name": {"$first": "$name"},
                        "contact": {"$first": "$contact"},
                        "step": {"$first": "$step"},
                        "updated": {"$first": "$updated"},
                        "score": {"$first": "$score"},
                        "isPositive": {"$first": "$isPositive"},
                        "isInvalid": {"$first": "$isInvalid"},
                        "additionalComments": {"$first": "$additionalComments"}
                    }
                },
            ])
        else: 
            #Find reviews by user ID
            reviews = reviewDb.find(query)

        return reviews

    @staticmethod
    def getUserContacts(userId, selectIds=False):
        query = {'userId': ObjectId(userId), 'contact': True}

        if selectIds:
            query['_id'] = {
                '$in': [ObjectId(id) for id in selectIds]
            }

        reviews = db.reviews.find(query)

        return reviews

    @staticmethod
    def getNewest(userId, limit):
        """
        """

        review = db.reviews.find({'userId': ObjectId(userId)}).sort('created', -1).limit(limit)

        return review

    @staticmethod
    def getByCode(userId, code, fromDate=False, toDate=False, complete=False, questions=False):
        """
        """

        query = {'code': code}
        
        return ReviewModel.getByUserId(userId, query=query, fromDate=fromDate, toDate=toDate, complete=complete, questions=questions)

    @staticmethod
    def getByPhoneNumber(userId, reviewPhoneNumber, fromDate=False, toDate=False, complete=False, questions=False):
        """
        """

        query = {'phoneNumber': reviewPhoneNumber}

        return ReviewModel.getByUserId(userId, query=query, fromDate=fromDate, toDate=toDate, complete=complete, questions=questions)

    @staticmethod
    def getIncompleteByPhoneNumber(userId, reviewPhoneNumber):
        """
        """

        reviews = db.reviews.find({'userId': ObjectId(userId), 'phoneNumber': reviewPhoneNumber, 'completed': {'$exists': False}})
        
        return reviews
    
    @staticmethod
    def getById(id):
        """
        """

        #Find review by ID
        review = db.reviews.find_one({'_id': ObjectId(id)})

        return review  
 
    # Setters
    @staticmethod
    def reviewInvalid(review):
        update = {'invalid': True}
        
        return ReviewModel.update(review['_id'], update)

    @staticmethod
    def setCode(id, code):
        update = {
            'code': code
        }

        return ReviewModel.update(id, update)

    @staticmethod
    def addAnswer(id, response, question):
        review = ReviewModel.getById(id)

        answers = review['answers']

        answers.append({
            'questionId': question['_id'],
            'answer': response
        })

        update = {
            'answers': answers
        }

        return ReviewModel.update(id, update)

    @staticmethod
    def setContact(id, contact):
        update = {
            'contact': contact
        }

        return ReviewModel.update(id, update)

    @staticmethod
    def setEmail(id, email):
        update = {
            'email': email
        }

        return ReviewModel.update(id, update)

    @staticmethod
    def setName(id, name):
        update = {
            'name': name
        }

        return ReviewModel.update(id, update)

    @staticmethod
    def updateStep(id, step):
        update = {
            'step': step,
        }

        return ReviewModel.update(id, update)

    @staticmethod
    def updateQuestionIndex(id, index):
        update = {
            'questionIndex': index
        }

        return ReviewModel.update(id, update)

    @staticmethod
    def setContact(id, contact):
        update = {
            'contact': contact
        }

        return ReviewModel.update(id, update)

    @staticmethod
    def setPositive(id, isPositive):
        update = {
            'isPositive': isPositive
        }

        return ReviewModel.update(id, update)

    @staticmethod
    def setInvalid(id, invalid):
        update = {
            'isInvalid': invalid
        }

        return ReviewModel.update(id, update)

    # Methods

    @staticmethod
    def checkReviewFrequency(userId, frequency, _from):
        date = datetime.date.today()

        if frequency == 'daily':
            date = date - datetime.timedelta(days=1)
        elif frequency == 'weekly':
            date = date - datetime.timedelta(days=7)
        elif frequency == 'monthly':
            date = date - datetime.timedelta(months=1)
        elif frequency == '60 days':
            date = date - datetime.timedelta(days=60)
        else:
            date = date - datetime.timedelta(days=90)

        total = reviewDb.find({'created': {'$gte': date}, 'phoneNumber': _from}).count()

        return total > 0

    @staticmethod
    def getPositiveReviewMessage(reviewId, survey, sendDetailedReview, detailedReviewMessage="Please provide additional comments here:"):

        if sendDetailedReview == 'positive reviews' or sendDetailedReview == 'always':
            reviewUrl = ReviewModel.makeReviewUrl(reviewId)

            return "%s\n%s %s" % (survey['positiveReviewMessage'], detailedReviewMessage, reviewUrl)
        else:
            return survey['positiveReviewMessage']

    @staticmethod
    def getNegativeReviewMessage(reviewId, survey, sendDetailedReview, detailedReviewMessage="Please provide additional comments here:"):

        if sendDetailedReview == 'negative reviews' or sendDetailedReview == 'always' or sendDetailedReview == 'negative and invalid reviews':
            reviewUrl = ReviewModel.makeReviewUrl(reviewId)

            return "%s\n%s %s" % (survey['negativeReviewMessage'], detailedReviewMessage, reviewUrl)
        else:
            return survey['negativeReviewMessage']

    @staticmethod
    def getInvalidReviewMessage(reviewId, survey, sendDetailedReview, detailedReviewMessage="Please provide additional comments here:"):
        if sendDetailedReview == 'invalid reviews' or sendDetailedReview == 'always' or sendDetailedReview == 'negative and invalid reviews':
            reviewUrl = ReviewModel.makeReviewUrl(reviewId)

            return "%s\n%s %s" % (survey['invalidReviewMessage'], detailedReviewMessage, reviewUrl)
        else:
            return survey['invalidReviewMessage']

    @staticmethod
    def gradeReview(id, survey, questions):
        review = ReviewModel.getById(id)
        answers = review['answers']
        invalid = False
        positive = True

        total = 0
        graded = 0

        for index, question in enumerate(questions):
            grade = ReviewModel.gradeQuestion(question, answers[index])

            if grade == 'invalid':
                invalid = True
                graded += 1

            elif grade is not None and grade >= 0:
                graded += 1
                total += grade

        if graded:
            score = total / graded
            ReviewModel.update(id, {'score': score * 100})

        if invalid:
            ReviewModel.setInvalid(id, True)

        positive = (score >= survey['positiveCutoff'])

        return ReviewModel.setPositive(id, positive)
    
    @staticmethod
    def gradeQuestion(question, answer):
        indicator = question['indicator']
        qType = question['type']

        res = None

        if not indicator['isIndicator'] or qType == 'open-ended':
            return res
        elif qType == 'yes-no':
            res = ReviewModel.checkBooleanIndicator(indicator, answer['answer'])
        
        elif qType == 'range':
            res = ReviewModel.checkRangeIndicator(indicator, answer['answer'])

        return res

    @staticmethod
    def checkBooleanIndicator(indicator, answer):
        formattedAnswer = answer.lower()
        booleanIndicator = indicator['booleanIndicator']
        value = 0

        if any(formattedAnswer in s for s in responses.positive) or responses.positiveRegex.match(formattedAnswer):
            if booleanIndicator == 'yes':
                value = 1
        
        elif any(formattedAnswer in s for s in responses.negative) or responses.negativeRegex.match(formattedAnswer):
            if booleanIndicator == 'no':
                value = 1
        else:
            value = 'invalid'
        
        return value

    @staticmethod
    def checkRangeIndicator(indicator, answer):
        parsedAnswer = False

        try:
            parsedAnswer = int(float(answer))
        except Exception:
            return 'invalid'
        
        value = 0

        if indicator['higherIsGood']:
            if indicator['positiveCutoff'] <= parsedAnswer:
                value = 1
        else:
            if indicator['positiveCutoff'] >= parsedAnswer:
                value = 1

        return value

    @staticmethod
    def setCompleted(id):
        update = {
            'completed': datetime.datetime.utcnow()
        }

        return ReviewModel.update(id, update)

    @staticmethod
    def makeReviewUrl(reviewId):
        url = getenv('URL')

        reviewUrl = "%s/review/%s" % (url, reviewId)

        return reviewUrl

    @staticmethod
    def getAggregation(startDate, endDate):
        aggregation = db.reviews.aggregate([
            {"$match" : { "created" : { "$gte": startDate, "$lt": endDate} } },
            {
                "$addFields":{
                    "created":{
                        "$dateFromParts":{
                            "year":{
                                "$year":"$created"
                            },
                            "month":{
                                "$month":"$created"
                            }, 
                            "day":{
                                "$dayOfMonth" : "$created" 
                            }
                        }
                    }, 
                    "dateRange":{
                        "$map":{
                            "input":{
                                "$range":[0, {"$divide": [{"$subtract":[endDate,startDate]}, 1000]}, 60*60*24]
                            },
                            "in":{
                                "$add":[startDate, {"$multiply": ["$$this", 1000]}]
                            }
                        }
                    }
                }
            },
            {"$unwind":"$dateRange"},
            {
                "$group":{
                    "_id":"$dateRange", 
                    "reviews":{
                        "$push":{
                            "$cond":[
                                {"$eq":["$dateRange","$created"]},
                                {"count": 1},
                                {"count":0}
                            ]
                        }
                    },
                    "positiveReviews": {
                        "$push":{
                            "$cond":[
                                {"$and": [{"$eq": ["$dateRange","$created"]}, {"$eq": ["$isPositive", True]}]},
                                {"count": 1},
                                {"count":0}
                            ]
                        }
                    },
                    "negativeReviews": {
                        "$push":{
                            "$cond":[
                                {"$and": [{"$eq": ["$dateRange","$created"]}, {"$eq": ["$isPositive", False]}]},
                                {"count": 1},
                                {"count":0}
                            ]
                        }
                    },
                    "invalidReviews": {
                        "$push":{
                            "$cond":[
                                {"$and": [{"$eq": ["$dateRange","$created"]}, {"$eq": ["$isInvalid", True]}]},
                                {"count": 1},
                                {"count":0}
                            ]
                        }
                    }
                }
            },
            {"$sort":{"_id":1}},
            {"$project":{
                "_id":0,
                "date":"$_id",
                "totalReviews":{"$sum":"$reviews.count"},
                "positiveReviews":{"$sum": "$positiveReviews.count"},
                "negativeReviews":{"$sum": "$negativeReviews.count"},
                "invalidReviews":{"$sum": "$invalidReviews.count"},
            }}
        ])

        return aggregation