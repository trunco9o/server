import datetime
import hashlib
import uuid

from services.TwilioService import twilio_service
from services.StripeService import stripe_service

from .ReviewLinks import ReviewLinksModel
from .Survey import SurveyModel

from pymongo import MongoClient
from bson.objectid import ObjectId

db = MongoClient().reviews

userDb = db.user

class UserModel:
    """
    """

    @staticmethod
    def create(userJson=None):
        #Create a new user
        user =  {}

        if userJson is not None:
            user['name'] = userJson['name']
            user['business'] = userJson['businessName']
            user['email'] = userJson['email']
            user['phone'] = userJson['phoneNumber']

            user['emailVerification'] = uuid.uuid4().hex

            if not 'settings' in userJson:
                user['settings'] = {
                    'generateReviewCode': False
                }
            else: 
                user['settings'] = {}

                if not 'generateReviewCode' in userJson['settings']:
                    user['settings']['generateReviewCode'] = False
                else:
                    user['settings']['generateReviewCode'] = userJson['settings']['generateReviewCode']

            UserModel.setPassword(user, userJson['password'])

        user['created'] = datetime.datetime.utcnow()
        return user

    @staticmethod
    def createCustomer(user):
        stripeCustomerId = stripe_service.createCustomer(user['email'])

        user['customerId'] = stripeCustomerId
        
        return user

    @staticmethod
    def createSmsNumber(user):
        phoneNumber = user['phone']

        number = twilio_service.createNumber(phoneNumber[0:3])

        user['textNumber'] = number

        user['smsSetup'] = True

        return user

    @staticmethod
    def getById(id, reviewLinks=False, survey=False, ignoreFields=False):
        """
        Get a user by a given ID
        """

        if ignoreFields:
            #Lookup user in DB
            return userDb.find_one({'_id': ObjectId(id)}, ignoreFields)
        else:
            return userDb.find_one({'_id': ObjectId(id)})
    
    @staticmethod
    def getByTextNumber(textNumber, reviewLinks=False, survey=False):
        """
        Get a user by a given 'textNumber' / twilio sms number
        """

        #Lookup user in DB
        user = userDb.find_one({'textNumber': textNumber})

        #If reviewLinks are requested, add them to the user object
        if reviewLinks == True and user:
            user['reviewLinks'] = list(ReviewLinksModel.getByUserId(user['_id']))

        #If survey is requested, add it to user object
        if survey == True and user:
            user['smsSurvey'] = SurveyModel.getByUserId(user['_id'])
        
        return user

    @staticmethod
    def getByToken(token):
        #Lookup user in DB
        user = userDb.find_one({'emailVerification': token})

        return user

    @staticmethod
    def verify(token):
        user = UserModel.getByToken(token)

        if user is None:
            return False

        user['verified'] = True
        user['emailVerification'] = ''

        return user


    @staticmethod
    def setHasSurvey(userId, value):
        user = userDb.find_one({'_id': userId})

        user['hasSurvey'] = value

    @staticmethod
    def setPassword(user, password):
        salt = uuid.uuid4().hex
        hashed_password = hashlib.sha512(password.encode('utf-8') + salt.encode('utf-8')).hexdigest()

        user['password'] = hashed_password
        user['salt'] = salt

    @staticmethod
    def checkLogin(user, password):
        hashed_password = hashlib.sha512(password.encode('utf-8') + user['salt'].encode('utf-8')).hexdigest()

        return user['password'] == hashed_password

    @staticmethod
    def getByEmail(email):
        user = userDb.find_one({'email': email})

        return user

    @staticmethod
    def save(user):
        result = userDb.insert_one(user)

        user['_id'] = result.inserted_id

        return user

    @staticmethod
    def updateSettings(userId, prop, value):
        update = {
            'settings': {}
        }

        update['settings'][prop] = value

        return UserModel.update(userId, update)

    @staticmethod
    def update(userId, update):
        update['updated'] = datetime.datetime.utcnow()

        if('_id' in update):
            update.pop('_id', None)

        if('created' in update):
            update.pop('created', None)

        userDb.update_one({'_id': ObjectId(userId)}, {'$set': update}, upsert=False)

        return UserModel.getById(userId, ignoreFields={'password': 0, 'salt': 0})


    @staticmethod
    def validateUserJson(userJson, validatePassword=False):
        errorMessage = {}

        if not 'name' in userJson or not userJson['name']:
            errorMessage['name'] = 'First and last name are required'
        
        if not 'email' in userJson or not userJson['email']:
            errorMessage['email'] = 'Email is required'
        
        if not 'business' in userJson or not userJson['business']:
            errorMessage['business'] = 'Business name is required'
        
        if not 'phone' in userJson or not userJson['phone']:
            errorMessage['phone'] = 'Phone number is required'

        if validatePassword: 

            if not 'password' in userJson or not userJson['password']:
                errorMessage['password'] = 'Password is required'
        
            if not 'confirmPassword' in userJson or not userJson['confirmPassword']:
                errorMessage['passwordConfirmation'] = 'Confirm your password'

            if userJson['password'] != userJson['confirmPassword']:
                errorMessage['passwordConfirmation'] = 'Password confirmation doesn\'t match'

        if len(errorMessage) > 0:
            return errorMessage

        else:
            return True