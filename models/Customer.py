import datetime

from pymongo import MongoClient
from bson.objectid import ObjectId

db = MongoClient().reviews

customerDb = db.customers

class CustomerModel:
    """
    """

    @staticmethod
    def getByUserId(userId, selectIds=False):
        """
        """

        query = {'userId': ObjectId(userId)}

        if selectIds:
            query['_id'] = {
                '$in': [ObjectId(id) for id in selectIds]
            }

        customers = customerDb.find(query)

        return customers

    @staticmethod
    def create():
        """
        """
        customer = {}

        customer['created'] = datetime.datetime.utcnow()

        return customer

    @staticmethod
    def save(customer):
        """
        """
        customerDb.insert_one(customer)