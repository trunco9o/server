import datetime

from pymongo import MongoClient
from bson.objectid import ObjectId

db = MongoClient().reviews

retiredQuestionDb = db.retiredQuestion

class RetiredQuestionModel:
    """
    RetiredQuestionModel handles interaction with connection to find and create Retired Questions in the database.
    TO BE REFACTORED
    """
    
    @staticmethod
    def create():
        """
        Create a RetiredQuestion
        """

        retiredQuestion = {}

        retiredQuestion['created'] = datetime.datetime.utcnow()

        #Create a question
        return retiredQuestion

    @staticmethod
    def getByOriginalId(id):
        """
        Retrieve a question from the database
        """

        #Find question by ID
        question = retiredQuestionDb.find_one({'originalId': ObjectId(id)})

        return question

    @staticmethod
    def save(question):
        retiredQuestionDb.insert_one(question)