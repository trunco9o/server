import datetime

from pymongo import MongoClient

db = MongoClient().reviews

emailListDb = db.emailList
class EmailListModel:
    """
    """
    
    @staticmethod
    def create(email):
        """
        Create a new Question
        """

        #Create a question
        emailObject = {}

        emailObject['email'] = email

        emailObject['created'] = datetime.datetime.utcnow()

        emailListDb.insert_one(emailObject)