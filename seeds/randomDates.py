import random
from datetime import datetime
from calendar import monthrange
 
year = random.choice(range(1950, 2001))
month = random.choice(range(1, 13))
day = random.choice(range(1, 29))
birth_date = datetime(year, month, day)

def getRandomDatePastMonth():
    today = datetime.now()

    year = today.year
    month = today.month
    
    day = random.choice(range(1, today.day))

    return datetime(year, month, day)

def getRandomDatePastNMonths(numberOfMonths):
    today = datetime.now()

    month = today.month

    if(numberOfMonths >= month):
        numberOfMonths = random.choice(range(1, month))

    difference = (month - numberOfMonths)

    monthRange = range(difference, month)
    
    year = today.year
    chosenMonth = random.choice(monthRange)


    dayRange = monthrange(year, chosenMonth)

    maxDays = dayRange[1]

    chosenDay = random.choice(range(1, maxDays))

    return datetime(year, chosenMonth, chosenDay)