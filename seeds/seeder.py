#import sys to accept cli arguments
import sys

from .seed import seed

#Import app for env loading
import app

if(len(sys.argv) > 1):
    param = sys.argv[1]

    paramInfo = param.split('=')

    if paramInfo[0] == 'reviews':
        numberOfReviews = int(paramInfo[1])

        seed(numberOfReviews)
else:
    seed()