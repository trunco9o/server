from os import getenv
from pathlib import Path

from dotenv import load_dotenv
import stripe

env_path = str(Path('.') / '.env')
load_dotenv(dotenv_path=env_path)

secret = getenv('STRIPE_SECRET')

stripe.api_key = secret

product = stripe.Product.create(
    name='RepGuru Subscription',
    type='service'
)

productId = product['id']

plan = stripe.Plan.create(
  product=productId,
  nickname='Tiered Pricing',
  interval='month',
  currency='usd',
  billing_scheme='tiered',
  tiers_mode='graduated',
  tiers=[
      {
          'amount': 25,
          'up_to': 1500
      },
      {
          'amount': 20,
          'up_to': 5000
      },
      {
          'amount': 15,
          'up_to': 'inf'
      }
  ],
  usage_type='metered'
)