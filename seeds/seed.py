#Import datetime for dates 
from datetime import datetime

#Import MongoKit schemas from models folder 
from models.User import userDb, UserModel
from models.ReviewLinks import reviewLinkDb, ReviewLinksModel
from models.Survey import surveyDb, SurveyModel
from models.Question import questionDb, QuestionModel
from models.Review import reviewDb, ReviewModel
from models.Customer import customerDb, CustomerModel

from seeds.randomDates import getRandomDatePastMonth, getRandomDatePastNMonths

#import getenv to get phone number
from os import getenv

#Import app for env loading
import app as app

def seed(reviews = 0):
    #Delete all existing users
    userDb.delete_many({})

    #Create a user
    user = UserModel.create()

    #Set user data
    user['name'] = 'test'
    user['business'] = 'test'
    user['email'] = 'test@test.com'
    user['textNumber'] = getenv('TWILIO_NUMBER', '1234567890')
    user['verified'] = True
    user['phone'] = '12345'

    if user['textNumber']:
        user['smsSetup'] = True

    user['settings'] = {
        # Keep as false for unit tests
        'generateReviewCode': False
    }

    UserModel.setPassword(user, 'test')

    #Set hasSurvey to True for user
    user['hasSurvey'] = True

    #Save user
    user = UserModel.save(user)

    #Delete all review links
    reviewLinkDb.delete_many({})

    reviewLink = ReviewLinksModel.create()

    reviewLink['userId'] = user['_id']
    reviewLink['site'] = 'Facebook'
    reviewLink['url'] = 'https://facebooksucks.com'
    
    ReviewLinksModel.createHash(reviewLink)

    ReviewLinksModel.save(reviewLink)

    #Delete all surveys
    surveyDb.delete_many({})

    #Create a new survey
    survey = SurveyModel.create()

    #Set user id 
    survey['userId'] = user['_id']

    survey['positiveCutoff'] = 0.75
    survey['positiveReviewMessage'] = 'Thank you for completing our survey.'
    survey['negativeReviewMessage'] = 'Sorry you had a negative experience.'
    survey['invalidReviewMessage'] = 'Thank you for completing our survey invalid.'
    survey['trigger'] = 'Hey RepGuru'
    survey['hasIndicators'] = False
    survey['reviewColor'] = '#3f51b5'


    #Save survey
    survey = SurveyModel.save(survey)

    #Delete questions
    questionDb.delete_many({})

    #Create a question
    question = QuestionModel.create()

    #Set survey id
    question['surveyId'] = survey['_id']

    #Set data
    question['type'] = 'yes-no'
    question['text'] = 'Do you like bananas?'
    question['scale'] = 5
    question['indicator'] = {
        'isIndicator': True,
        'positiveCutoff': 3,
        'higherIsGood': True,
        'booleanIndicator': 'yes'
    }

    #Save question
    QuestionModel.save(question)

    #Delete all reviews
    reviewDb.delete_many({})

    #Create a new Review
    review = ReviewModel.create()

    #Add userId to review
    review['userId'] = user['_id']

    #Add phone number to review
    review['phoneNumber'] = '1234567890'

    review['answers'] = [
        {
            'questionId': question['_id'],
            'answer': 'no'
        }
    ]

    review['contact'] = False

    review['completed'] = datetime.utcnow()

    savedReview = ReviewModel.save(review)

    ReviewModel.gradeReview(savedReview['_id'], survey, [question])

    customerDb.delete_many({})
    
    for num in range(1, 20):
        customer = CustomerModel.create()

        customer['userId'] = user['_id']

        customer['phoneNumber'] = '%d' % num

        customer['email'] = 'test@test.com'
        
        customer['name'] = 'test %d' % num

        CustomerModel.save(customer)

    if reviews > 0:
        for num in range(1, reviews):
            #Create a new Review
            review = ReviewModel.create()

            #Add userId to review
            review['userId'] = user['_id']

            #Add phone number to review
            review['phoneNumber'] = '1234567890%d' % num

            review['answers'] = [
                {
                    'questionId': question['_id'],
                    'answer': 'no'
                }
            ]

            review['completed'] = getRandomDatePastMonth()
            review['email'] = 'test@test.com'
            review['name'] = 'poopy pants %d' % num

            #Even number divisible by 4 completed and positive review no contact
            if num % 2 == 0 and num % 4 == 0:
                review['answers'][0]['answer'] = 'yes'
                review['contact'] = False

            #Even number completed and positive review and contact
            elif num % 2 == 0:
                review['answers'][0]['answer'] = 'yes'
                review['contact'] = True

            #Odd number divisible by 3, negative review
            elif num % 2 != 0 and num % 3 == 0:
                review['contact'] = False

            #Else odd number not divisible by 3, review negative and contact, gave detailed review
            else:
                review['additionalComments'] = '%d sucky sucky shit' % num
                review['contact'] = False

            savedReview = ReviewModel.save(review)

            ReviewModel.updateStep(savedReview['_id'], 'done')

            ReviewModel.gradeReview(savedReview['_id'], survey, [question])
