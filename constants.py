#Save the supported review sites w internal code (lowercase, - instead of ' ') to be used in app
reviewSites = {
    'yelp': 'Yelp',
    'facebook': 'Facebook',
    'google': 'Google',
    'trip-advisor': 'Trip Advisor'
}