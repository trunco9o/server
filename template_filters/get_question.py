from app import app
from models.Question import QuestionModel
from models.RetiredQuestion import RetiredQuestionModel

@app.template_filter('getQuestion')
def _get_question_text(questionId):
    """
    Get question text by question ID
    """

    #Lookup question
    question = QuestionModel.getById(questionId)

    if question is None:
        question = RetiredQuestionModel.getByOriginalId(questionId)

    #Return question text
    return question['text']