from dateutil import tz

from app import app

@app.template_filter('strftime')
def _filter_utc_date(date):
    """
    Converts datetime object from UTC to EST in m/d/Y H:M p
    """

    #Get UTC timezone
    utc = tz.gettz('UTC')

    #Get EST timezone
    est = tz.gettz('America/New_York')

    #Update timezone of date
    utcDate = date.replace(tzinfo=utc)

    #Get EST timezone'd date
    estDate = utcDate.astimezone(est)

    #Get formatted string from EST date
    timeString = estDate.strftime('%m/%d/%Y %H:%M %p')

    return timeString