#getenv to get environment variables
from os import getenv

#livereload watches files and reloads the running code without stopping + starting code
from livereload import Server

from os import getenv

#app.py defines Flask app
from app import app

#If this file is being run directly, start app
if __name__ == '__main__':
    #Load flask app as wsgi app into livereload
    server = Server(app.wsgi_app)

    #Set up files to be watched by livereload
    server.watch('server.py')
    server.watch('controllers/*')
    
    #Run livereload server port 5500
    server.serve(host=getenv('HOST', 'localhost'), port=getenv('PORT', 5500))
