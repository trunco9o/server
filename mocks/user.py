#Mock user to be used in place of database user
user = {
    'id': 1,
    'name': 'carlos',
    'business': 'Carlos\' Crack',
    'smsSetup': False,
    'email': 'carlos@gmail.com',
    'phone': '(123) 456-7890',
    'textNumber': '(123) 456-7890',
    'reviewLinks': {
        'yelp': 'yelp.com',
        'facebook': 'facebook.com',
        'google': 'google.com',
        'trip-advisor': 'tripadvisor.com'
    },
    'hasSurvey': True,
    'smsSurvey': [
        {
            'id': 1,
            'type': 'yes-no',
            'text': 'Did you enjoy your experience?'
        },
        {
            'id': 2,
            'type': 'open-ended',
            'text': 'In 160 characters or less, what did you like most about Carlos\' Crack?'
        },
        {
            'id': 3,
            'type': 'scale',
            'scale': 10,
            'text': 'From 1-10, how satisfied were you with your crack?'
        }
    ]
}