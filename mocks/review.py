class review:
    def __init__(self):
        self.positive = ''

    def __getitem__(self, key):
        return getattr(self, key)

    def __setitem__(self, key, value):
        setattr(self, key, value)

    def save(self):
        return