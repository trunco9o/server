import unittest
from unittest.mock import MagicMock

from bson.json_util import dumps

from services.ReviewService import review_service

from seeds.seed import seed

from models.Review import ReviewModel, reviewDb
from models.User import UserModel, userDb
from models.Survey import SurveyModel, surveyDb
from models.Question import QuestionModel
from models.ReviewLinks import ReviewLinksModel

from mocks.review import review as reviewMock

import assets.strings.python.responses as responses

def make_response(message):
    return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response><Message>%s</Message></Response>" % message 

class ReviewServiceTest(unittest.TestCase):
    def setUp(self):
        seed()

        self.review_service = review_service(ReviewModel, UserModel, SurveyModel, QuestionModel, ReviewLinksModel)
        self.survey = surveyDb.find_one()
        self.user = userDb.find_one()


    def make_question(self, _type='range', indicator=True, text='test', positiveCutoff=3, higherIsGood=True, booleanIndicator='yes'):
        question = QuestionModel.create()

        surveyId = self.survey['_id']

        question['text'] = text
        question['surveyId'] = surveyId
        question['type'] = _type
        question['indicator'] = {
            'isIndicator': indicator,
            'positiveCutoff': positiveCutoff,
            'higherIsGood': higherIsGood,
            'booleanIndicator': booleanIndicator
        }

        question = QuestionModel.save(question)

        if indicator:
            SurveyModel.setIndicator(surveyId, indicator)

        return question
    
    def test_do_review_sends_first_question(self):
        response = self.review_service.doReview('1', self.user['textNumber'], self.survey['trigger'])

        firstQuestion = QuestionModel.getByIndex(self.survey['_id'], 0)

        self.assertEqual(response, make_response(firstQuestion['text']))

    def test_do_review_sends_first_question_doesnt_answer_wrong_trigger(self):
        response = self.review_service.doReview('2', self.user['textNumber'], "%s123" % self.survey['trigger']) 

        self.assertEqual(response, False)

    def test_do_review_records_answer_after_first_question(self):
        self.review_service.doReview('1', self.user['textNumber'], self.survey['trigger'])
        
        self.review_service.doReview('1', self.user['textNumber'], 'yes')

        review = reviewDb.find_one({'phoneNumber': '1'})

        self.assertEqual(len(review['answers']), 1)

    def test_do_review_records_increments_question_index(self):
        self.review_service.doReview('1', self.user['textNumber'], self.survey['trigger'])

        self.review_service.doReview('1', self.user['textNumber'], 'yes')

        review = reviewDb.find_one({'phoneNumber': '1'})

        self.assertEqual(review['questionIndex'], 0)

    def test_do_review_sends_contact_question(self):
        self.review_service.doReview('1', self.user['textNumber'], self.survey['trigger'])

        response = self.review_service.doReview('1', self.user['textNumber'], 'yes')

        self.assertEqual(response, make_response(responses.contact_info % self.user['name']))

    def test_do_review_saves_contact_answer(self):
        self.review_service.doReview('1', self.user['textNumber'], self.survey['trigger'])

        self.review_service.doReview('1', self.user['textNumber'], 'yes')

        self.review_service.doReview('1', self.user['textNumber'], 'yes')

        review = reviewDb.find_one({'phoneNumber': '1'})

        self.assertEqual(review['contact'], True)

    def test_do_review_asks_for_email(self):
        self.review_service.doReview('1', self.user['textNumber'], self.survey['trigger'])

        self.review_service.doReview('1', self.user['textNumber'], 'yes')

        response = self.review_service.doReview('1', self.user['textNumber'], 'yes')

        self.assertEqual(response, make_response(responses.email_address % self.user['name']))

    def test_do_review_asks_for_name(self):
        self.review_service.doReview('1', self.user['textNumber'], self.survey['trigger'])

        self.review_service.doReview('1', self.user['textNumber'], 'yes')

        response = self.review_service.doReview('1', self.user['textNumber'], 'yes')

        self.assertEqual(response, make_response(responses.email_address % self.user['name']))

        response = self.review_service.doReview('1', self.user['textNumber'], 'yes')

        self.assertEqual(response, make_response(responses.name % self.user['name']))        

    def test_do_review_saves_email(self):
        self.review_service.doReview('1', self.user['textNumber'], self.survey['trigger'])

        self.review_service.doReview('1', self.user['textNumber'], 'yes')

        self.review_service.doReview('1', self.user['textNumber'], 'yes')

        self.review_service.doReview('1', self.user['textNumber'], 'test123@test123.com')

        review = reviewDb.find_one({'phoneNumber': '1'})

        self.assertEqual(review['email'], 'test123@test123.com')

    def test_do_review_sends_review_links_when_no_to_contact_no_indicators(self):
        self.review_service.doReview('1', self.user['textNumber'], self.survey['trigger'])

        self.review_service.doReview('1', self.user['textNumber'], 'yes')

        response = self.review_service.doReview('1', self.user['textNumber'], 'no')

        reviewLinksString = ReviewLinksModel.getReviewLinksString(self.user['_id'])

        expected = make_response("%s\n%s" % (self.survey['positiveReviewMessage'], reviewLinksString))

        self.assertEqual(response, expected)
    
    def test_do_review_sends_review_links_with_code_when_no_to_contact_no_indicators(self):

        UserModel.updateSettings(self.user['_id'], 'generateReviewCode', True)

        self.review_service.doReview('1', self.user['textNumber'], self.survey['trigger'])

        self.review_service.doReview('1', self.user['textNumber'], 'yes')

        response = self.review_service.doReview('1', self.user['textNumber'], 'no')

        reviewLinksString = ReviewLinksModel.getReviewLinksString(self.user['_id'])

        code = ReviewModel.getByPhoneNumber(self.user['_id'], '1')[0]['code']

        expected = make_response("%s\n%s\n%s" % (self.survey['positiveReviewMessage'], reviewLinksString, code))

        self.assertEqual(response, expected)
    
    def test_do_review_positive_when_review_positive(self):
        self.make_question()

        response = self.review_service.doReview('1', self.user['textNumber'], self.survey['trigger'])

        response = self.review_service.doReview('1', self.user['textNumber'], 'yes')

        response = self.review_service.doReview('1', self.user['textNumber'], '5')

        response = self.review_service.doReview('1', self.user['textNumber'], 'no')

        review = reviewDb.find_one({'phoneNumber': '1'})

        self.assertEqual(review['isPositive'], True)
    
    def test_do_review_negative_when_review_negative(self):
        self.make_question()

        self.review_service.doReview('1', self.user['textNumber'], self.survey['trigger'])

        self.review_service.doReview('1', self.user['textNumber'], 'yes')

        self.review_service.doReview('1', self.user['textNumber'], '2')

        self.review_service.doReview('1', self.user['textNumber'], 'yes')

        self.review_service.doReview('1', self.user['textNumber'], 'test123@test123.com')

        self.review_service.doReview('1', self.user['textNumber'], 'pooo yo')

        review = reviewDb.find_one({'phoneNumber': '1'})

        self.assertEqual(review['isPositive'], False)

    def test_do_review_sends_review_links_when_review_positive(self):
        self.make_question()

        response = self.review_service.doReview('1', self.user['textNumber'], self.survey['trigger'])

        response = self.review_service.doReview('1', self.user['textNumber'], 'yes')

        self.review_service.doReview('1', self.user['textNumber'], '5')

        response = self.review_service.doReview('1', self.user['textNumber'], 'no')

        review = reviewDb.find_one({'phoneNumber': '1'})

        reviewLinksString = ReviewLinksModel.getReviewLinksString(self.user['_id'])

        expected = make_response("%s\n%s" % (self.survey['positiveReviewMessage'], reviewLinksString))

        self.assertEqual(response, expected)

    def test_do_review_sends_review_links_and_code_when_review_positive(self):
        self.make_question()

        UserModel.updateSettings(self.user['_id'], 'generateReviewCode', True)

        response = self.review_service.doReview('1', self.user['textNumber'], self.survey['trigger'])

        response = self.review_service.doReview('1', self.user['textNumber'], 'yes')

        self.review_service.doReview('1', self.user['textNumber'], '5')

        response = self.review_service.doReview('1', self.user['textNumber'], 'no')

        review = reviewDb.find_one({'phoneNumber': '1'})

        reviewLinksString = ReviewLinksModel.getReviewLinksString(self.user['_id'])

        expected = make_response("%s\n%s\n%s" % (self.survey['positiveReviewMessage'], reviewLinksString, review['code']))

        self.assertEqual(response, expected)

    def test_do_review_sends_negative_message_when_review_negative(self):
        self.make_question()

        self.review_service.doReview('1', self.user['textNumber'], self.survey['trigger'])

        self.review_service.doReview('1', self.user['textNumber'], 'yes')

        self.review_service.doReview('1', self.user['textNumber'], '2')

        response = self.review_service.doReview('1', self.user['textNumber'], 'no')

        review = reviewDb.find_one({'phoneNumber': '1'})

        reviewUrl = ReviewModel.makeReviewUrl(review['_id'])

        expected = make_response("%s\n%s" % (self.survey['negativeReviewMessage'], reviewUrl))

        self.assertEqual(response, expected)

    def test_do_review_sends_negative_message_when_review_negative_with_code(self):
        self.make_question()

        UserModel.updateSettings(self.user['_id'], 'generateReviewCode', True)

        self.review_service.doReview('1', self.user['textNumber'], self.survey['trigger'])

        self.review_service.doReview('1', self.user['textNumber'], 'yes')

        self.review_service.doReview('1', self.user['textNumber'], '2')

        response = self.review_service.doReview('1', self.user['textNumber'], 'no')

        review = reviewDb.find_one({'phoneNumber': '1'})

        reviewUrl = ReviewModel.makeReviewUrl(review['_id'])

        expected = make_response("%s\n%s\n%s" % (self.survey['negativeReviewMessage'], reviewUrl, review['code']))

        self.assertEqual(response, expected)
    
    def test_do_review_sends_invalid_message_when_review_invalid(self):
        self.make_question()

        self.review_service.doReview('1', self.user['textNumber'], self.survey['trigger'])

        self.review_service.doReview('1', self.user['textNumber'], 'yes')

        self.review_service.doReview('1', self.user['textNumber'], 'poop')

        response = self.review_service.doReview('1', self.user['textNumber'], 'no')

        review = reviewDb.find_one({'phoneNumber': '1'})

        reviewUrl = ReviewModel.makeReviewUrl(review['_id'])

        expected = make_response("%s\n%s" % (self.survey['invalidReviewMessage'], reviewUrl))

        self.assertEqual(response, expected)

    def test_do_review_sends_invalid_message_when_review_invalid_with_code(self):
        self.make_question()

        UserModel.updateSettings(self.user['_id'], 'generateReviewCode', True)

        self.review_service.doReview('1', self.user['textNumber'], self.survey['trigger'])

        self.review_service.doReview('1', self.user['textNumber'], 'poop')

        self.review_service.doReview('1', self.user['textNumber'], 'poop')

        response = self.review_service.doReview('1', self.user['textNumber'], 'no')

        review = reviewDb.find_one({'phoneNumber': '1'})

        reviewUrl = ReviewModel.makeReviewUrl(review['_id'])

        expected = make_response("%s\n%s\n%s" % (self.survey['invalidReviewMessage'], reviewUrl, review['code']))

        self.assertEqual(review['isInvalid'], True)
        self.assertEqual(response, expected)

    def test_do_review_sets_step_done_invalid(self):
        self.make_question()

        self.review_service.doReview('1', self.user['textNumber'], self.survey['trigger'])

        self.review_service.doReview('1', self.user['textNumber'], 'poop')

        self.review_service.doReview('1', self.user['textNumber'], 'poop')

        self.review_service.doReview('1', self.user['textNumber'], 'no')

        review = reviewDb.find_one({'phoneNumber': '1'})

        self.assertEqual(review['isInvalid'], True)
        self.assertEqual(review['step'], 'end')

    
    def test_do_review_sets_step_done_negative(self):
        self.make_question()

        self.review_service.doReview('1', self.user['textNumber'], self.survey['trigger'])

        self.review_service.doReview('1', self.user['textNumber'], 'no')

        self.review_service.doReview('1', self.user['textNumber'], '2')

        self.review_service.doReview('1', self.user['textNumber'], 'no')

        review = reviewDb.find_one({'phoneNumber': '1'})

        self.assertEqual(review['isPositive'], False)
        self.assertEqual(('isInvalid' in review), False)
        self.assertEqual(review['step'], 'end')

    def test_do_review_sets_step_done_positive(self):
        self.make_question()

        self.review_service.doReview('1', self.user['textNumber'], self.survey['trigger'])

        self.review_service.doReview('1', self.user['textNumber'], 'yes')

        self.review_service.doReview('1', self.user['textNumber'], '5')

        self.review_service.doReview('1', self.user['textNumber'], 'no')

        review = reviewDb.find_one({'phoneNumber': '1'})

        self.assertEqual(review['isPositive'], True)
        self.assertEqual(('isInvalid' in review), False)
        self.assertEqual(review['step'], 'end')
    
    def test_do_review_sets_step_done_no_indicator(self):

        question = QuestionModel.getByIndex(self.survey['_id'], 0)

        indicator = question['indicator']

        indicator['isIndicator'] = False

        QuestionModel.update(question['_id'], indicator)

        self.review_service.doReview('1', self.user['textNumber'], self.survey['trigger'])

        self.review_service.doReview('1', self.user['textNumber'], 'poopy')

        self.review_service.doReview('1', self.user['textNumber'], 'no')

        review = reviewDb.find_one({'phoneNumber': '1'})

        self.assertEqual(('isPositive' in review), False)
        self.assertEqual(('isInvalid' in review), False)
        self.assertEqual(('completed' in review), True)
        self.assertEqual(review['step'], 'end')
    
if __name__ == '__main__':
    unittest.main()