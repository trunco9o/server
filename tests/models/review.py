import unittest
from unittest.mock import MagicMock

from seeds.seed import seed

from models.Review import ReviewModel, reviewDb
from models.Question import QuestionModel

def make_question(_type='range', indicator=True, text='test', positiveCutoff=3, higherIsGood=True, booleanIndicator='yes'):
        question = QuestionModel.create()

        question['type'] = _type
        question['indicator'] = {
            'isIndicator': indicator,
            'positiveCutoff': positiveCutoff,
            'higherIsGood': higherIsGood,
            'booleanIndicator': booleanIndicator
        }

        return question

class ReviewTest(unittest.TestCase):
    
    def tearDown(self):
        seed()

    def test_checkBooleanIndicator_grades_yes_is_good_correctly(self):
        question = make_question(_type="yes-no")

        answer = 'yes'

        result = ReviewModel.checkBooleanIndicator(question['indicator'], answer)

        self.assertEqual(result, 1)

        answer = 'no'

        result = ReviewModel.checkBooleanIndicator(question['indicator'], answer)

        self.assertEqual(result, 0)

    def test_checkBooleanIndicator_grades_no_is_good_correctly(self):
        question = make_question(_type="yes-no", booleanIndicator='no')

        answer = 'no'

        result = ReviewModel.checkBooleanIndicator(question['indicator'], answer)

        self.assertEqual(result, 1)

        answer = 'yes'

        result = ReviewModel.checkBooleanIndicator(question['indicator'], answer)

        self.assertEqual(result, 0)

    def test_checkBooleanIndicator_grades_invalid_correctly_no_is_good(self):
        question = make_question(_type="yes-no", booleanIndicator='no')

        answer = 'poop'

        result = ReviewModel.checkBooleanIndicator(question['indicator'], answer)

        self.assertEqual(result, 'invalid')

    def test_checkBooleanIndicator_grades_invalid_correctly_yes_is_good(self):
        question = make_question(_type="yes-no")

        answer = 'poop'

        result = ReviewModel.checkBooleanIndicator(question['indicator'], answer)

        self.assertEqual(result, 'invalid')

    def test_checkRangeIndicator_grades_higher_is_good_correctly(self):
        question = make_question(_type="range")

        answer = '4'

        result = ReviewModel.checkRangeIndicator(question['indicator'], answer)

        self.assertEqual(result, 1)

        answer = '3'

        result = ReviewModel.checkRangeIndicator(question['indicator'], answer)

        self.assertEqual(result, 1)

        answer = '2'

        result = ReviewModel.checkRangeIndicator(question['indicator'], answer)

        self.assertEqual(result, 0)

    def test_checkRangeIndicator_grades_lower_is_good_correctly(self):
        question = make_question(_type="range", higherIsGood=False)

        answer = '4'

        result = ReviewModel.checkRangeIndicator(question['indicator'], answer)

        self.assertEqual(result, 0)

        answer = '3'

        result = ReviewModel.checkRangeIndicator(question['indicator'], answer)

        self.assertEqual(result, 1)

        answer = '2'

        result = ReviewModel.checkRangeIndicator(question['indicator'], answer)

        self.assertEqual(result, 1)

    def test_checkRangeIndicator_returns_invalid_correctly_higherIsGood(self):
        question = make_question(_type="range")

        answer = 'poop'

        result = ReviewModel.checkRangeIndicator(question['indicator'], answer)

        self.assertEqual(result, 'invalid')

    def test_checkRangeIndicator_returns_invalid_correctly_lowerIsGood(self):
        question = make_question(_type="range", higherIsGood=False)

        answer = 'poop'

        result = ReviewModel.checkRangeIndicator(question['indicator'], answer)

        self.assertEqual(result, 'invalid')

    def test_grade_question_classifies_boolean_indicator(self):
        question = make_question(_type="yes-no")

        checkBooleanIndicator = ReviewModel.checkBooleanIndicator

        ReviewModel.checkBooleanIndicator = MagicMock(return_value=1)

        answer = 'yes'

        ReviewModel.gradeQuestion(question, {'answer': answer})

        ReviewModel.checkBooleanIndicator.assert_called_with(question['indicator'], answer)

        ReviewModel.checkBooleanIndicator = checkBooleanIndicator

    def test_grade_question_classifies_range_indicator(self):
        question = make_question(_type="range")

        checkRangeIndicator = ReviewModel.checkRangeIndicator

        ReviewModel.checkRangeIndicator = MagicMock(return_value=1)

        answer = '1'

        ReviewModel.gradeQuestion(question, {'answer': answer})

        ReviewModel.checkRangeIndicator.assert_called_with(question['indicator'], answer)

        ReviewModel.checkRangeIndicator = checkRangeIndicator

    def test_grade_question_returns_on_open_ended(self):
        question = make_question(_type="open-ended")

        checkRangeIndicator = ReviewModel.checkRangeIndicator
        checkBooleanIndicator = ReviewModel.checkBooleanIndicator

        ReviewModel.checkRangeIndicator = MagicMock(return_value=1)
        ReviewModel.checkBooleanIndicator = MagicMock(return_value=1)

        answer = '1'

        ReviewModel.gradeQuestion(question, {'answer': answer})

        ReviewModel.checkRangeIndicator.assert_not_called()
        ReviewModel.checkBooleanIndicator.assert_not_called()

        ReviewModel.checkRangeIndicator = checkRangeIndicator
        ReviewModel.checkBooleanIndicator = checkBooleanIndicator

    def test_grade_review_grades_positive_review(self):
        # Test is positive
        getById = ReviewModel.getById
        update = ReviewModel.update

        ReviewModel.getById = MagicMock(return_value={'answers': [{'answer': 3}, {'answer': 3}, {'answer': 3}]})
        ReviewModel.update = MagicMock(return_Value=False)

        ReviewModel.gradeReview(1, {'positiveCutoff': 0.75}, [make_question(), make_question(), make_question()])

        ReviewModel.update.assert_called_with(1, {'isPositive': True})

        ReviewModel.getById = getById
        ReviewModel.update = update
    
    def test_grade_review_grades_positive_review_one_indicator(self):
        # Test is positive 1 indicator
        getById = ReviewModel.getById
        update = ReviewModel.update

        ReviewModel.getById = MagicMock(return_value={'answers': [{'answer': 3}, {'answer': 3}]})
        ReviewModel.update = MagicMock()

        ReviewModel.gradeReview(1, {'positiveCutoff': 0.75}, [make_question(indicator=False), make_question()])

        ReviewModel.update.assert_called_with(1, {'isPositive': True})

        ReviewModel.getById = getById
        ReviewModel.update = update

    def test_grade_review_grades_negative_review(self):
        # Test is negative
        getById = ReviewModel.getById
        update = ReviewModel.update

        ReviewModel.getById = MagicMock(return_value={'answers': [{'answer': 2}, {'answer': 2}, {'answer': 2}]})
        ReviewModel.update = MagicMock()

        ReviewModel.gradeReview(1, {'positiveCutoff': 0.75}, [make_question(), make_question(), make_question()])

        ReviewModel.update.assert_called_with(1, {'isPositive': False})

        ReviewModel.getById = getById
        ReviewModel.update = update

    def test_grade_review_grades_positive_review_at_cutoff(self):
        # Test is @ cutoff
        getById = ReviewModel.getById
        update = ReviewModel.update

        ReviewModel.update = MagicMock()
        ReviewModel.getById = MagicMock(return_value={'answers': [{'answer': 3}, {'answer': 3}, {'answer': 3}, {'answer': 2}]})

        ReviewModel.gradeReview(1, {'positiveCutoff': 0.75}, [make_question(), make_question(), make_question(), make_question()])

        ReviewModel.update.assert_called_with(1, {'isPositive': True})

        ReviewModel.getById = getById
        ReviewModel.update = update

    def test_grade_review_grades_invalid(self):
        # Test invalid
        getById = ReviewModel.getById
        setInvalid = ReviewModel.setInvalid
        update = ReviewModel.update

        ReviewModel.setInvalid = MagicMock()
        ReviewModel.getById = MagicMock(return_value={'answers': [{'answer': 3}, {'answer': 'poop'}, {'answer': 3}, {'answer': 3}]})
        ReviewModel.update = MagicMock()

        ReviewModel.gradeReview(1, {'positiveCutoff': 0.75}, [make_question(), make_question(), make_question(), make_question()])

        ReviewModel.update.assert_called_with(1, {'isPositive': True})

        ReviewModel.setInvalid.assert_called_with(1, True)

        ReviewModel.getById = getById
        ReviewModel.setInvalid = setInvalid
        ReviewModel.update = update
    
if __name__ == '__main__':
    unittest.main()