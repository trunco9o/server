from os import getenv

if not getenv('TWILIO_SID', False):
    #Import dotenv to load env variables
    from dotenv import load_dotenv
    #Import pathlib to load env path
    from pathlib import Path

    #Set env path
    env_path = str(Path('.') / '.env')

    #Load .env file
    load_dotenv(dotenv_path=env_path)

from twilio.rest import Client
from twilio.twiml.messaging_response import MessagingResponse

class twilio_service:
    """
    twilio_service handles interaction with the Twilio API
    """
    @staticmethod
    def sendResponse(message=None):
        """
        Create a response to a message
        """

        response = MessagingResponse()

        response.message(message)

        return str(response)

    @staticmethod
    def createNumber(areaCode):
        client = Client(getenv('TWILIO_SID'), getenv('TWILIO_AUTH_TOKEN'))

        numbers = client.available_phone_numbers("US").local.list(area_code=areaCode)

        number = client.incoming_phone_numbers.create(phone_number=numbers[0].phone_number)

        return number.PhoneNumber