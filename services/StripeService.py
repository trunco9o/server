from os import getenv

if not getenv('STRIPE_SECRET', False):
    #Import dotenv to load env variables
    from dotenv import load_dotenv
    #Import pathlib to load env path
    from pathlib import Path

    #Set env path
    env_path = str(Path('.') / '.env')

    #Load .env file
    load_dotenv(dotenv_path=env_path)

import stripe

stripe.api_key = getenv('STRIPE_SECRET')

class stripe_service:
    @staticmethod
    def createCustomer(email):
        customer = stripe.Customer.create(email=email)

        return customer['id']