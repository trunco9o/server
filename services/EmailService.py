from flask_mail import Message

from app import mail

class email_service:
    """
    email_service handles emails
    """

    @staticmethod
    def createMessage(subject, sender=None):
        """
        Static createMessage creates a Message object
        """

        if sender is None:
            sender = ('ReviewJawn', 'info@reviewjawn.com')

        email = Message(subject, sender=sender)

        return email

    @staticmethod
    def makeEmailBody(text, contact, number=False):
        """
        Generate HTML for email body
        """

        email = """
            <h2>New Review</h2>
            <p>%s</p>
        """

        if contact:
            email += """
            The reviewer can be contacted at: %s
            """

            email = email % (text, number)

        else:
            email = email % text

        return email

    @staticmethod
    def sendEmail(message):
        """
        sendEmail uses mail object defined in app to send message
        """

        mail.send(message)