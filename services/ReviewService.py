from os import getenv
from hashids import Hashids

from services.TwilioService import twilio_service
import assets.strings.python.responses as responses

class review_service:
    """
    self has methods for managing a SMS review
    """

    def __init__(self, reviewModel, userModel, surveyModel, questionModel, reviewLinksModel):
        self.reviewModel = reviewModel
        self.userModel = userModel
        self.surveyModel = surveyModel
        self.questionModel = questionModel
        self.reviewLinksModel = reviewLinksModel

    def doReview(self, _from, to, body):
        user = self.userModel.getByTextNumber(to)

        userId = user['_id']

        canReview = self.reviewModel.checkReviewFrequency(userId, user['settings']['reviewFrequency'], _from)

        if canReview:
            review = self.reviewModel.getIncompleteByPhoneNumber(userId, _from)

            survey = self.surveyModel.getByUserId(userId)

            if review is None:
                return self.getFirstMessage(_from, user, body, survey)
            else:
                return self.handleSurvey(user, body, survey, review)
        else:
            return self.frequencyMessage(user)

    def frequencyMessage(user):
        if 'reviewFrequencyMessage' in user['settings']:
            message = user['settings']['reviewFrequencyMessage']
        else:
            message = 'Sorry, you have already completed this survey. Try again later'

        return twilio_service.sendResponse(message)

    def getFirstMessage(self, _from, user, body, survey):
        if body.lower() == survey['trigger'].lower():
            review = self.reviewModel.create()

            review['questionIndex'] = 0
            review['step'] = 'survey'

            review['phoneNumber'] = _from

            review['userId'] = user['_id']

            self.reviewModel.save(review)

            questions = self.questionModel.getBySurveyId(survey['_id'])

            if questions.count() == 1:
                self.updateStep(review, 'last-question')

            return twilio_service.sendResponse(questions[0]['text'])

        else:
            return False

    def handleSurvey(self, user, body, survey, review):
        if review['step'] == 'survey':

            return self.nextSurveyQuestion(body, survey, review)

        elif review['step'] == 'last-question':

            self.saveLastQuestion(body, survey, review)
            
            if user['settings']['requestContactInfo']:
                return self.requestContactInfo(body, survey, review, user)
            else: 
                review['step'] = 'no-contact-info'

                return self.handleSurvey(user, body, survey, review)

        elif review['step'] == 'contact-info':

            saveContactInfoResponse = self.saveContactInfoResponse(body, review)
            return self.requestEmail(review, user) if saveContactInfoResponse is True else self.questionsComplete(survey, review, user)

        elif review['step'] == 'contact-info-email':

            self.saveEmail(body, review)
            return self.requestName(review, user)

        elif review['step'] == 'contact-info-name' or review['step'] == 'no-contact-info':
            
            self.saveName(body, review)
            return self.questionsComplete(survey, review, user)


        elif review['step'] == 'end':
            return self.sendLastMessage(survey)

        else:
            return False

    def nextSurveyQuestion(self, body, survey, review):
        questionIndex = review['questionIndex']

        questions = self.questionModel.getBySurveyId(survey['_id'])

        if (questionIndex + 2) >= questions.count():
            self.updateStep(review, 'last-question')

        self.updateReviewAnswers(review, survey, body)

        self.updateQuestionIndex(review['_id'], questionIndex)

        return twilio_service.sendResponse(questions[questionIndex + 1]['text'])

# Survey Lifecycle Steps

    def saveLastQuestion(self, body, survey, review):
        self.updateReviewAnswers(review, survey, body)

        self.updateStep(review, 'contact-info')

    def requestContactInfo(self, body, survey, review, user):
        return twilio_service.sendResponse(responses.contact_info % user['business'])

    def saveContactInfoResponse(self, body, review):
        lowerCaseAnswer = body.lower()

        contact = 'invalid'

        if any(lowerCaseAnswer in s for s in responses.positive) or responses.positiveRegex.match(lowerCaseAnswer):
            contact = True
        elif any(lowerCaseAnswer in s for s in responses.negative) or responses.negativeRegex.match(lowerCaseAnswer):
            contact = False

        self.reviewModel.setContact(review['_id'], contact)

        return contact

    def requestEmail(self, review, user):
        self.updateStep(review, 'contact-info-email')

        if 'emailRequestMessage' in user['settings']:
            response = user['settings']['emailRequestMessage']
        else:
            response = responses.email_address % user['business']

        return twilio_service.sendResponse(response)

    def requestName(self, review, user):
        self.updateStep(review, 'contact-info-name')

        if 'nameRequestMessage' in user['settings']:
            response = user['settings']['nameRequestMessage']
        else:
            response = responses.name % user['business']

        return twilio_service.sendResponse(response)

    def questionsComplete(self, survey, review, user):
        questions = self.questionModel.getBySurveyId(survey['_id'])

        code = ''
        generateCode = user['settings']['generateReviewCode']

        if generateCode:
            code = self.getReviewCode(user['_id'], review['phoneNumber'])

            self.reviewModel.setCode(review['_id'], code)

            if 'reviewCodeMessage' in user['settings']:
                code = '%s %s' % (user['settings']['reviewCodeMessage'], code)
            else:
                code = 'Review verification code: %s' % code

        message = ''

        if survey['hasIndicators']:
            gradedReview = self.reviewModel.gradeReview(review['_id'], survey, questions)
            isPositive = gradedReview['isPositive']

            if isPositive:
                message = self.sendReviewLinks(survey, review, user, generateCode, code, user)
            elif 'isInvalid' in gradedReview and gradedReview['isInvalid']:
                message = self.sendInvalidReviewMessage(survey, review, generateCode, code, user)
            else:
                message = self.sendNegativeReviewMessage(survey, review, generateCode, code, user)
        else:
            message = self.sendReviewLinks(survey, review, user, generateCode, code, user)

        self.updateStep(review, 'end')
        self.reviewModel.setCompleted(review['_id'])

        return message

    def sendReviewLinks(self, survey, review, user, sendCode, code, user):
        
        reviewLinksString = self.reviewLinksModel.getReviewLinksString(user['_id'])

        message = self.reviewModel.getPositiveReviewMessage(review['_id'], survey, user['settings']['sendDetailedReview'], detailedReviewMessage=user['settings']['detailedReviewMessage'])

        response = "%s\n%s" % (message, reviewLinksString)

        if sendCode:
            response = "%s\n%s" % (response, code)

        return twilio_service.sendResponse(response)

    def sendInvalidReviewMessage(self, survey, review, sendCode, code, user):
        message = self.reviewModel.getInvalidReviewMessage(review['_id'], survey, user['settings']['sendDetailedReview'], detailedReviewMessage=user['settings']['detailedReviewMessage'])

        if sendCode:
            message = "%s\n%s" % (message, code)


        return twilio_service.sendResponse(message)

    def sendNegativeReviewMessage(self, survey, review, sendCode, code, user):
        message = self.reviewModel.getNegativeReviewMessage(review['_id'], survey, user['settings']['sendDetailedReview'], detailedReviewMessage=user['settings']['detailedReviewMessage'])

        if sendCode:
            message = "%s\n%s" % (message, code)

        return twilio_service.sendResponse(message)

    def saveEmail(self, body, review):
        self.reviewModel.setEmail(review['_id'], body)

    def saveName(self, body, review):
        self.reviewModel.setName(review['_id'], body)

    def sendLastMessage(self, review):
        self.updateStep(review, 'done')

        return twilio_service.sendResponse(responses.done)

# Helper methods

    def updateReviewAnswers(self, review, survey, response):
        question = self.questionModel.getByIndex(survey['_id'], review['questionIndex'])

        self.reviewModel.addAnswer(review['_id'], response, question)

    def updateQuestionIndex(self, id, questionIndex):
        self.reviewModel.updateQuestionIndex(id, questionIndex + 1)

    def updateStep(self, review, step):
        self.reviewModel.updateStep(review['_id'], step)

    def getReviewCode(self, userId, phone):
        hash_id = Hashids(salt=getenv('REVIEW_CODE_SALT', ''), min_length=8)

        code = hash_id.encrypt("%s:%s" % (userId,phone))

        return code
